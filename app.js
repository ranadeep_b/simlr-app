require('nodetime').profile({
    accountKey: 'ea8cd610a15c2427512e37a48ac28c4a25fd735a', 
    appName: 'simlr.me - App'
  });

var toobusy 	= require('toobusy');
var express 	= require('express');
var http 		= require('http');
var mongoose  	= require('./routes/models/dbConnection/dbConnection');
var MongooseStore = require('./node_modules/express-mongodb.js')(express);
var util 		= require('util');
var utils 		  = require('./node_modules/express/node_modules/connect/lib/utils.js');
var cookieParser  = require('./node_modules/express/node_modules/cookie');
var fs 			= require('fs');

var mainDomain = require('domain').create();
var d 		   = require('./routes/domains');

var app = express();
var server = http.createServer(app);
server.listen(5000);
console.log('Server Listening 5000');

var isConnectedToDatabase = false;

mainDomain.on('error',function(err){
	util.log(err);
	fs.appendFile('main&AppDomainErrors.txt',err.message+"\r"+err.stack,function(err){
		if(err) throw new Error('Error updating main&AppDomainErrors');
	//Safely stop accepting incoming connections
		server.close(function(){
			util.log('Closed server connections. There maybe running i/o ops so dont worry if mongodb is not closed');
			if(isConnectedToDatabase) mongoose.connection.close();
			else {
				util.log('Not yet connected to mongoDB, so exiting directly');
				process.exit(1);
				toobusy.shutdown();
			}
		})
	})

})

mainDomain.run(function(){
//Start domain code

d.app.on('error',function(err){
	util.log(err);
	throw new Error(err);
	fs.appendFile('appErrors.txt',err.message+"\r"+err.stack,function(err){
		if(err) throw new Error('Error updating appErrors.txt');
	})
});

var mongoStore = new MongooseStore({connection : mongoose.connection,clear_interval : 60*60});
var sendToSocket = require('./socket');

app.configure(function(){
	app.set('view engine','jade');
	app.set('views',__dirname+'/views')
	app.use(express.logger('dev'));
	app.use(express.static(__dirname+'/public'));
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack	  : true
	}));
	app.use(function(req, res, next) {
	  if (toobusy()) {
	    res.send(503, "I'm busy right now, sorry.");
	  } else {
	    next();
	  } 
	});
	app.use(express.favicon(__dirname+'/public/img/favicon.ico'))
	app.use(express.bodyParser({keepExtensions : true , uploadDir : 'public/img/originals'}));
	app.use(express.cookieParser("shoutbox"));
	app.use(express.session({
		secret : 'shoutbox',
		cookie : {
			maxAge : 60000 * 60 * 24 // 60,000k
		},
		store  : mongoStore
	}));
	app.use(app.router);
});

//socketIo

//Auhtentication
var authenticate = function(req,res,next){
	if(req.session.userid) next();
	else res.redirect('/')
}

//All Routes , placed here to make the above globals to be visible
var routes 		= require('./routes');

app.get('/',routes.default);

app.post('/login',routes.login);

app.get('/register',routes.register);

app.post('/fbResponse',routes.fbResponse)

app.post('/registerProfile',routes.POSTregisterProfile);
//Authenticated Routes
app.get('/home',authenticate,routes.home);

app.get('/getMiniProfile',authenticate,routes.getMiniProfile);

app.get('/getLatestNotifications',authenticate,routes.getLatestNotifications);

app.get('/clearNotifications',authenticate,routes.clearNotifications);

app.get('/clearNotifications/:threadId',authenticate,routes.clearThreadNotifications);

app.post('/initiateChat',authenticate,routes.initiateChat);

app.get('/closeChat/:id',authenticate,routes.closeChat);

app.post('/sendFeedback/:id',authenticate,routes.chatFeedback);

app.get('/getTwinProfile/:id',authenticate,routes.getTwinProfile);

app.post('/sendRevealRequest',authenticate,routes.sendRevealRequest);

app.get('/getMyPic',authenticate,routes.getMyPic);

app.post('/setMyPic',authenticate,routes.setMyPic);// 

app.get('/getLatestThreads',authenticate,routes.getLatestThreads);

app.get('/getRecentThreads',authenticate,routes.getRecentThreads);

app.get('/thread/:threadId',authenticate,routes.getThreadById);

app.post('/sendThreadMessage',authenticate,routes.sendMessage);

app.get('/checkThreadStatus/:threadId',authenticate,routes.checkThreadStatus);

app.get('/getMyActivity',authenticate,routes.getMyActivity);

app.post('/modifyShout/:shoutId',authenticate,routes.modifyShout);

app.get('/getContacts',authenticate,routes.getContacts);

app.get('/settings',authenticate,routes.getSettings);

app.get('/settings/update',authenticate,routes.setSettings);

app.post('/shoutRespond',authenticate,routes.shoutRespond);

app.post('/shoutRespondInitiateChat',authenticate,routes.shoutRespondInitiateChat);

app.post('/shoutRespondSubmit',authenticate,routes.shoutRespondSubmit);

app.post('/sendChatMessage',authenticate,routes.sendChatMessage);

app.post('/sendContactRequest',authenticate,routes.sendContactRequest);

app.post('/contactResponse',authenticate,routes.contactResponse);

app.get('/getShouts',authenticate,routes.GETshouts);

app.get('/getLatestShouts',authenticate,routes.getLatestShouts);

app.get('/search',authenticate,routes.shoutSearch);

app.get('/getQuestion/:chatId',authenticate,routes.getQuestion);

app.post('/answer',authenticate,routes.answer);

app.get('/skipQuestion/:chatId',authenticate,routes.skipQuestion);

app.get('/getTwinLikes/:chatId',authenticate,routes.getTwinLikes);

app.get('/getFaces',authenticate,routes.getFaces);

app.post('/changeStatus',authenticate,routes.changeStatus);

app.post('/shoutSubmit',authenticate,routes.shoutSubmit);

app.post('/abracadabra',authenticate,routes.abracadabra);

app.get('/report',authenticate,routes.report);

app.get('/unfollow',authenticate,routes.unfollow);

app.get('/getRecentNotifications',authenticate,routes.getRecentNotifications);

app.get('/logout',authenticate,routes.logout);

app.get('/getCompliments',authenticate,routes.getCompliments);

app.get('/getPSST/:chatId',authenticate,routes.getPSST);

app.get('/sendThreadContactRequest/:threadId',authenticate,routes.sendThreadContactRequest);

app.get('/chatLog/:chatId',routes.getChatLog);

app.get('/feedback',function(req,res){
	res.redirect('https://docs.google.com/spreadsheet/embeddedform?formkey=dGZJVWxKNHpfMXkwa1BDelgyRHlmWFE6MA');
})


//Add methods only before this line
mongoose.connection.on('open',d.db.bind(function(){
	isConnectedToDatabase = true;
	console.log('mongoose connected to db :D');
}))

mongoose.connection.on('close',d.db.bind(function(){
	util.log('Closed mongodb & Exiting process with code 1');
	toobusy.shutdown();
	process.exit(1);
}))

mongoose.connection.on('error',d.db.bind(function(){
	util.log('Error connected to database..:(');
	//Send alert to me somehow
}));

//Closeing the d.app domain
})

