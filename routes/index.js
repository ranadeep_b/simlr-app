var models 		= require('./models/models.js');
var path   		= require('path');
var async 		= require('async');
var ik     		= require('gm').subClass({imageMagick : true});
var fs     		= require('fs');
var _ 	   		= require('lodash');
var fb 			= require('./fbApi.js');
var v   		= require('validator');	
var sendToSocket = require('../socket');
var d 			= require('./domains');

/* Later 
req.checkHeader('referer').contains('localhost'); validation
*/

d.app.run(function(){
	//Wrapping all the route code in d.app.run eror handler	
var noop = function(isOk){
	if(!isOk) console.log('The socket might be disconnected');
}

/*------- ROUTES ---------*/


/*-------Home route or login page , route : /, status : 1---------*/
module.exports.default = function(req,res){
	if(!req.session.userid) res.redirect('/login.html');
	else res.redirect('/home');
}

/*------- SignIn response, route : POST/login, status : ---------*/
module.exports.login = function(req,res){
	// if verified  , then store session , redirect to home
	try{
		v.check(req.body.fbId).notNull();
		v.check(req.body.accessToken).notNull();
		var fbId 		= parseInt(req.body.fbId),
		accessToken = req.body.accessToken;
	} catch(e){
		throw e;
		req.redirect('/');
		return ;
	}
	//
	models.Misc.findOne({bannedUsers : fbId},d.db.bind(function(err,doc){
		if(!doc){
			//Check if the given accessToken is real with those permissions asked and user requested
			fb.verifyAccessToken(fbId,accessToken,function(isError,accessToken){
				if(isError) res.send(400);
				else {
					models.UserDetail.findOne({fbId : fbId},'_id',d.db.bind(function(err,doc){
						if(err || !doc){
							models.PreRegister.findOne({fbId : fbId},'verified params fbAccessToken',d.db.bind(function(err,doc){
								if(err || !doc) {
									res.redirect('/');
									return;
								}
								else {
									res.redirect('/register?eat='+doc.fbAccessToken+'&fbId='+doc.fbId);
								}
							}))
						}
						else {
							var userId = doc._id;
							fb.extendAccessToken(accessToken,d.fb.bind(function(err,doc){
								if(err) res.send(400);
								else {
									models.UserDetail.update({_id : userId},{fbAccessToken : doc.accessToken,fbExpires : doc.expires},d.db.bind(function(err){
										if(err) res.send(500);
										else {
											req.session.userid = userId;
											req.session.cookie.maxAge = 27 * 24 * 60 * 60 * 1000;
											res.send(200);
											fb.setupArchive(doc.accessToken,function(err,fbResults){
												async.parallel([
													function(cbt){
														models.UserDetail.update({_id : userId},{fbFriends : fbResults.friendsIds,fbLikes : fbResults.likesIds},cbt);
													},
													function(cbt){
														models.FBArchive.update({user_id : userId},
															{
																likes 	  	  : fbResults.likes,
						 										likesIds 	  : fbResults.likesIds,
																musicLikes 	  : fbResults.musicLikes,
																moviesLikes	  : fbResults.moviesLikes,
																tvLikes 	  : fbResults.tvLikes,
																booksLikes 	  : fbResults.booksLikes,
																foodsLikes 	  : fbResults.foodsLikes,
																videoGamesLikes : fbResults.videoGamesLikes,
																causesLikes   : fbResults.causesLikes,
																fosLikes 	  : fbResults.fosLikes,
																educationLikes: fbResults.educationLikes,
																sportsLikes   : fbResults.sportsLikes,
																authorsLikes  : fbResults.authorsLikes,
																pstLikes 	  : fbResults.pstLikes,
																friends 	  : fbResults.friends,
																friendsIds    :  fbResults.friendsIds
															},
															cbt
														)
													}
													],d.db.bind(function(err,results){
														if(err || !results.length) {
															 console.log(err);
														}
													}));
											})
										}
									}))
								}
							}));
						}
					}))
				} 
			})
		}
		else{
			res.redirect('/sorry.html')
		}
	}))
}

/*------- Registration, route : GET/register, status : 0---------*/
/* ToDo :  Change the vit registraion and other reg stuff , like choosing the new linkedIn Skills and Meetup scraped hobbies :D*/
module.exports.register = function(req,res){
	try{
		v.check(req.query.eat).notNull();
		v.check(req.query.fbId).notNull();
	var eat 	= req.query.eat,
		fbId 	= parseInt(req.query.fbId);
	} catch(e){
		throw e;
		res.send(400);
		return;
	}

	models.PreRegister.findOne({fbId : fbId},d.db.bind(function(err,doc){
		if(err || !doc) res.redirect('/');
		else{
			var userId = doc._id;
			models.Params.find({type : 1},'title pid',d.db.bind(function(err,doc){
				if(err) res.redirect('/');
				else res.render('registerProfile',{id : userId,interests : doc});
			}));
		}
	}));
}

/*------- First time FB Registration response , route : POST/fbResponse, status : 1---------*/
module.exports.fbResponse = function(req,res){
	try{
		v.check(req.body.fb).notNull();
		var fbResponse = JSON.parse(req.body.fb);
	} catch(e){
		throw e;
		res.send(400);
		return;
	}
		// check if fbResponse actually has 'accessToken','expires','userID' and check if the userID is the generator of AT and it has all perms
		if(_.isUndefined(fbResponse.accessToken) || _.isUndefined(fbResponse.userID)) {
			throw new Error(req.session.userid + 'misuse/suspicious');
			res.send(400);
			return;
		}

		models.Misc.findOne({bannedUsers : fbResponse.userID},d.db.bind(function(err,doc){
			if(!doc){
				fb.verifyAccessToken(fbResponse.userID,fbResponse.accessToken,d.fb.bind(function(isError,accessToken){
					if(isError) res.send(400);
					else {
						models.PreRegister.findOne({fbId : fbResponse.userID},d.db.bind(function(err,doc){
							if(err || !doc){
								fb.extendAccessToken(fbResponse.accessToken,d.fb.bind(function(err,tokenInfo){
									if(err) res.send(400);
									else {
										fb.getBasicInformation(fbResponse.accessToken,fbResponse.userID,d.fb.bind(function(err,basicInfo){
											if(err) console.log(err);
											models.PreRegister.create(
												{
													fbExpires 	  : tokenInfo.expires,
													name 		  : basicInfo.name,
													gender 		  : basicInfo.gender,
													normalPic 	  : basicInfo.normalPic,
													thumb 		  : basicInfo.thumb,
													fbId  		  : fbResponse.userID,
													fbAccessToken : tokenInfo.accessToken,
													params 		  : [],
													created_At	  : Date.now()
												},
												d.db.bind(function(err,doc){
													res.json({to : '/register?eat='+tokenInfo.accessToken+'&fbId='+fbResponse.userID});
													if(err) console.log(err);
												})
											)
											fb.setupArchive(tokenInfo.accessToken,d.fb.bind(function(err,archiveInfo){
												models.FBArchive.create({
													fbId 		  : fbResponse.userID,
													createdAt 	  : Date.now(),
													publicProfile : archiveInfo.publicProfile,
													likes 		  : archiveInfo.likes,
													likesIds 	  : archiveInfo.likesIds,
													musicLikes 	  : archiveInfo.musicLikes,
													moviesLikes   : archiveInfo.moviesLikes,
													tvLikes 	  : archiveInfo.tvLikes,
													booksLikes 	  : archiveInfo.booksLikes,
													foodsLikes 	  : archiveInfo.foodsLikes,
													videoGamesLikes: archiveInfo.videoGamesLikes,
													causesLikes   : archiveInfo.causesLikes,
													fosLikes 	  : archiveInfo.fosLikes,
													educationLikes: archiveInfo.educationLikes,
													sportsLikes   : archiveInfo.sportsLikes,
													authorsLikes  : archiveInfo.authorsLikes,
													pstLikes 	  : archiveInfo.pstLikes,
													normalPic 	  : basicInfo.normalPic,
													thumb 		  : basicInfo.thumb,
													friends 	  : archiveInfo.friends,
													friendsIds 	  : archiveInfo.friendsIds
												},d.db.bind(function(err,doc){
													;
												}));
											
											}));
										}));
									}
								}));
							}
							else {

								try{
									var fbAccessToken = doc.fbAccessToken,fbId = doc.fbId;
								} catch(e){
									throw e;
								}
								models.UserDetail.findOne({fbId : fbResponse.userID},'_id accessToken',d.db.bind(function(err,doc){
									if(err || !doc){
										res.json({
											to : '/register?eat='+fbAccessToken+'&fbId='+fbId
										});
									}
									else {
										req.session.userid = doc._id;
										req.session.cookie.maxAge = 27 * 24 * 60 * 60 * 1000;
										res.json({
											to : '/'
										});
									}
								}));
							}
						}));

					}
				}));
			}
			else {
				res.json({
					to : '/sorry.html'
				})
			}
		}));

}

/*------- Registraion Finish , route : POST/registerProfile, status : 1---------*/
/*------- ToDO : Sending verification email , not sure---------*/
module.exports.POSTregisterProfile = function(req,res){

	try{
		v.check(req.body.params).contains(';').len(14,14);
		v.check(req.body.id).isHexadecimal().len(24);
		var	params   = req.body.params.split(';').map(function(e){return parseInt(e)}),
		id 		 = req.body.id;
	} catch(e){
		throw e;
		res.send(400);
		return;
	}

	var isAuthentic = true;

	_.each(params,function(e){
		if(e < 20 || e > 39) isAuthentic = false;
	})

	if(!isAuthentic) {
		throw new Error(req.session.userid + 'misuse/suspicious');
		res.send(400);
		return;
	}
	// Auth End
	models.PreRegister.findOne({_id : id},'fbId',d.db.bind(function(err,doc){
		if(err || !doc) res.send(400);
		else {
			models.PreRegister.findOneAndUpdate(
				{_id : id},
				{
					params : params	
				},
				d.db.bind(function(err,doc){
					if(err) {
						res.send(400);
						return;
					}
					models.FBArchive.findOne({fbId : doc.fbId},'friendsIds likesIds',d.db.bind(function(err,fbResults){
									if(err) {
										res.send(500);
										return;
									}
									models.UserDetail.create({
										preRegId 		: id,
										name 	  		: doc.name,
										params    		: doc.params,
										reqParams 		: [],
										top5Params		: doc.params,
										complients 		: [],
										gender 	  		: doc.gender,
										password  		: doc.password,
										tags 	  		: [],
										followers 		: [],
										following 		: [],
										buddies   		: [],
										allThreads 		: [],
										allNotifications: [],
										images 			: [doc.normalPic],
										normalPic 		: doc.normalPic,
										thumb 			: doc.thumb,
										fbId 			: doc.fbId,
										fbAccessToken 	: doc.fbAccessToken,
										fbExpires 		: doc.fbExpires,
										fbFriends 		: fbResults.friendsIds,
										fbLikes 		: fbResults.likesIds
									},d.db.bind(function(err,doc){
										req.session.userid = doc._id;
										req.session.cookie.maxAge = 59 * 24 * 60 * 60 * 1000;
										res.send(200);
										models.FBArchive.update({fbId : doc.fbId},{user_id : doc._id},d.db.bind(function(err){
											;
										}));
										models.UserProfile.create({
											user_id 		: doc._id, 
											name 			: doc.name,
											picUrl 			: doc.normalPic,
											newThreads  	: [],
											newNotifications:[]
										},d.db.bind(function(err,doc){
											if(err) console.log(err);
											models.UserDetail.findOneAndUpdate({_id : doc.user_id},{userp_id : doc._id},function(err,d){
												if(err) console.log(err);
											})
										}));
										models.UserStatus.create({
											user_id : doc._id
										},d.db.bind(function(err,doc){
											if(err) console.log(err);
										}));
									}));
								}));	
						}));
			}
		}))
}

/*------- The default home , route : GET/home, status : 1---------*/
module.exports.home= function(req,res){
	var userId = req.session.userid;

	models.UserProfile.findOne({user_id : userId}).select('-user_id -_id -__v').exec(d.db.bind(function(err,data){
		res.render('home',{
			profileModelData 	 : data
		});	
	}));
};

/*------- Get user's mini profile, route : GET/getMiniProfile, status : 1---------*/
module.exports.getMiniProfile = function(req,res){
	models.UserProfile.findOne({user_id : req.session.userid}).select('-user_id -_id -__v').exec(d.db.bind(function(err,data){
		res.json(data);
	}));
};

/*------- Get latest User notfications, route : GET/getlatestNotifications, status : 1---------*/
module.exports.getLatestNotifications = function(req,res){
	var userId = req.session.userid;
	models.UserProfile.findOne({user_id : userId},'newNotifications',d.db.bind(function(err,doc){
		if(err) {
			res.send (500,{error : 'Please re-login and try again . Report if problem persists'});
			console.log(err);
		}
		else {

			res.json(doc.newNotifications);
			/*
			var data = [];

			async.map(
				doc.newNotifications,
				function(notification,cbt){
					data.push(_.pick(notification,'content','actionUrl'));
					cbt(null,null);
				},
				function(err,results){
					res.json(data);
				}
			);
			*/
			//clear up the new notifications
		}
	}));
}

/*------- Clear newNotfications in Userprofile, route : GET/clearNotifications, status : 1---------*/
module.exports.clearNotifications = function(req,res){
	var userId = req.session.userid;
	models.UserProfile.update({user_id : userId},{newNotifications : []},d.db.bind(function(err){
		if(err) res.send(500);
		else res.send(200);
	}));
}

/*------- Clear thread notfications in userProfile, route : GET/clearNotifications/:threadId, status : 1---------*/
module.exports.clearThreadNotifications = function(req,res){
	try{
		v.check(req.params.threadId).notNull().len(24);
	var threadId = req.params.threadId,
		userId 	 = req.session.userid;
	} catch(e){
		throw new Error(req.session.userid + 'misuse/suspicious')
		res.send(400);
		return;
	}
		models.UserProfile.findOne({user_id : userId,'newThreads.threadId' : threadId},d.db.bind(function(err,doc){
			if(err || !doc) res.send(400);
			else {
				models.UserProfile.update({user_id : userId},{$pull : {newThreads : {threadId : threadId}}},d.db.bind(function(err){
					if(err) res.send(400);
					else res.send(200);

				}));
			}
		}));
}

/*------- Called In : homeCenterItemView > onShow, route : POST/initiateChat, status : 1---------*/
module.exports.initiateChat = function(req,res){

	try{
		v.check(req.body.twinSocketId).notNull();
		v.check(req.body.mySocketId).notNull();
		var userId 		 = req.session.userid;
		var twinSocketId = req.body.twinSocketId; 
		var mySocketId   = req.body.mySocketId;

	} catch(e){
		throw new Error(req.session.userid + 'misuse/suspicious');
		res.send(400);
		return;
	}
	models.OnlineUsers.update({socketId : mySocketId},{avlblStatus : false},d.db.bind(function(err){
		if(err) console.log(err);
	}))

	models.OnlineUsers.getUserIdBySocketId(twinSocketId,d.db.bind(function(err,twinId){
		if(err) {
			res.send(400);
		}
		else {
			models.UserDetail.getNamesAndPicsByUserIds([userId,twinId],1,d.db.bind(function(err,names){
				models.Chats.create({
					type 		   : 0,
					names 		   : names,
					userId   	   : userId,
					twinId 	 	   : twinId,
					userSocketId   : mySocketId,
					twinSocketId   : twinSocketId,
					messages 	   : [],
					revealRequests : [],
					revealed 	   : false,
					closed 		   : false,
					randomNo 	   : Math.random(),
					questionPool   : [],
					askedPssts	   : [],
					askedQuestions : [],
					userAnswers    : [],
					twinAnswers    : []

				},d.db.bind(function(err,doc){
					if(err) {
						res.send(400)
					}
					else {
						var chatId = doc._id;
						res.json({
							chatId : doc._id.toHexString()
						});
						models.UserDetail.find({_id : {$in : [userId,twinId]}},'params',d.db.bind(function(err,doc){
							var intersectionArr = _.intersection(doc[0].params,doc[1].params);
								intersectionArr.push(0);
								models.Questions.find({pid : {$in : intersectionArr}},d.db.bind(function(err,docs){
									models.Chats.update({_id : chatId},{questionPool : docs},d.db.bind(function(err){
										if(err) console.log(err);
									}));
								}))
								
						}));
					}
				}));
			}));
		}
	}));

};
/*------- Called In : chatCompositeView > sayBye, route : GET/closeChat/chatId, status : 1---------*/
module.exports.closeChat = function(req,res){
	try{
		v.check(req.params.id).isHexadecimal().len(24);
		var chatId = req.params.id;
		var userId = req.session.userid;
	} catch(e){
		res.send(400);
		return;
	}

	models.Chats.findOne({_id : chatId},'userId twinId',d.db.bind(function(err,doc){
		if(err || !doc) res.send(500);
		else {
			if(doc.userId.equals(userId) || doc.twinId.equals(userId)){
				models.Chats.findOneAndUpdate({_id : chatId},{closed : true,ended : Date.now()},d.db.bind(function(err,doc){
					if(err || !doc) res.send(500)
					else {
						var twinId = doc.userId.equals(userId) ? doc.twinId : doc.userId;
						var twinSocketId = null;
						if(doc.twinId.equals(twinId)) twinSocketId = doc.twinSocketId;
						else twinSocketId = doc.userSocketId;
						sendToSocket.send(
							'emit',
							{
								socketId 	: twinSocketId,
								eventName 	: 'closeChat',
								emitData  	: {}
							},
							noop
						);
						res.send(200);

						//updateScore
						models.UserDetail.updateScore(4,{userId : userId,chatSpan : ((doc.started - doc.ended)/60000)});

						//Copy Chat messages to threads if both are in contact
						models.Threads.update({participants : {$all : [userId,twinId]}},{$pushAll : {messages : doc.messages}},d.db.bind(function(err){
							;
						}));
					}

				}));
			}
			else res.send(500);
		}
	}));

}

/*------- Called In : chatClosedItemView > sendFeedback , route : POST/sendFeedback, status : 1---------*/
module.exports.chatFeedback = function(req,res){

	try{
		v.check(req.params.id).isHexadecimal().len(24);
		v.check(req.body.chatRating).notNull().len(1,1);
		v.check(req.body.choices).len(0,18);
	var userId     = req.session.userid;
	    chatId     = req.params.id,
	    choice 	   = req.body.chatRating,
	    traits 	   = req.body.choices;
	    choice     = parseInt(choice);
		traits     = traits.length > 0 ? traits.split(':').map(function(i){return parseInt(i)}) : [];
		testimony  = v.sanitize(v.sanitize(req.body.testimony).xss()).escape();
	} catch(e){
		res.send(400);
		return;
	}

	models.ObjectIdFromString(chatId,d.db.bind(function(chatIdObject){
		chatId = chatIdObject;
		models.Chats.findOne({_id : chatId},'userId twinId',d.db.bind(function(err,doc){
			if(err) res.send(500);
			else {
				var twinId = doc.userId.equals(userId) ? doc.twinId : doc.userId;
				if(doc.userId.equals(userId)){
					if(testimony.length){
						models.Chats.update({_id : chatId},{userRating : choice,userTestimony : testimony},d.db.bind(function(err,doc){
							if(err) res.send(500)
						}));
					} 
					else{
						models.Chats.update({_id : chatId},{userRating : choice},d.db.bind(function(err,doc){
							if(err) res.send(500)
						}));
					}
				}
				else {
					if(testimony.length){
						models.Chats.update({_id : chatId},{twinRating : choice,twinTestimony : testimony},d.db.bind(function(err,doc){
							if(err) res.send(500);
						}));
					}
					else {
						models.Chats.update({_id : chatId},{twinRating : choice},d.db.bind(function(err,doc){
							if(err) res.send(500)
						}));
					}
				}
				var updateCompliments = function(t,cbt){
					if(_.isUndefined(t)) return;
					models.UserDetail.findOne({_id : twinId,'allCompliments.pid' : t},d.db.bind(function(err,doc){
						if(doc === null){
							models.UserDetail.update({_id : twinId},{$addToSet : {allCompliments : {pid : t , checked : 1}}},d.db.bind(function(err){
								cbt(err);
							}));
						}
						else {
							models.UserDetail.update({_id : twinId,'allCompliments.pid' : t},{$inc : {'allCompliments.$.checked' : 1}},d.db.bind(function(err,doc){
								cbt(err);
							}));
						}
					}));
				}
				console.log(traits);
				async.each(traits,updateCompliments,d.db.bind(function(err){
					if(err) {
						console.log(err);
						res.send(500);
					}
					else {
						models.UserDetail.aggregate(
							{$match 	: {_id : twinId}},
							{$project 	: {allCompliments : 1}},
							{$unwind 	: '$allCompliments'},
							{$sort 		: {'allCompliments.checked' : -1}},
							{$limit 	: 5},
							{$group 	: {_id : '$_id',top5 : {$addToSet : '$allCompliments.pid'}}},
							d.db.bind(function(err,doc){
								if(!doc.length){
									res.send(200);
									return;
								}
								models.UserDetail.findOneAndUpdate({_id : twinId},{top5Compliments : doc[0].top5,$push : {testimonies : {givenBy : userId, chatId : chatId, testimony : testimony}}},d.db.bind(function(err,doc){
									if(err) ;
									else {
										res.send(200);
										//Update Score 
										models.UserDetail.updateScore(
											1,
											{
												userId 	    : doc._id,
												twinId      : twinId,
												chatRating  : choice,
												traitsLength: traits.length
											}
										);
										//Send notification to twin
										if(traits.length > 0){
											models.UserDetail.sendNotification(2,{
												userId 		: twinId,
												compliments : '%'+traits.join('%') 
											},function(isDone){
												if(!isDone) throw new Error('Error sending compliments notification to twin')
											})
										}
									}
							}));
						}));
					}
				}));	
			}
		}));
	}));
}

/*------- Called In : controller > initChat , route : GET/getTwinProfile/:chatId, status : 1---------*/
module.exports.getTwinProfile = function(req,res){
	//get socket id 
	try{
		v.check(req.params.id).isHexadecimal().len(24);
		var chatId = req.params.id;
		var userId = req.session.userid;
	} catch(e){
		res.send(400);
		return;
	}

	models.Chats.findOne({_id : chatId,closed : false},'revealed userId twinId',d.db.bind(function(err,doc){
		if(err || !doc) {
			res.send(500);
			return;
		}
		else {
			var twinId = doc.userId.equals(userId) ? doc.twinId : doc.userId;
			models.UserDetail.findOne({_id : twinId},'params',d.db.bind(function(err,doc2){
				if(err) res.send(500);
				else {
					res.json({
						revealed 	: doc.revealed,
						interests	: doc2.params
					});
				}
			}));
		}
	}));
}

/*------- Called In : twinProfileItemView > sendRevealRequest, route : POST/sendRevealRequest, status : 1---------*/
module.exports.sendRevealRequest = function(req,res){

	try{
		v.check(req.body.chatId).notNull().isHexadecimal().len(24);
		v.check(req.body.twinSocketId).notNull().len(10,50);
		v.check(req.body.mySocketId).notNull(10,50);
		var userId 		 = req.session.userid,
			twinId 		 = null,
			chatId 		 = req.body.chatId,
			twinSocketId = req.body.twinSocketId,
			mySocketId   = req.body.mySocketId;
	} catch(e){
		res.send(400);
	}

	models.OnlineUsers.getUserIdBySocketId(twinSocketId,d.db.bind(function(err,twinUserId){
		models.Chats.findOne({_id : chatId},'revealRequests revealed',d.db.bind(function(err,doc){
			if(err) res.send(500)
			else if(doc.revealed) res.send(400);
			else {
				if(doc.revealRequests.length > 0) {
					twinId = twinUserId;
					models.Chats.findOneAndUpdate({_id : chatId},{$push : {revealRequests : userId},revealed : true},d.db.bind(function(err,doc){
						if(err) ;
						else{
							models.UserDetail.getNamesAndPostersByUserIds([userId,twinId],2,function(err,results){
								if(err) res.err(500);
								else {
									sendToSocket.send(
										'emit',
										{
											socketId 	: mySocketId,
											eventName 	: 'readyToReveal',
											emitData	: results[1]
										},
										noop
									);
									sendToSocket.send(
										'emit',
										{
											socketId 	: twinSocketId,
											eventName	: 'readyToReveal',
											emitData 	: results[0] 
										},
										noop
									);
									res.send(200);
								}
							});
						}
					}));
				}
				else {
					models.Chats.findOneAndUpdate({_id : chatId},{$push : {revealRequests : userId}},d.db.bind(function(err,doc){
						res.send(200);
					}));
				}
			}
		}));	
	}));
	
}

/*------- Called In : miniProfileItemView, route : GET/getMyPic, status : 1---------*/
module.exports.getMyPic    = function(req,res){
	if(req.session.userid){
		models.UserProfile.findOne({user_id : req.session.userid},'picUrl',d.db.bind(function(err,doc){
			res.sendfile(path.resolve('public'+doc.picUrl)); //relative to app.js
		}));
	}
	else{
		res.send(400);
	}
};

/*------- Called In : miniProfileItemView > initPicUpload , route : POST/setMyPic, status : 0---------*/
/* ToDo : Manage permission settings of setting pic with rank*/
module.exports.setMyPic	   = function(req,res){
	try{
		var extension  = '.'+req.files.myPic.path.split('.').pop();
	}
	catch(e){
		extension = '.jpg';
	}
	var userId = req.session.userid;
	var newPicName = req.session.userid+";"+Date.now()+extension
	   ,newPath    = path.resolve('public/img/originals/'+newPicName);
	fs.rename(req.files.myPic.path,newPath,d.app.bind(function(err){
		if(err) console.log(err)
	}));
	var	image   = ik(newPath),
		methods 	= {
		resizeAndCrop: function(img, out, options, cb) {
       					 img.geometry(options.width, options.height, "^")
            			.gravity(options.gravity || "center")
            			.crop(options.width, options.height)
            			.write(out, cb);
    	},
    	thumb: function(img, out, options, cb) {
        		img.thumb(options.width,
            	options.height,
            	out,
            	options.quality || 80, cb);
   		 }
		},
	    ppicName = req.session.userid+';'+Date.now()+extension,
	    thumbName= req.session.userid+';'+Date.now()+extension,
		types = {
			ppic : {
				out : path.resolve('public/img/ppics/'+ppicName),
				opt : {
					height : 120,
					width  : 120
				}
			},
			thumb: {
				out : path.resolve('public/img/thumbs/'+thumbName),
				opt : {
					height : 50,
					width  : 50
				}
			}
		};

	if(validate()){

		methods.resizeAndCrop(image,types.ppic.out,types.ppic.opt,function(err){
			 models.UserDetail.findOneAndUpdate({_id : req.session.userid},
			 	{thumb  : '/img/thumbs/'+thumbName,normalPic : '/img/ppics/'+ppicName},
			 	d.db.bind(function(err,doc){
			 		if(err) console.log(err);
			 	}));
			 models.UserProfile.findOneAndUpdate({user_id : req.session.userid},
			 									 {picUrl : '/img/ppics/'+ppicName},
			 									 {select : 'picUrl'},
			 								d.db.bind(function(err,doc){
			 									 if(err) console.log(err);
			 									 else {
			 									 	res.json({
			 									 		"responseCode" : 1,
			 									 		"picUrl"       : doc.picUrl
			 									 	})
			 									 }
			 }))
		});

		methods.thumb(image,types.thumb.out,types.thumb.opt,d.app.bind(function(err){
			if(err) console.log(err);
		}));

	}
	else {
		res.send(400,"You are better than this");
	}

    function validate(){
		var ok = true;
		allowed = ['image/png','image/jpeg'];
		ok =  allowed.indexOf(req.files.myPic.type) != -1 ;
		if(!ok) return ok;
		ok = req.files.myPic.size < 2100000;
		return ok;
	};
//End of route
};

/*------- Called In : newMessages.js collection, route : GET/getLatestThreads, status : 1---------*/
module.exports.getLatestThreads = function(req,res){
	//add Authentication
	var userId = req.session.userid;
	models.UserProfile.getLatestThreads(userId,d.db.bind(function(doc){
		doc ? res.json(doc.newThreads.reverse()) : res.send(400) ;
	}));
}

/*------- Called In : newMessagesCompositeView.js , route : GET/getRecentThreads, status : 1---------*/
module.exports.getRecentThreads = function(req,res){
	var userId = req.session.userid;
	models.UserDetail.aggregate(
		{$match 	: {_id : userId}},
		{$project 	: {allThreads : 1,_id : 0}},
		{$unwind 	: '$allThreads'},
		{$project   : {'allThreads.lastUpdated' : 1,'allThreads.lastMessage' : 1,'allThreads.threadId' : 1,'allThreads.twinId' : 1}},
		{$sort 		: {'allThreads.lastUpdated' : -1}},
		{$limit 	: 5},
		d.db.bind(function(err,data){
			async.mapSeries(data,
				d.db.bind(function(doc,cbt){
					models.UserDetail.getNamesAndPicsByUserIds([doc.allThreads.twinId],2,function(err,results){
						if(err) cbt(err,null)
						else {
							cbt(null,{
								threadId   : doc.allThreads.threadId,
								senderName : results[0].name,
								senderPic  : results[0].picUrl,
								content    : doc.allThreads.lastMessage,
								lastUpdated: doc.allThreads.lastUpdated
							})
						}
					})
				}),
				function(err,results){
					if(err) res.send(500);
					else res.json(results);
				}
			);
		})
	);
}

/*------- Called In : controller > getThread, route : GET/thread/:threadId, status : 1---------*/
module.exports.getThreadById = function(req,res){
	try{
		v.check(req.params.threadId).notNull().len(24).isHexadecimal();
		var userId   = req.session.userid,
			threadId = req.params.threadId;
	} catch(e){
		res.send(400);
		return;
	}

	models.Threads.findOne({_id : threadId},function(err,doc){
		if(err || !doc) {
			res.send(500);
			return;
		}

		if(doc.participants[0].equals(userId) || doc.participants[1].equals(userId)){
			var twinId = doc.participants[0].equals(userId) ? doc.participants[1] : doc.participants[0];
			//get messages data 
			var recentMessages = req.query.loadAll && req.query.loadAll === '1' ? doc.messages :doc.messages.slice(-75);

			var data = 	_.map(recentMessages,function(msg){
							if(msg.from.equals(twinId)){
								return {
									sender   : "twin",
									message  : msg.content,
									timeSent : msg.timeSent,
								}
							}
							else {
								return {
									sender   : "me",
									message  : msg.content,
									timeSent : msg.timeSent,
								}
							}
						}); 
			//check if thread is shout response (expires ?) and send that data in threadInfo
			var canReport = false;
			if(doc.expires && doc.shouterId !== null &&userId.equals(doc.shouterId)){
				canReport = true;
			}

			var contactsState = {};
			if(doc.contactRequests.length === 1){
				if(doc.contactRequests[0].equals(userId)) {
					 contactsState.sent 	= true;
					 contactsState.fresh 	= false; 
				}
				else {
					contactsState.sent 	= false;
					contactsState.fresh = false
				}
			}
			else if(doc.contactRequests.length === 0) {
				contactsState.sent  = false;
				contactsState.fresh = true;
			}
			//get thread info , twin name,pic and his last status 
			async.parallel(
				[
					function(cbt){
						models.UserDetail.getNamesAndPicsByUserIds([twinId],2,cbt);
					},
					function(cbt){
						models.UserStatus.findOne({user_id : twinId},'status lastOnline',cbt);
					}
				],
				d.db.bind(function(err,results){
					if(err) res.send(400);
					else {
						res.json({
							data : data,
							threadInfo : {
								twinPicUrl   	: results[0][0].picUrl,
								twinName     	: results[0][0].name,
								twinLastSeen 	: results[1].lastOnline,
								online 		 	: results[1].status,
								isExpires    	: doc.isExpires,
								expires 	 	: doc.expires,
								contactRequests : contactsState,
								threadId 	 	: doc._id.toHexString(),
								canReport    	: canReport
							}
						});
					}
				})
			)
		}
		else res.send(403);

	});
}

/*------- Called In : messagesCompositeView > sendMessage, route : POST/sendThreadMessage, status : 1---------*/
module.exports.sendMessage = function(req,res){
	try{
		v.check(req.body.threadId).isHexadecimal().len(24);
		v.check(req.body.message).notNull();
	var userId   = req.session.userid,
		threadId = req.body.threadId,
		message  = v.sanitize(v.sanitize(req.body.message).xss()).escape();
	}
	catch(e){
		res.send(400);
	}
	//add to threadMessages
	models.Threads.findOne({_id : threadId},'participants',d.db.bind(function(err,doc){
		if(!(doc.participants[0].equals(userId) || doc.participants[1].equals(userId))){
			res.send(400);
			return;
		}
		var twinId = doc.participants[0].equals(userId) ? doc.participants[1] : doc.participants[0];
		models.Threads.update(
			{_id : threadId},
			{$push :
				 {messages : {
				 	content : message,
				 	from 	: userId,
				 	to 		: twinId,
				 	timeSent: Date.now()
				 }}
			},
			d.db.bind(function(err,doc){
				if(err) res.send(400);
				else res.send(200);
			})
		);

		models.UserDetail.update({_id : userId,'allThreads.threadId' : threadId},{'allThreads.$.lastMessage' : message,'allThreads.$.lastUpdated' : Date.now()},d.db.bind(function(err){
			if(err) console.log(err);
		}));

		models.UserDetail.getNamesAndPicsByUserIds([userId],2,d.db.bind(function(err,result){
			var name   = result[0].name,
				picUrl = result[0].picUrl;
			//Check if already exisits , if yes, then update content,timeSent else put
			models.UserProfile.findOne({user_id : twinId,'newThreads.threadId' : threadId},d.db.bind(function(err,doc){
				if(err) ;
				else if(!doc) {
					models.UserProfile.update(
						{ user_id : twinId },
						{
							$push : {
								newThreads : {
									threadId : threadId,
									sender   : name,
									content  : message,
									timeSent : Date.now,
									senderPic: picUrl
								}
							}
						},
						d.db.bind(function(err){
							if(err) console.log(err);
						})
					)

					models.UserDetail.update({_id : twinId,'allThreads.threadId' : threadId},{'allThreads.$.lastMessage' : message,'allThreads.$.lastUpdated' : Date.now()},d.db.bind(function(err){
						if(err) console.log(err);
					}));
				}
				else {
					models.UserProfile.update(
						{user_id : twinId,'newThreads.threadId' : threadId},
						{'newThreads.$.content' : message},
						d.db.bind(function(err){
							;
						}
					));

					models.UserDetail.update({_id : twinId,'allThreads.threadId' : threadId},{'allThreads.$.lastMessage' : message},d.db.bind(function(err){
						;
					}));
				}
			}))
		}));
	}));	
}

/*------- Called In : messagesCompositeView , route : GET/checkThreadStatus/:threadId, status : 1---------*/
module.exports.checkThreadStatus = function(req,res){
	try{
		v.check(req.params.threadId).notNull().isHexadecimal().len(24);
		v.check(req.query.lastTimeSent).notNull();
	var threadId   	  = req.params.threadId,
		lastTimeSent  = parseInt(req.query.lastTimeSent),
		userId        = req.session.userid; 
	} catch(e){
		res.send(400);
		return;
	}
		models.ObjectIdFromString(threadId,function(threadID){
			models.Threads.findOne({_id : threadID},'participants',d.db.bind(function(err,doc){
				if(err || !doc) {
					console.log(err);
					res.send(400);
					return;
				}
				if(!(doc.participants[0].equals(userId) || doc.participants[1].equals(userId))){
					res.send(400);
					return;
				}
				var twinId = doc.participants[0].equals(userId) ? doc.participants[1] : doc.participants[0];
				//Find twinStatus and Latest twinMessages
				async.parallel([
					function(cbt){
						models.UserDetail.getNamesAndPicsByUserIds([userId,twinId],2,cbt);	
					},
					function(cbt){
						models.Threads.aggregate(
							{$match  : {_id : threadID}},
							{$project: {messages : 1,_id : 0}},
					 		{$unwind : '$messages'},
					 		{$match  : {'messages.timeSent' : {$gt : lastTimeSent}, 'messages.from' : twinId}},
							{$project: {'messages.content' : 1,'messages.timeSent' : 1}},
							{$sort   : {'messages.timeSent' : 1}},
					 		cbt
						)	
					},
					function(cbt){
						models.UserStatus.findOne({user_id : twinId},'status lastOnline',cbt);
					}
				],d.db.bind(function(err,results){
					if(err) res.send(400);
					else {
						var data = [];
						if(!!results[1].length){
							data = _.map(results[1],function(msg){
									return {
											sender   : 'twin',
											message  : msg.messages.content,
											timeSent : msg.messages.timeSent,
									}
							});
						}
						res.json({
							contactStatus : results[2].status ? results[2].status : results[2].lastOnline ,
							data 		  : data
						});
					}
				}));

			}))
		})
}

/*------- Called In : models/myActivity.js   , route : GET/getMyActivity, status : 1---------*/
module.exports.getMyActivity = function(req,res){
	var userId = req.session.userid;
	async.parallel([
		function(cbt){
			models.Shouts.find({shouter_id : userId},'_id content created_At').sort({created_At : -1}).limit(5).exec(cbt)
		},
		function(cbt){
			models.Chats.find({$or : [{userId : userId},{twinId : userId}],closed : true},'_id revealed ended names messages').sort({ended : -1}).limit(5).exec(function(err,docs){
				if(err) cbt(err,null);
				else {
					var modDocs = _.map(docs,function(doc){
						var obj = {};
						if(doc.revealed === true) obj.names = doc.names;
						obj.revealed       = doc.revealed;
						obj.ended    	   = doc.ended;
						obj.messagesLength = doc.messages.length;
						obj._id 		   = doc._id;
						return obj;
					})
					cbt(null,modDocs);
				}

			})
		}
		],d.db.bind(function(err,results){
			if(err) res.send(500);
			else res.json({
				recentShouts : results[0],
				recentChats  : results[1]
			});
		}))
}

/*------- Called In :  myActivityItemView, route : POST/modifyShout/:shoutId, status : 1---------*/
module.exports.modifyShout = function(req,res){
	//Check if shout is shouted by user , check other stuff like if shout exisits etc..
	var userId  = req.session.userid,
	    shoutId = req.params.shoutId,
	    type 	= req.body.type;
	if(type === 'edit'){
		var shoutContent = req.body.shoutContent;
			models.Shouts.update({_id : shoutId,shouter_id : userId},{content : shoutContent},d.db.bind(function(err){
				if(err) res.send(500);
				else res.send(200);
			}));
	}
	else if(type === 'delete'){
		models.Shouts.findOneAndRemove({_id : shoutId,shouter_id : userId},d.db.bind(function(err,doc){
		_.each(doc.threads,function(thread){
			models.Threads.remove({_id : thread.threadId},function(err){
				if(err) console.log(err);
			})
			models.UserDetail.update({_id : doc.shouter_id,'allThreads.threadId' : thread.threadId},{$pull : {allThreads : 'allThreads.$'}},function(err){
				if(err) console.log(err);
			})
			models.UserDetail.update({_id : thread.responderId,'allThreads.threadId' : thread.threadId},{$pull : {allThreads : 'allThreads.$'}},function(err){
				if(err) console.log(err);
			})
		});
			if(err) res.send(500);
			else res.send(200);
		}));
	}
	else res.send(400);
}

/*------- Called In : collections/contacts.js, route : GET/getContacts, status : 1---------*/
module.exports.getContacts = function(req,res){
	var userId = req.session.userid;

	models.UserDetail.getAllBuddies(userId,d.db.bind(function(err,doc){
		if(err) res.send(500);
		else{
			res.json(doc);
		}
	}));
}

/*------- Called In : models/settings.js, route : GET/settings, status : 1---------*/
module.exports.getSettings = function(req,res){
	var userId = req.session.userid;
	//get name , interests[],clearSlate
	models.UserDetail.findOne({_id : userId},'name clearSlate params',d.db.bind(function(err,doc){
		if(err) {
			res.send(400);
			return;
		}
		res.json({
			name 	  : doc.name,
			interests : doc.params,
			clearSlate: !!doc.clearSlate
		});
	}));
}

/*------- Called In : settingsItemView, route : GET/settings/update?type=, status : 1---------*/
module.exports.setSettings = function(req,res){
	try{
		v.check(req.query.type).notNull().len(3,11);
	var userId = req.session.userid,
		type   = req.query.type;
	} catch(e){
		res.send(400);
		return;
	}
		switch(type) {
			case 'name' : 
				//validate name;
				var name = req.query.name;
				try{
					v.check(name).regex(/^[a-z ,.'-]+$/i);
				} catch(e){
					res.send(400);
					return;
				}
				async.parallel([
					function(cbt){
						models.UserDetail.update({_id : userId},{name : name},cbt)
					},
					function(cbt){
						models.UserProfile.update({user_id : userId},{name : name},cbt);
					}
					],d.db.bind(function(err,results){
					if(err || !results.length) res.send(500);
					else res.send(200);
				}))
				break;
			case 'interests' :
				var params = req.query.params; //validate;
					params = params.split(';').map(function(i){ return parseInt(i)});
				models.UserDetail.update({_id : userId},{params : params},d.db.bind(function(err){
					if(err) {
						res.send(500);
						console.log(err);
					}
					else res.send(200);
				}));
				break;
			case 'fbLikes' :
				models.UserDetail.findOne({_id : userId},'fbAccessToken',d.db.bind(function(err,doc){
					if(err) {
						console.log(err);
						res.send(500);
						return;
					}
					fb.setupArchive(doc.fbAccessToken,function(err,fbResults){
						async.parallel([
							function(cbt){
								models.UserDetail.update({_id : userId},{fbFriends : fbResults.friendsIds,fbLikes : fbResults.likesIds},cbt);
							},
							function(cbt){
								models.FBArchive.update({user_id : userId},
									{
										likes 	  	  : fbResults.likes,
 										likesIds 	  : fbResults.likesIds,
										musicLikes 	  : fbResults.musicLikes,
										moviesLikes	  : fbResults.moviesLikes,
										tvLikes 	  : fbResults.tvLikes,
										booksLikes 	  : fbResults.booksLikes,
										foodsLikes 	  : fbResults.foodsLikes,
										videoGamesLikes : fbResults.videoGamesLikes,
										causesLikes   : fbResults.causesLikes,
										fosLikes 	  : fbResults.fosLikes,
										educationLikes: fbResults.educationLikes,
										sportsLikes   : fbResults.sportsLikes,
										authorsLikes  : fbResults.authorsLikes,
										pstLikes 	  : fbResults.pstLikes,
										friends 	  : fbResults.friends,
										friendsIds    :  fbResults.friendsIds
									},
									cbt
								)
							}
							],d.db.bind(function(err,results){
								if(err || !results.length) {
									 res.send(500);
								}
								else res.send(200);
							}))
					})
				}))
				break;
			case 'clearSlate' : 
				models.UserDetail.findOne({_id : userId},'clearSlate',d.db.bind(function(err,doc){
					if(doc.clearSlate !== false) {
						res.send(500);
						return;
					}
					else {
						models.UserDetail.update({_id : userId},{clearSlate : true,rank : 3,karma : 50},function(err){
							if(err) {
								res.send(500);
								return;
							}
							res.send(200);
						})
						models.UserProfile.update({user_id : userId},{rank : 3},function(err){
							;
						})
					}
				}))
				break;
			default : 
				res.send(200);
				break;
		}
}

/*------- Called In : shoutItemView.js, route : GET/shoutRespond, status : 1---------*/
module.exports.shoutRespond = function(req,res){
	try{
		v.check(req.body.shoutId).notNull().isHexadecimal().len(24);
		v.check(req.body.mySocketId).notNull();
	var userId 	  = req.session.userid,
		shoutId   = req.body.shoutId,
		mySocketId= req.body.mySocketId;
	} catch(e){
		res.send(400);
		return;
	}

	models.Shouts.findOne({_id : shoutId},'shouter_id',function(err,doc){
		if(err || !doc){
			res.send(500);
			return;
		}
		else {
			var shouterId = doc.shouter_id;

			models.OnlineUsers.findOne({user_id : shouterId, avlblStatus : true},d.db.bind(function(err,doc){
				if(err) {
					res.send(500);
					return;
				}
				else if(!doc){
					res.json({
						error : 1,
						msg   : 'The shouter is not online. Send a message'
					});
				}
				else {
					sendToSocket.send(
						'emit',
						{
							eventName : 'AreYouReady',
							emitData  : {
								shoutId 		  : shoutId,
								initiatorSocketId : mySocketId,
								initiatorId 	  : userId,
								type 			  : 'shoutRespond' 
							},
							socketId  : doc.socketId
						},
						function(isOk){
							if(!isOk){
								res.json({
									error : 1,
									msg   : 'The other person just went offline, please try again'
								});
							}
							else {
								res.json({
									error 		  : 0,
									twinSocketId  : doc.socketId
								});

								sendToSocket.send(
									'on',
									{
										eventName 	 : 'twinAreYouReadyResponse',
										twinSocketId : doc.socketId,
										socketId 	 : mySocketId
									},
									noop
								);
							}
						}
					)
				}
			}));
		}

	});
}

module.exports.shoutRespondInitiateChat = function(req,res){
	try{
		var shouterId			= req.session.userid,
			userId 				= req.body.twinId,
			shoutId 			= req.body.shoutId,
			shouterSocketId 	= req.body.mySocketId,   //Caution here , my stupidity & laziness area
			mySocketId 			= req.body.twinSocketId;

	} catch(e){
		res.send(400);
		return;
	}
	
	models.UserDetail.getNamesAndPicsByUserIds([userId,shouterId],1,d.db.bind(function(err,names){
		models.Chats.create({
			type  		: 1, //0 for normal , 1 for shoutresponse
			names 		: names,
			userId 		: userId,
			twinId 		: shouterId,
			userSocketId: mySocketId,
			twinSocketId: shouterSocketId,
			messages	: [],
			revealRequests : [],
			revealed  	: false,
			closed 		: false,
			randomNo 	: Math.random(),
			questionPool: [],
			askedQuestions : [],
			askedPssts 	: [],
			userAnswers : [],
			twinAnswers : [],
			shouterId 	: shouterId,
			shoutId 	: shoutId
		},d.db.bind(function(err,doc){
			var chatId = doc._id;
			res.json({
				error : 0,
				chatId: doc._id.toHexString()
			});
			//Update score
			models.UserDetail.updateScore(2,{userId : userId});
			//Fill the pool questions
			models.UserDetail.find({_id : {$in : [userId,shouterId]}},'params',d.db.bind(function(err,doc){
				if(err) console.log(err);
				else {
					var intersectionArr = _.intersection(doc[0].params,doc[1].params);
						intersectionArr.push(0);
					models.Questions.find({pid : {$in : intersectionArr}},d.db.bind(function(err,docs){
						models.Chats.update({_id : chatId},{questionPool : docs},function(err){
							;
						})
					}))
				}
			}));

			models.Shouts.findOneAndUpdate({_id : shoutId},{$push : {responders : userId}},d.db.bind(function(err,doc){
				;
			}));

			models.OnlineUsers.update({socketId : {$in : [mySocketId,shouterSocketId]}},{avlblStatus : false},{multi : true},d.db.bind(function(x,y,z){
				;
			}));
		}));
	}));
}

/*------- Called In : shoutItemView.js, route : POST/shoutRespondSubmit, status : 1---------*/
module.exports.shoutRespondSubmit = function(req,res){
	try{
		v.check(req.body.message).notNull().len(0,140);
		v.check(req.body.shoutId).notNull().isHexadecimal().len(24);
	var message = v.sanitize(v.sanitize(req.body.message).xss()).escape(),
		shoutId = req.body.shoutId,
	    userId  = req.session.userid;
	} catch(e){
		res.send(400);
		return;
	}
	if(!(0 < message.length < 140)) {
		res.send(400);
		return;
	}
	else {
		models.Shouts.findOne({_id : shoutId,responders : {$in : [userId]}},function(err,doc){
			if(!doc){
				models.Shouts.findOneAndUpdate({_id : shoutId},{$push : {responders : userId}},d.db.bind(function(err,doc){
					if(err || !doc) {
						res.send(500);
						return;
					}
					else {
						var shouterId = doc.shouter_id;
						    date  	  = new Date(),
							year 	  = date.getFullYear(),
							month 	  = date.getMonth(),
							date 	  = date.getDate(),
							expiresOn = new Date(year,month,date+3).valueOf();

						models.Threads.findOne({participants : {$all : [userId,shouterId]}},d.db.bind(function(err,doc){
							if(err || !doc){
								models.Threads.create({
									isExpires 	 : true,
									participants : [userId,shouterId],
									messages 	 : [{
										content : message,
										from 	: userId,
										to 		: shouterId
									}],
									expires 	: expiresOn,
									shouterId 	: shouterId,
									shoutId 	: shoutId,
									contactRequests : []
								},d.db.bind(function(err,doc){
									if(err) {
										console.log(err);
										res.send(500);
										return;
									}
									var threadId = doc._id;
									res.json({
										threadId : threadId
									});
									models.UserProfile.updateShoutRespondSubmit(threadId,message,userId,shouterId,'newThread');
								}));
							}
							else {
								var threadId = doc._id;
									res.json({
										threadId : threadId
									});
									models.Threads.update(
										{_id : threadId},
										{$push : {
											messages : {
												content : '<b>Response to your shout :</b>'+message,
												from    : userId,
												to 		: shouterId
												}
											}
										},
										d.db.bind(function(err){
											;
										})
									);
									models.UserProfile.updateShoutRespondSubmit(threadId,message,userId,shouterId,'oldThread');
							}
						}))
					}
				}));
			}
			else {
				res.json({
					error : 'You already responded to this shout.'
				});
			}
		})
	}

}

/*------- Called In : chatCompositeView.js, route : POST/sendChatMessage, status : 1---------*/
module.exports.sendChatMessage = function(req,res){
	try{
		v.check(req.body.content).notNull();
		v.check(req.body.twinSocketId).notNull();
		v.check(req.body.chatId).notNull().isHexadecimal().len(24);
	var userId  = req.session.userid,
		content = v.sanitize(req.body.content).xss(),
	twinSocketId= req.body.twinSocketId,
		chatId  = req.body.chatId;
	} catch(e){
		res.send(400);
		return;
	}
	//check if casting from string to object is needed 
	models.OnlineUsers.getUserIdBySocketId(twinSocketId,d.db.bind(function(err,twinId){
		if(err) {
			res.send(500);
			return;
		}
		models.Chats.findOneAndUpdate({_id : chatId}
			,{$push : {messages : {from:userId,to:twinId,content:content,timeSent : Date.now()}}}
			,d.db.bind(function(err,doc){
				if(err) console.log(err);
				else {
					var twinName = doc.userId.equals(userId) ? doc.names[1] : doc.names[0];
					//send response,socket emit to twinSocket
					res.send(200);
					if(doc.revealed) {
						sendToSocket.send(
							'emit',
							{
								socketId 	: twinSocketId,
								eventName 	: 'newChatMessage',
								emitData 	: {content : content,twinName : twinName,timeSent : Date.now()}
							},
							noop
						);
					}
					else {
						sendToSocket.send(
							'emit',
							{
								socketId 	: twinSocketId,
								eventName 	: 'newChatMessage',
								emitData 	: {content : content,timeSent : Date.now()}
 							},
 							noop
						);
					}
				}
		}));
	}));

}

/*------- Called In : twinProfileItemView.js, route : GET/sendContactRequest, status : 1---------*/
module.exports.sendContactRequest = function(req,res){
	try{
		v.check(req.body.chatId).isHexadecimal().notNull().len(24);
		v.check(req.body.twinSocketId).notNull();
		v.check(req.body.mySocketId).notNull();
	var userId   = req.session.userid,
		chatId   = req.body.chatId,
		twinSocketId = req.body.twinSocketId,
		mySocketId 	 = req.body.mySocketId;
	} catch(e){
		res.send(400);
		return;
	}
	//check if you are already friends , 
	models.Chats.findOne({_id : chatId},'userId twinId',d.db.bind(function(err,doc){
		if(err) {
			res.send(500);
			return;
		}
		var twinId = doc.twinId.equals(userId) ? doc.userId : doc.twinId ;

		models.UserDetail.findOne({_id : userId, 'contacts.user' : {$all : [twinId]}},d.db.bind(function(err,doc){
			if(err) {
				res.send(500);
				return;
			}
			if(!doc){
				models.Chats.findOne({_id : chatId},'contactRequests',d.db.bind(function(err,doc){
					if(doc.contactRequests.length === 1){
						res.send(400)
					}
					else if(doc.contactRequests.length === 0){
						doc.update({$push : {contactRequests : userId}},d.db.bind(function(err){
							if(err) {
								res.send(500);
								return;
							}
							else {
								res.json({
									error : 0,
									msg   : 'Request sent, action awaiting'
								})
							}
						}));
						sendToSocket.send(
							'emit',
							{
								socketId 	: twinSocketId,
								eventName 	: 'contactRequest',
								emitData 	: {}
							},
							noop
						);
					}
					else {
						res.send(500);
						return;
					}
				}));
			}
			else {
				res.json({
					error : 1,
					msg   : 'You are already in touch'
				})
			}
		}))
	}))

}

/*------- Called In : twinProfileItemView, route : POST/contactResponse, status : 1---------*/
module.exports.contactResponse = function(req,res){
	try{
		v.check(req.body.response).notNull().len(4);
		v.check(req.body.chatId).notNull().isHexadecimal().len(24);
		v.check(req.body.twinSocketId).notNull();
		v.check(req.body.mySocketId).notNull();
	var userId 	 = req.session.userid;
		response 	 = req.body.response === 'true' ? true : false,
		chatId   	 = req.body.chatId,
		twinSocketId = req.body.twinSocketId,
		mySocketId 	 = req.body.mySocketId;
	} catch(e){
		res.send(400);
		return;
	}

	models.Chats.findOne({_id : chatId},'userId twinId',d.db.bind(function(err,doc){
		var twinId = doc.userId.equals(userId) ? doc.twinId : doc.userId;
		if(response){
			models.Chats.findOneAndUpdate({_id : chatId, contactRequests : {$all : [twinId]}},
										  {$addToSet : {contactRequests : userId}},
										  d.db.bind(function(err,doc){
										  	if(err) res.send(500);
										  	else if(!doc) res.send(400);
										  	else res.send(200);
											sendToSocket.send(
												'emit',
												{
													socketId 	: twinSocketId,
													eventName	: 'contactResponse',
													emitData	: response.toString()
												},
												noop
											);
										  }));
			//create Thread
			models.Threads.create({
				isExpires    : false,
				messages     : [],
				participants : [userId,twinId],
				contactRequests : [] 
			},d.db.bind(function(err,doc){
				var threadId = doc._id;

				models.UserDetail.findOneAndUpdate(
					{_id : userId},
					{$push : {contacts : {user : twinId , started : Date.now()},allThreads : {threadId : threadId,twinId : twinId,lastMessage : '',lastUpdated : Date.now()}}},
					d.db.bind(function(err,doc){
						models.UserDetail.sendNotification(
							3,
							{userId : twinId,
							 name 	: doc.name
							},
							function(bool){
								if(!bool) throw new Error('Error sending Notification 3')
							}
						);
					})
				);

				models.UserDetail.findOneAndUpdate(
					{_id : twinId},
					{$push : {contacts : {user : userId, started : Date.now()},allThreads : {threadId : threadId,twinId : userId,lastMessage : '',lastUpdated : Date.now()}}},
					d.db.bind(function(err,doc){
						models.UserDetail.sendNotification(
							3,
							{userId : userId,
							 name 	: doc.name
							},
							function(bool){
								if(!bool) throw new Error('Error sending Notification 3')
							}
						)
					})
				);

				models.UserProfile.update({user_id : {$in : [userId,twinId]}},{$inc : {contacts_c : 1}},{multi : true},d.db.bind(function(err){
					;
				}));
			}));
			
		}
	}))
}

/*------- Called In : collections/shouts, route : GET/getShouts, status : 1---------*/
module.exports.GETshouts = function(req,res){
	var userId = req.session.userid;

	if(typeof req.query.pageNo === 'undefined' && typeof req.query.offset === 'undefined'){
		models.Shouts.find({shouter_id : {$ne : userId}})
		.sort({created_At : -1})
		.limit(10)
		.select('shouterStatus content created_At tags responders')
		.exec(d.db.bind(function(err,docs){
			if(err || !docs.length){
				res.json([{}]);
			}
			else {
				//needs to check
				async.mapSeries(
					docs,
					function(doc,cbt){
						doc.populate({
							path 	: 'shouterStatus',
							select  : 'status lastOnline' 
							},cbt);
					},
					d.db.bind(function(err,results){
						var shouts = [];
						_.each(results,function(result){
							var shout = {};
							_.each(result.responders,function(e){
								if(e.equals(userId)) shout.respondedByYou = true;
								else shout.respondedByYou = false;
							})
							shout.content 					= result.content;
							shout.shouterStatus = {};
							shout.shouterStatus.lastOnline 	= result.shouterStatus.lastOnline;
							shout.shouterStatus.status 		= result.shouterStatus.status;
							shout.tags 						= result.tags;
							shout.created_At    			= result.created_At;
							shout._id 						= result._id;
							shouts.push(shout);
						});
						res.json(_.sortBy(shouts,function(shout){
							return !shout.shouterStatus.status;
						}));
					})
				);
			}
		}));
	}
	else {
		var pageNo = req.query.pageNo,
			offset = req.query.offset,
			skipNo = parseInt(pageNo) * 10 + parseInt(offset);
		models.Shouts.find({shouter_id : {$ne : userId}})
		.sort({created_At : -1})
		.skip(skipNo)
		.limit(10)
		.select('shouterStatus content created_At tags responders')
		.exec(d.db.bind(function(err,docs){
			if(err || !docs.length){
				res.json([{}]);
			}
			else {
				//needs to check
				async.mapSeries(
					docs,
					function(doc,cbt){
						doc.populate({
							path 	: 'shouterStatus',
							select  : 'status lastOnline' 
							},cbt);
					},
					d.db.bind(function(err,results){
						var shouts = [];
						_.each(results,function(result){
							var shout = {};
							_.each(result.responders,function(e){
								if(e.equals(userId)) shout.respondedByYou = true;
								else shout.respondedByYou = false;
							})
							shout.content 		= result.content;
							shout.shouterStatus = {};
							shout.shouterStatus.lastOnline 	= result.shouterStatus.lastOnline;
							shout.shouterStatus.status 		= result.shouterStatus.status;
							shout.tags 			= result.tags;
							shout.created_At    = result.created_At;
							shout._id 			= result._id;
							shouts.push(shout);
						});
						res.json(_.sortBy(shouts,function(shout){
							return !shout.shouterStatus.status;
						}));
					})
				);
			}
		}));	
	}
	//Get the users last online and then get shouts from there
};

/*------- Called In : shoutsWrapperCompositeView.js, route : GET/getLatestShouts, status : 1---------*/
module.exports.getLatestShouts = function(req,res){
	try{
	v.check(req.query.lastTimeSent).notNull();
	var userId = req.session.userid;
	var lastTimeStamp = parseInt(req.query.lastTimeSent);
	} catch(err){
		res.send(400);
		return;
	}
	models.Shouts.find({shouter_id : {$ne : userId}, created_At : {$gt : lastTimeStamp}})
	.sort({created_At : -1})
	.limit(10)
	.select('shouterStatus content created_At tags')
	.exec(d.db.bind(function(err,docs){
		if(err || !docs.length){
			res.json([{}]);
		}
		else {
			//needs to check
			async.mapSeries(
				docs,
				function(doc,cbt){
					doc.populate({
						path 	: 'shouterStatus',
						select  : 'status lastOnline -user_id -_id' 
						},cbt);
				},
				d.db.bind(function(err,results){
					res.json(results)
				})
			);
		}
	}));
}

/*------- Called In : shoutSearchItemView, route : GET/search, status : 1---------*/
module.exports.shoutSearch = function(req,res){
	try{
		v.check(req.query.query).notNull().len(0,30);
	var userId = req.session.userid;
	var query = req.query.query.trim(); //validate query
	} catch(e){
		res.send(400);
		return;
	}
	//Store user queries

	models.Shouts.textSearch(query,d.db.bind(function(err,docs){
		if(err || !docs.results.length){
			res.json([{}]);
		}
		else {
			var data = _.pluck(docs.results,'obj');
				data = _.filter(data,function(item){
					return !item.shouter_id.equals(userId);
				})
			async.mapSeries(
				data,
				function(doc,cbt){
					doc.populate({
						path 	: 'shouterStatus',
						select  : 'status lastOnline -user_id -_id' 
						},cbt);
				},
				d.db.bind(function(err,results){
					var response = [];
					_.each(results,function(result){
						var obj = _.pick(result,'content','tags','created_At','shouterStatus','_id');
							obj.shouterStatus = _.pick(obj.shouterStatus,'lastOnline','status');
							response.push(obj);
					})
					res.json(response);
				})
			);
		}
	}));
}

/*------- Called In : QuestionsCompositeView.js, route : GET/getQuestion/:chatId, status : 1---------*/
module.exports.getQuestion = function(req,res){
	try{
		v.check(req.params.chatId).notNull().isHexadecimal().len(24);
	var userId = req.session.userid,
		chatId = req.params.chatId;
	} catch(e){
		res.send(400);
		return;
	}
	models.ObjectIdFromString(chatId,function(chatId){
		models.Chats.findOne({_id : chatId},d.db.bind(function(err,doc){
			if(err || !doc) {
				res.send(400);
				return;
			}
			var rand = doc.randomNo;
			if(doc.currentQuestion !== null) {
				models.Questions.findOne({_id : doc.currentQuestion},d.db.bind(function(err,doc){
					res.json({
						q   : doc.q,
						qid : doc._id 
					})
				}))
				
			}
			else {
			var askedQuestions = doc.askedQuestions.length > 0 ? doc.askedQuestions : [];
				if(doc.currentResponseCount > 0) res.send(400);
				else {
					models.Chats.aggregate([
						{$match    : {_id : chatId}},
						{$project  : {questionPool : 1,_id : 0}},
						{$unwind   : '$questionPool'},
						{$match    : {'questionPool._id' : {$nin : askedQuestions}}},
						{$match    : {'questionPool.randomNo' : {$gte : rand}}},
						{$limit    : 1}
						],d.db.bind(function(err,doc){
							if(err) {
								res.send(500);
							}
							else if(!doc || !doc.length){
								models.Chats.aggregate([
									{$match    : {_id : chatId}},
									{$project  : {questionPool : 1,_id : 0}},
									{$unwind   : '$questionPool'},
									{$match    : {'questionPool._id' : {$nin : askedQuestions}}},
									{$match    : {'questionPool.randomNo' : {$lt : rand}}},
									{$limit    : 1}
									],d.db.bind(function(err,doc){
										if(err) console.log(err);
										else if(!doc || !doc.length){
											res.send(200); //todo
										}
										else {
											var qid = doc[0].questionPool._id;
											res.json({
												q   : doc[0].questionPool.q,
												qid : qid
											});

											models.Chats.update({_id : chatId},{$push : {askedQuestions : qid},currentQuestion : qid,currentResponseCount : 0},d.db.bind(function(err){
												;
											}));

										}
									})
								)
							}
							else {
								var qid = doc[0].questionPool._id
								res.json({
									q   : doc[0].questionPool.q,
									qid : qid
								});

								models.Chats.update({_id : chatId},{$push : {askedQuestions : qid},currentQuestion : qid,currentResponseCount : 0},d.db.bind(function(err){
									;
								}))
							}
						}))
				}
			}

		}))
	})
}

/*------- Called In : questionItemView, route : POST/answer, status : 1---------*/
module.exports.answer = function(req,res){
	try{
		v.check(req.body.chatId).notNull().isHexadecimal().len(24);
		v.check(req.body.qid).notNull().isHexadecimal().len(24);
		v.check(req.body.answer).notNull().len(1,200);
	var userId  	 = req.session.userid,
		chatId 		 = req.body.chatId,
		qid    		 = req.body.qid,
		answer  	 = req.body.answer;
	} catch(e){
		res.send(400);
		return;
	}
	models.Chats.findOne({_id : chatId},d.db.bind(function(err,doc){
		if(err) res.send(400);
		else {
			models.ObjectIdFromString(qid,function(qid){
				if(!doc.currentQuestion.equals(qid) || doc.currentResponseCount === 2) res.send(400);
				else {
					res.send(200);
					if(doc.userId.equals(userId)){
						models.Chats.findOneAndUpdate({_id : chatId},{$push : {userAnswers : {qid : qid, answer : answer}},$inc : {currentResponseCount : 1}},d.db.bind(function(err,doc){
							sendToSocket.send(
								'emit',
								{
									socketId 	: doc.twinSocketId,
									eventName 	: 'newAnswer',
									emitData 	: {qid : qid.toHexString(), answer : answer}
								},
								noop
							);
							if(doc.currentResponseCount === 2) {
								models.Chats.update({_id : chatId},{currentQuestion : null,currentResponseCount : 0},d.db.bind(function(err){
									if(err) return;
									else {
										setTimeout(function(){
											sendToSocket.send(
												'emit',
												{
													socketId 	: doc.userSocketId,
													eventName 	: 'newQuestion',
													emitData 	: {}
												},
												noop
											)
											sendToSocket.send(
												'emit',
												{
													socketId 	: doc.twinSocketId,
													eventName 	: 'newQuestion',
													emitData 	: {}
												},
												noop
											);
										},1000);
									}
								}));
							}
						}));
					}
					else {
						models.Chats.findOneAndUpdate({_id : chatId},{$push : {twinAnswers : {qid : qid, answer : answer}},$inc : {currentResponseCount : 1}},function(err,doc){
							sendToSocket.send(
								'emit',
								{
									socketId 	: doc.userSocketId,
									eventName 	: 'newAnswer',
									emitData 	: {qid : qid.toHexString(), answer : answer}
								},
								noop
							);
							if(doc.currentResponseCount === 2) {
								models.Chats.update({_id : chatId},{currentQuestion : null,currentResponseCount : 0},d.db.bind(function(err){
									if(err) ;
									else {
										setTimeout(function(){
											sendToSocket.send(
												'emit',
												{
													socketId 	: doc.userSocketId,
													eventName 	: 'newQuestion',
													emitData 	: {} 
												},
												noop
											)
											sendToSocket.send(
												'emit',
												{
													socketId 	: doc.twinSocketId,
													eventName 	: 'newQuestion',
													emitData	: {}
												},
												noop
											);
										},1000);
									}
								}));
							}
						});
					}
				}
			})
		}
	}))
}

/*------- Called In : questionItemView, route : GET/skipQuestion, status : 1---------*/
module.exports.skipQuestion = function(req,res){
	try{
		v.check(req.params.chatId);
	var userId = req.session.userid,
		chatId = req.params.chatId;
	} catch(e){
		res.send(400);
		return;
	}
	models.Chats.findOneAndUpdate({_id : chatId},{currentResponseCount : 0,currentQuestion : null},d.db.bind(function(err,doc){
		if(err) ;
		else {
			res.send(200);
			setTimeout(function(){
					sendToSocket.send(
						'emit',
						{
							socketId 	: doc.userSocketId,
							eventName 	: 'newQuestion',
							emitData	: {}
						},
						noop
					);
					sendToSocket.send(
						'emit',
						{
							socketId 	: doc.twinSocketId,
							eventName 	: 'newQuestion',
							emitData 	: {}
						},
						noop
					);
			},1000)
		}
	}))
}


/*------- Called In : questionsCompositeView.js, route : GET/getTwinLikes/:chatId, status : 1---------*/
module.exports.getTwinLikes = function(req,res){
	try{
		v.check(req.params.chatId).notNull().isHexadecimal().len(24);
		v.check(req.query.type).notNull();
	var userId = req.session.userid,
		chatId = req.params.chatId,
		type   = req.query.type;
	} catch(e){
		res.send(400);
		return;
	}

	models.Chats.findOne({_id : chatId},'userId twinId',d.db.bind(function(err,doc){
		if(err) res.send(500);
		else{
			var twinId = doc.userId.equals(userId) ? doc.twinId : doc.userId;
			models.FBArchive.findOne({user_id : twinId},type,d.db.bind(function(err,doc){
				if(err) {
					console.log(err);
					res.send(400);
				}
				else res.json(doc[type]);
			}))
		}
	}))
}

/*------- Called In : homeCenterItemView, route : GET/getFaces, status : 1---------*/
module.exports.getFaces = function(req,res){
	//authenticate
	var userId = req.session.userid;
	models.OnlineUsers.find({user_id : {$ne : userId}}).select('userPic -_id').limit(25).exec(d.db.bind(function(err,doc){
		if(err) res.send(500);
		else res.json(doc);
	}));
}

/*------- Called In : homeCenterItemView, route : POST/changeStatus, status : 1---------*/
module.exports.changeStatus = function(req,res){
	//auth
	try{
		var userId 		= req.session.userid,
		socketId 		= req.body.mySocketId,
		status 			= !!parseInt(req.body.status);
	} catch(e){
		res.send(400);
		return;
	}

	models.OnlineUsers.findOneAndUpdate({user_id : userId,socketId : socketId},{avlblStatus : status},d.db.bind(function(err,doc){
		if(err) res.send(500);
		else res.send(200);
	}));
}

/*------- Called In : homeCenterItemView, route : POST/shoutSubmit, status : 1---------*/
module.exports.shoutSubmit = function(req,res){
	try{
		v.check(req.body.shout).notNull();
	var shoutContent = v.sanitize(v.sanitize(req.body.shout).xss()).escape();
	var tags         = req.body.tags ? req.body.tags.split(':') : [];
	} catch(e){
		res.send(400);
		return;
	}
	if(shoutContent.length > 200 || tags.length > 4) {
		res.send(400);
		return;
	}
	//More valiadation on content required
	models.UserStatus.findOne({user_id : req.session.userid},'_id',d.db.bind(function(err,doc){
		models.Shouts.create({
			shouter_id 		: req.session.userid,
			content    		: shoutContent,
			tags 	   		: tags,
			shouterStatus 	: doc._id,
			responders 		: []
		},function(err){
			if(err) res.json({ error : 1 , msg : 'Database error . mail us : ranadeep@simlr.me'})
			else {
				res.json({ error : 0 , msg : 'Success.Your shout is heard.(Note: Your shouts are not displayed in your shoutbox)'});

			}
		})
	}));
}

/*------- Called In : homeCenterItemView, route : POST/abracadabra, status : 1---------*/
module.exports.abracadabra = function(req,res){
	try{
		v.check(req.body.socketId);
	var userId 	 = req.session.userid;
	var mySocketId = req.body.socketId;
	var reqParams = req.body.params.split(':').map(function(i){return parseInt(i)}); 
	} catch(e){
		res.send(400);
		return;
	}
		if(reqParams.length > 27) {
			res.send(400);
			return;
		}
		//validate
		models.UserDetail.checkAndUpdateTop5(userId,reqParams,d.db.bind(function(err,bool){
			if(err) ;
			else if(!bool) res.send(400);
		}));	

		//Do magic , temporary
		models.OnlineUsers.abracadabra(userId,reqParams,d.db.bind(function(err,doc){
			if(err) res.json({error : 1,msg : 'Problem with server please try again later'});
			else if(!doc || !doc.length) res.json({error : 1,msg : 'No online users at the moment. Invite your friends'});
			else {
				models.OnlineUsers.getSocketAndUserIdByOnlineId(doc[0]._id,d.db.bind(function(err,socketId,twinId){
					if(err) {
						res.json({
							error : 1,
							msg   : 'Oops the other person just went offline , try again'
						});
					}
					else {
						var twinId = twinId.toHexString();
						 userId = userId.toHexString();
						sendToSocket.send(
							'emit',
							{
								socketId 	: socketId,
								eventName 	: 'AreYouReady',
								emitData 	: {initiatorSocketId : mySocketId ,type : 'discoverPeople'}
							},
							function(isOk){
								if(!isOk){
									res.json({
										error : 1,
										msg   : 'The other person just went offline, please try again'
									});
								}
								else {
									res.json({
										error : 0,
										twin  : socketId
									});

									sendToSocket.send(
										'on',
										{
											eventName 	 : 'twinAreYouReadyResponse',
											twinSocketId : socketId,
											socketId 	 : mySocketId
										},
										noop
									);
								}
							}
						)
					}
				}));
			}
		}));
}

/*------- Called In : someManyFrigginplaces, route : GET/report?type=, status : 1---------*/
module.exports.report = function(req,res){
	try{
		v.check(req.query.type).notNull();
	var userId = req.session.userid,
		type   = req.query.type;
	} catch(e){
		res.send(400);
		return;
	}
	//add authentication with session
	if(typeof userId === 'undefined' || typeof type === 'undefined') {
		res.send(400);
		return;
	}
	switch(type) {
		case 'chatCancel' : 
			models.UserDetail.updateScore(3,{userId : userId});
			res.send(200);
			break;
		case 'reportChat'  : 
			var chatId = req.query.chatId;
			models.Chats.findOne({_id : chatId},d.db.bind(function(err,doc){
				var twinId = null;
				if(doc.userId.equals(userId)){
					twinId = doc.twinId;
					if(doc.userReported) res.send(400);
					else {
						models.UserDetail.updateScore(5,{reportUser : twinId,userId : userId})
						res.send(200);
					}
				}
				else {
					twinId = doc.userId;
					if(doc.twinReported) res.send(400);
					else {
						models.UserDetail.updateScore(5,{reportUser : twinId,userId : userId});
						res.send(200);
					}
				}

			}))
			break;
		case 'reportShout'  :
			var shoutId = req.query.shoutId;
				models.Shouts.findOne({_id : shoutId,'reportLog.reporterId' : userId},d.db.bind(function(err,doc){
					if(err) res.send(400);
					else if(!doc) {
						models.UserDetail.getRanksByIds([userId],d.db.bind(function(err,rank){
							var userRank = rank[0];
							models.Shouts.findOneAndUpdate(
								{_id : shoutId},
								{$push :{reportLog : {reporterId : userId,reportedAt : Date.now()}},$inc : {reportImpact : userRank}},
								function(err,doc){
									if(err) res.send(400);
									else {
										if(doc.reportImpact > 6) {
											models.findOneAndRemove({_id : shoutId},d.db.bind(function(err){
												if(err) console.log(err);
											}));
											models.UserDetail.updateScore(6,{shouterId: doc.shouterId,reportImpact : reportImpact});
										}
										res.send(200);
									}
								}
							)
						}))

					}
					else res.send(200);
				}))
			break;
		case 'reportShoutResponse' : 
			var threadId = req.query.threadId;
				models.Threads.findOne({_id : threadId},'participants shouterId',d.db.bind(function(err,doc){
					if(err) res.send(400);
					else {
						if(!doc.shouterId) {
							res.send(400);
							return;
						}
						else {
							if(!doc.shouterId.equals(userId)) {
								res.send(400);
								return;
							}
							else {
								var twinId = doc.participants[0].equals(doc.shouterId) ? doc.participants[1] : doc.participants[0];
								models.UserDetail.updateScore(7,{reportUser : twinId,userId : doc.shouterId});
								//Remove the thread,and remve it from allThreads of both users.
								res.send(200);
							}
						}
					}
				}));
			break;
		default  :
			res.send(400); 
			break;
	}
}

module.exports.unfollow = function(req,res){
	var userId = req.session.userid;
	//remove in contacts , stringy thread , store in archive and remove it
	try{
		v.check(req.query.threadId).notNull().isHexadecimal().len(24);
	var threadId = req.query.threadId;
	} catch(e){
		res.send(400);
		return;
	}
	models.Threads.findOne({_id : threadId},d.db.bind(function(err,doc){
		var threadDoc = doc;
		if(err) {
			res.send(400);
			return;
		}
		var twinId = doc.participants[0].equals(userId) ? doc.participants[1] : doc.participants[0];
		models.UserDetail.update({_id : userId},{$pull : {contacts : {user : twinId},allThreads : {threadId : threadId}}},d.db.bind(function(err){
			if(err) ;
			else res.send(200);
		}))
		models.UserDetail.update({_id : twinId},{$pull : {contacts : {user : userId},allThreads : {threadId : threadId}}},d.db.bind(function(err){
			;
		}))
		models.UserProfile.update({user_id : userId},{$inc : {contacts_c : -1}},d.db.bind(function(err){
			;
		}))
		models.UserProfile.update({user_id : twinId},{$inc : {contacts_c : -1}},d.db.bind(function(err){
			;
		}))
		models.Archive.moveToArchive('thread',threadDoc,[userId,twinId],d.db.bind(function(err,doc){
			if(err) ;
			else models.Threads.remove({_id : threadId},d.db.bind(function(err){
				;
			}))
		}))
	}));
}

module.exports.getRecentNotifications = function(req,res){
	var userId = req.session.userid;
	models.UserDetail.aggregate(
		{$match 	: {_id : userId}},
		{$project 	: {allNotifications : 1,_id : 0}},
		{$unwind 	: '$allNotifications'},
		{$project   : {'allNotifications.content' : 1,'allNotifications.type' : 1,'allNotifications.createdAt' : 1,'allNotifications.actionUrl' : 1}},
		{$sort 		: {'allNotifications.createdAt' : -1}},
		{$limit 	: 5},
		d.db.bind(function(err,data){
			async.mapSeries(data,
				function(doc,cbt){
					cbt(null,{
						content 	: doc.allNotifications.content,
						type 		: doc.allNotifications.type,
						actionUrl	: doc.allNotifications.actionUrl
					})
				},
				d.db.bind(function(err,results){
					if(err) res.send(500);
					else res.json(results);
				})
			);
		})
	)
}

module.exports.logout = function(req,res){
	req.session.destroy(function(){
		;
	});
	res.redirect('/');
}

module.exports.getCompliments = function(req,res){
	var userId = req.session.userid;
	models.UserDetail.findOne({_id : userId},'top5Compliments top5Params params testimonies',d.db.bind(function(err,doc){
		var compliments 	= doc.top5Compliments.length > 2 ? [doc.top5Compliments[0],doc.top5Compliments[1]] : doc.top5Compliments;
			if(!compliments.length) compliments.push('0');
 		var pooledParams 	= _.intersection(doc.top5Params,doc.params);
		var params 			= pooledParams.length > 2 ? [pooledParams[0],pooledParams[1],pooledParams[2]] : [doc.params[0],doc.params[1],doc.params[2]]; 
		var testimonies 	= [];

		async.mapSeries(doc.testimonies,function(testimony,cbt){
			models.Chats.findOne({_id : testimony.chatId},'revealed userId names',function(err,doc){
				if(doc.revealed){
					cbt(null,{
						givenBy 	: testimony.givenBy.equals(doc.userId) ? '- '+doc.names[0] : '- '+doc.names[1],
						testimony 	: testimony.testimony
 					})
				}
				else {
					cbt(null,{
						givenBy : '- Stranger',
						testimony : testimony.testimony
					})
				}
			})
		},d.db.bind(function(err,results){
			if(err) {
				console.log(err);
				res.send(400);
				return;
			}
			var testimonies = results.reverse();
			res.json({
				compliments : compliments.join(';'),
				params 		: params.join(';'),
				testimonies : testimonies
			});
		}));
	}))
}

module.exports.getPSST = function(req,res){
	try{
		v.check(req.params.chatId).notNull().isHexadecimal().len(24);
	var chatId 		= req.params.chatId;
	} catch(e){
		console.log(err);
		res.send(400);
		return;
	}

	var userId 		= req.session.userid,
		randomNo 	= Math.random();

	models.Chats.findOne({_id : chatId},'twinSocketId userSocketId userId twinId askedPssts',d.db.bind(function(err,chatDoc){
		var socketId = chatDoc.userId.equals(userId) ? chatDoc.twinSocketId : chatDoc.userSocketId;
		var twinId 	 = chatDoc.userId.equals(userId) ? chatDoc.twinId : chatDoc.userId;
		models.UserDetail.getCommonInterests(userId,twinId,function(err,commonParams){
			commonParams.push(0); // Add the general section : )
			models.PSST.findOne({pid : {$in : commonParams},randomNo : {$gt : randomNo},_id : {$nin : chatDoc.askedPssts}},d.db.bind(function(err,doc){
				if(err) res.send(400);
				else {
					if(!doc){
						models.PSST.findOne({pid : {$in : commonParams},randomNo : {$lte : randomNo},_id : {$nin : chatDoc.askedPssts}},d.db.bind(function(err,doc){
							if(err || !doc) res.json({psst : 'Guess there have been too many pssst..'});
							else {
								res.json({psst : doc.psst});
								sendToSocket.send(
									'emit',
									{
										socketId 	: socketId,
										eventName 	: 'newPSST',
										emitData 	: {psst : doc.psst}
									},
									noop
								)

								models.Chats.update({_id : chatId},{$push : {askedPssts : doc._id}},d.db.bind(function(err){
									;
								}))
							}
						}))
					}
					else {
						res.json({psst : doc.psst});
						sendToSocket.send(
							'emit',
							{
								socketId 	: socketId,
								eventName 	: 'newPSST',
								emitData 	: {psst : doc.psst}
							},
							noop
						)

						models.Chats.update({_id : chatId},{$push : {askedPssts : doc._id}},d.db.bind(function(err){
							;
						}));
					}
				}
			}))
		})
	}));

}

module.exports.sendThreadContactRequest = function(req,res){
	try{
		v.check(req.params.threadId).notNull().isHexadecimal().len(24);
	var userId 		= req.session.userid,
		threadId 	= req.params.threadId;
	} catch(e){
		res.send(400);
		return;
	}
	models.Threads.findOne({_id : threadId},d.db.bind(function(err,doc){
		if(err || !doc ||doc.contactRequests.length > 1) res.send(400);
		else {
			if(doc.participants[0].equals(userId) || doc.participants[1].equals(userId)){
				if(doc.contactRequests.length === 0 ){
					doc.update({$push : {contactRequests : userId}},d.db.bind(function(err){
						if(err) ;
						res.send(200);
					}))
				}
				else {
					if(doc.contactRequests[0].equals(userId)) res.send(400);
					else{
						doc.update({$push : {contactRequests : userId},isExpires : false,expires : new Date('07/19/2020').valueOf()},d.db.bind(function(err){
							if(err) console.log(err);
							res.send(200);
						}));
						var twinId = doc.contactRequests[0];
						models.UserProfile.update({user_id : userId},{$inc : {contacts_c : 1}},d.db.bind(function(err){
							;
						}))
						models.UserProfile.update({user_id : twinId},{$inc : {contacts_c : 1}},d.db.bind(function(err){
							;
						}))
						models.UserDetail.update({_id : userId},{$push : {contacts : {user : twinId, started : new Date()}}},d.db.bind(function(err){
							;
						}))
						models.UserDetail.update({_id : twinId},{$push : {contacts : {user : userId, started : new Date()}}},d.db.bind(function(err){
							;
						}));
					}
				}
			}
			else res.send(400);
		}
	})); 
}

module.exports.getChatLog = function(req,res){
	try{
		v.check(req.params.chatId).notNull().isHexadecimal().len(24);
		var chatId = req.params.chatId;
		var userId = req.session.userid;
	} catch(e){
		res.send(400);
		return;
	}
	models.Chats.findOne({_id : chatId},d.db.bind(function(err,doc){
		if(doc.userId.equals(userId) || doc.twinId.equals(userId)){
			var jadeRenderObject = {
				twinName 	: '',
				userName  	: '',
				messages 	: [],
				questions 	: []
			};
			if(doc.revealed){
				if(doc.userId.equals(userId)){
					jadeRenderObject.userName = doc.names[0],
					jadeRenderObject.twinName = doc.names[1];
				}
				else {
					jadeRenderObject.userName = doc.names[1],
					jadeRenderObject.twinName = doc.names[0];
				}
			}
			else {
				jadeRenderObject.twinName = 'Stranger';
				jadeRenderObject.userName = 'You';
			}

			_.each(doc.messages,function(message){
				jadeRenderObject
				.messages
				.push({content : message.content,from : message.from.equals(userId) ? jadeRenderObject.userName : jadeRenderObject.twinName});
			})

			if(doc.userId.equals(userId)){
				_.each(doc.askedQuestions,function(qid){
					var q = _.find(doc.questionPool,function(question){
						return question._id.equals(qid)
					}).q,
					userAnswer = _.find(doc.userAnswers,function(question){return question.qid.equals(qid)}),
					twinAnswer = _.find(doc.twinAnswers,function(question){return question.qid.equals(qid)});

					userAnswer = userAnswer ? userAnswer.answer : '';
					twinAnswer = twinAnswer ? twinAnswer.answer : '';

					jadeRenderObject.questions.push({q : q,userAnswer : userAnswer,twinAnswer : twinAnswer});
				})
			}
			else{	
				_.each(doc.askedQuestions,function(qid){
					var q = _.find(doc.questionPool,function(question){
						return question._id.equals(qid)
					}).q,
					userAnswer = _.find(doc.twinAnswers,function(question){return question.qid.equals(qid)}),
					twinAnswer = _.find(doc.userAnswers,function(question){return question.qid.equals(qid)});

					userAnswer = userAnswer ? userAnswer.answer : '';
					twinAnswer = twinAnswer ? twinAnswer.answer : '';

					jadeRenderObject.questions.push({q : q,userAnswer : userAnswer,twinAnswer : twinAnswer});
				});
			}
			res.render('chatlog',jadeRenderObject);
		}
		else {
			res.send(400,'This is private content and you are not authorized to see this.oops.');
			return;
		}
	}));
}

//Put all routes below this line

//A function to remove expired threads , that runs every 2 hours
var deleteExipiredThreads = function(){
	models.Threads.find({isExpires : true,expires : {$lt : Date.now()}},d.db.bind(function(err,docs){
		_.each(docs,function(doc){

			models.UserDetail.update({_id : doc.participants[0],'allThreads.threadId' : doc._id},{$pull : {allThreads : 'allThreads.$'}},d.db.bind(function(err){
				;
			}))

			models.UserDetail.update({_id : doc.participants[1],'allThreads.threadId' : doc._id},{$pull : {allThreads : 'allThreads.$'}},d.db.bind(function(err){
				;
			}))

			models.Threads.remove({_id : doc._id},d.db.bind(function(err){
				;
			}));

			models.Shouts.update({_id : doc.shoutId,'threads.threadId' : doc._id},{$pull : {threads : 'threads.$'}},d.db.bind(function(err){
				;
			}));
		})
	}))
}

setInterval(deleteExipiredThreads,12*60*60*1000);

deleteExipiredThreads();

//closeing d.app.run , any code to be added should be add before this line
})