var mongoose   = require('mongoose');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var notificationSchema 	= new Schema({
	content 		: String,
	actionUrl		: String,
	createdAt		: Number,
	type 			: Number, 
	_id 			: false
})

module.exports = notificationSchema;