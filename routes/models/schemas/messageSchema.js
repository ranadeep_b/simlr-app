var mongoose   = require('mongoose');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var messageSchema  		= new Schema({
	content : String,
	from 	: {type : ObjectId},
	to 		: {type : ObjectId},
	timeSent: {type : Number, default : Date.now},
	_id 	: false
});

module.exports = messageSchema;