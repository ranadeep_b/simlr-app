var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var miscSchema	 	= new Schema({
	bannedUsers : [Number]
})

module.exports = Params = mongoose.model('misc',miscSchema);