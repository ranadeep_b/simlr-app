var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var messageSchema = require('./schemas/messageSchema');

var threadSchema 		= new Schema({
	isExpires 		: {type : Boolean , default : false},
	participants 	: [{type : ObjectId , index : true, ref : 'UserDetail'}], 
	messages  		: [messageSchema],
	started 		: {type : Number,default : Date.now},
	expires 		: {type : Number, default : new Date('07/19/2020').valueOf()},
	shouterId 		: {type :ObjectId,default : null},
	shoutId 		: {type :ObjectId,default : null},
	contactRequests : [{type : ObjectId}]
})

module.exports =  mongoose.model('Thread',threadSchema);