var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 
var Mixed 	 = Schema.Types.Mixed;

var fbArchive		= new Schema({
	user_id 	  : {type : ObjectId,index : true},//later on userDetail
	fbId 		  : {type : Number, index : true},
	createdAt 	  : Number,
	publicProfile : {type  : Mixed,_id : false},
	likes 		  : [{type : Mixed, _id : false}],
	likesIds 	  : [Number],
	musicLikes 	  : [{type : Mixed, _id : false}],
	moviesLikes   : [{type : Mixed, _id : false}],
	tvLikes 	  : [{type : Mixed, _id : false}],
	booksLikes 	  : [{type : Mixed, _id : false}],
	authorsLikes  : [{type : Mixed, _id : false}],
	foodsLikes 	  : [{type : Mixed, _id : false}],
	videoGamesLikes: [{type : Mixed, _id : false}],
	causesLikes   : [{type : Mixed, _id : false}],
	fosLikes 	  : [{type : Mixed, _id : false}],
	educationLikes: [{type : Mixed, _id : false}],
	sportsLikes   : [{type : Mixed, _id : false}],
	pstLikes 	  : [{type : Mixed, _id : false}],
	normalPic	  : String,
	thumb 		  : String,
	friends 	  : [{type : Mixed, _id : false}],
	friendsIds 	  : [Number]
})

module.exports = mongoose.model('FBArchive',fbArchive)