var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 
var Mixed 	 = Schema.Types.Mixed;

var archive		= new Schema({
	type 		: String, //model name in smallcase
	document 	: String,
	movedOn 	: {type : Number,default : Date.now()},
	relatedUsers: [{type : ObjectId,ref : 'UserDetail'}]
});

module.exports = mongoose.model('Archive',archive);