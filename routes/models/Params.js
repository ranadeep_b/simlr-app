var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var paramSchema 	= new Schema({
	title 			: String,
	type 			: Number, // 0 - trait , 1 - interest
	pid 			: Number,
})

module.exports = Params = mongoose.model('Params',paramSchema);

