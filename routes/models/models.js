var mongoose   	 = require('./dbConnection/dbConnection');
var _    	  	 = require('lodash');
var async      	 = require('async');
var sendToSocket = require('../../socket'); 
/*-------All Models----------------*/
var models = {
	PreRegister : require('./PreRegisters'),
	UserProfile : require('./UserProfiles'),
	UserDetail 	: require('./UserDetails'),
	UserStatus 	: require('./UserStatus'),
	OnlineUsers : require('./OnlineUsers'),
	Params 		: require('./Params'),
	Chats 		: require('./Chats'),
	Questions 	: require('./Questions'),
	PSST 		: require('./PSST'),
	Shouts 		: require('./Shouts'),
	Threads 	: require('./Threads'),
	FBArchive 	: require('./FBArchive'),
	Archive 	: require('./Archive'),
	Misc 		: require('./Misc')
}

var d = require('../domains');

module.exports = models;
/* -------------------- METHODS ------------------------*/
d.app.run(function(){
	//wrap all the code in this for error handling	


models.UserProfile.getLatestThreads = function(userId,cb){
	models.UserProfile.findOne({user_id : userId},'newThreads',d.db.bind(function(err,doc){
		if(err) return cb(err,null)
		else {
			cb(doc);
		};

	}))
}

models.UserProfile.updateShoutRespondSubmit = function(threadId,message,userId,shouterId,isNew){
	models.UserDetail.getNamesAndPicsByUserIds([userId],2,d.db.bind(function(err,result){
		var name  	  = result[0].name,
			senderPic = result[0].picUrl;
			models.UserProfile.update(
				{user_id : shouterId},
				{
					$push : {newThreads : {
						threadId : threadId,
						sender 	 : name,
						content  : message,
						timeSent : Date.now,
						senderPic: senderPic,	
						}
					}
				},
				d.db.bind(function(err){
					if(err) throw new Error( err + 'Error Updating UserProfile for shoutRespond');
				})
			) 
	}));
	models.UserDetail.sendNotification(1,{
			userId : shouterId,
			threadId : threadId
		},
		d.db.bind(function(bool){
			if(!bool) throw new Error('Error updating notification for shouterId shout Response')
		})
	);
	if(isNew === 'newThread'){
		models.UserDetail.update(
			{_id : userId},
			{$push : {allThreads : {
				threadId 	: threadId,
				twinId 	 	: shouterId,
				lastMessage : message,
				lastUpdated : Date.now() 
			}}},
			d.db.bind(function(err){
				if(err) throw new Error('Error adding thread to userId');
			})
		);
		models.UserDetail.update(
			{_id : shouterId},
			{$push : {allThreads : {
				threadId 	: threadId,
				twinId   	: userId,
				lastMessage : message,
				lastUpdated : Date.now()
			}}},
			d.db.bind(function(err){
				if(err) throw new Error(err + 'Error adding thread to userId');
			})	
		);
	}
	else {
		models.UserDetail.update({_id : userId,'allThreads.threadId' : threadId},{'allThreads.$.lastMessage' : message,'allThreads.$.lastUpdated' : Date.now()},d.db.bind(function(err){
			;
		}))
		models.UserDetail.update({_id : shouterId,'allThreads.threadId' : threadId},{'allThreads.$.lastMessage' : message,'allThreads.$.lastUpdated' : Date.now()},d.db.bind(function(err){
			;
		}))
	}
}

models.UserDetail.getNamesAndPicsByUserIds = function(UserIdArr,flag,cb){
	if(flag === 1){
		async.map(UserIdArr,
				function(userId,cbt){
					models.UserDetail.findOne({_id : userId},'name',d.db.bind(function(err,doc){
						if(err) cbt(err,null);
						else cbt(null,doc.name);
					}))
				},
				cb
		);
	}
	else {
		async.map(UserIdArr,
				function(userId,cbt){
					models.UserDetail.findOne({_id : userId},'name thumb',d.db.bind(function(err,doc){
						if(err) cbt(err,null);
						else cbt(null,{name : doc.name,picUrl : doc.thumb});
					}))
				},
				cb
		);
	}
}

models.UserDetail.getNamesAndPostersByUserIds = function(UserIdArr,flag,cb){
	if(flag === 1){
		async.map(UserIdArr,
				function(userId,cbt){
					models.UserDetail.findOne({_id : userId},'name',d.db.bind(function(err,doc){
						if(err) cbt(err,null);
						else cbt(null,doc.name);
					}))
				},
				cb
		);
	}
	else {
		async.map(UserIdArr,
				function(userId,cbt){
					models.UserDetail.findOne({_id : userId},'name normalPic',d.db.bind(function(err,doc){
						if(err) cbt(err,null);
						else cbt(null,{name : doc.name,picUrl : doc.normalPic});
					}))
				},
				cb
		);
	}
}

models.UserDetail.getRanksByIds = function(userIdArray,cb){
	async.map(
		userIdArray,
		function(user,cbt){
			models.UserDetail.findOne({_id : user},'rank',d.db.bind(function(err,doc){
				if(err) cbt(err,null);
				else cbt(null,doc.rank);
			}))
		},
		cb
	)
}

models.UserDetail.updateScore = function(typeCode,options){
	/*
		1.ChatResponse + Gave Compliments ?
		2.ShoutResponse
		3.Reply to chatRequests
		4.ChatLength
		5.Report on Chat
		6.Report Shout
		7.Report Shout Respond
	
	true - add and false - subsctract
	*/
	function getRankForScore(score){
		return score <= 20 && 1 || score <=40 && 2 || score<=100 && 3 || score <=140 && 4 || 5;
	}

	function update(userId,score){
			models.UserDetail.findOneAndUpdate({_id : userId},{$inc : {karma : score}},d.db.bind(function(err,doc){
				if(err) return;
				else {
					var rank = getRankForScore(doc.karma);
					if(rank !== doc.rank){
						models.UserDetail.update({_id : userId},{rank : rank},d.db.bind(function(err){
							if(err) console.log(err);
						}))

						models.UserProfile.update({user_id : userId},{rank : rank},d.db.bind(function(err){
							;
						}))
					}
				}
			}));
	}

	switch(typeCode) {
		case 1 : 
			var chatRating   = options.chatRating,
				traitsLength = options.traitsLength;
				models.UserDetail.getRanksByIds([options.userId,options.twinId],d.db.bind(function(err,results){
					var userRank = results[0],
						twinRank = results[1];
						chatRating > 0 ? update(options.twinId,(userRank * chatRating)) : update(options.twinId,-(userRank * chatRating));
						if(1 < traitsLength < 5) update(options.userId,(5/userRank));
				}))
			break;
		case 2 :
			models.UserDetail.getRanksByIds([options.userId],d.db.bind(function(err,results){
				var userRank = results[0];
				update(options.userId,(5/userRank));
			}))
			break;
		case 3 :
			update(options.userId,-2);
			break;
		case 4 : 
			if(options.chatSpan < 2) update(options.userId,-5);
			else {
				 var chatScore = options.chatSpan > 15 ? 5 : options.chatSpan/3;
				 update(options.userId,chatScore);
			}
			break;
		case 5 :
			models.UserDetail.getRanksByIds([options.userId,options.reportUser],d.db.bind(function(err,results){
				var userRank = results[0],
					twinRank = results[1];
				update(options.reportUser,-(userRank + (parseInt(userRank / twinRank))));
			}))
			break;
		case 6 :
			update(options.shouterId,-options.reportImpact);
			break;
		case 7 :
			models.UserDetail.getRanksByIds([options.userId,options.reportUser],d.db.bind(function(err,results){
				var userRank = results[0],
					twinRank = results[1];
					update(options.reportUser,-(userRank + 2 * (parseInt(userRank / twinRank))))
			}))
			break;
		default :
			break;

	}
}

models.UserDetail.sendNotification = function(type,options,cb){
	var userId = options.userId;

	var updateNotification = function(content,actionUrl){
		notification= {
					content   : content,
					actionUrl : actionUrl,
					createdAt : Date.now(),
					type 	  : type 
		};

		models.UserDetail.update({_id : userId},{$push : {allNotifications : notification}},d.db.bind(function(err,doc){
			if(err) ;
			else {
				cb(true);
			}
		}));

		models.UserProfile.update({user_id : userId},{$push : {newNotifications : notification}},d.db.bind(function(err){
			;
		}));
	}

	switch (type) {
		// shoutRespond message --> type 1
		case 1 : 
			var content 	= "Somebody responded to your shout . Check it out !",
				actionUrl  	= "#messages/"+options.threadId;
				updateNotification(content,actionUrl);
			break;
		case 2 : 
			//Compliments --->type 2
			var content 	= "The last person you talked to thinks you are "+ options.compliments,
				actionUrl  	= "#compliments";
				updateNotification(content,actionUrl);
			break;
		case 3 : 
			//stay in touch , --> type 3
			var content 	= "You and "+options.name+" are now in touch with each other. You can now contact each other",
				actionUrl  	= "#contacts";
				updateNotification(content,actionUrl);
			break;
		case 4 : 
			//Stauts change
			var content 	= "Based on your activity you are moved to "+ options.strata,
				actionUrl  	= "#home";
				updateNotification(content,actionUrl);
			break;
		default : 
			cb(false);
			break;
	}
}

models.UserDetail.getAllBuddies = function(userId,cb){
	models.UserDetail.findOne({_id : userId},'contacts',d.db.bind(function(err,doc){
		if(!doc.contacts.length){
			cb(null,[]);
			return;
		}
		else {
			doc.populate({
				path 	: 'contacts.user',
				select  : 'name thumb',
				options : {sort : {started : -1}} 
			},d.db.bind(function(err,doc){
				if(err) cb(err,null);
				else {
					var users = _.pluck(doc.contacts,'user');
					var contacts = [];
					async.each(users,function(user,ncb){	
						models.Threads.findOne({isExpires : false,participants : {$all : [userId,user._id]}},'_id',d.db.bind(function(err,doc){
							contacts.push({name : user.name,thumb : user.thumb,threadId : doc._id});
							ncb(null);
						}))
					},d.db.bind(function(err){
						if(err) ;
						cb(null,contacts);
					}));
				}
			}))
		}
		
	}))
}

models.UserDetail.checkAndUpdateTop5 = function(userId,params,cb){

	var updateTop5 = function(p,cbt){
		models.UserDetail.findOne({_id : userId,'reqParams.pid' : p},function(err,doc){
			if(doc === null){
				models.UserDetail.findOneAndUpdate({_id : userId},{$addToSet : {reqParams : {pid : p, checked : 1}}},d.db.bind(function(err,doc){
					cbt(err);
				}));
			}
			else {
				models.UserDetail.findOneAndUpdate({_id : userId,'reqParams.pid' : p},{$inc : {'reqParams.$.checked' : 1}},d.db.bind(function(err,doc){
					cbt(err) 
				}));
			}
		})
	}

	async.each(params,updateTop5,function(err){
		if(err) cb(err,null);
		else {
			models.UserDetail.aggregate(
				{$match 	: {_id : userId}},
				{$project 	: {reqParams : 1}},
				{$unwind 	: '$reqParams' },
				{$sort 		: {'reqParams.checked' : -1}},
				{$limit 	: 5},
				{$group 	: {_id : '$_id',top5 : {$addToSet : '$reqParams.pid'}}},
				d.db.bind(function(err,doc){
					if(err) cb(err,false);
					else{
						models.UserDetail.findOneAndUpdate({_id : userId},{top5Params : doc[0].top5},d.db.bind(function(err,doc){
							if(err) cb(err,false);
							else cb(err,true);
						}));
					}
			}))
		}
	});
}

models.OnlineUsers.abracadabra = function(userId,reqParams,cb){
	//get user params
	//TODO !!!!!!!! --> in the algorithm , sort by gender in the last 
	// and also crushMix -> select people u like , if they like you back if you will be connected
	models.UserDetail.findOne({_id : userId},'params top5Params',d.db.bind(function(err,doc){
		var userParams = doc.params;
		var userTop5   = doc.top5Params;
		models.OnlineUsers.aggregate(
				{$match	 	: {avlblStatus: true,user_id: {$ne : userId}} },
				{$project   : {dupParams : '$params',params : 1,top5Params : 1}},
            	{$unwind    : '$params'},
            	{$match     : {params : {$in : reqParams}}},
            	{$group     : {_id : '$_id',score1 : {$sum : 1},dupParams : {$first : '$dupParams'},top5Params : {$first : '$top5Params'}}},
            	{$project   : {s1 : {$divide : ['$score1',reqParams.length]},dupParams : 1,top5Params : 1}},
            	{$unwind    : '$top5Params'},
            	{$match     : {top5Params : {$in : userTop5}}},
            	{$group     : {_id : '$_id',score2 : {$sum : 1},dupParams : {$first : '$dupParams'},s1 : {$first : '$s1'}}},
            	{$project   : {s2 : {$divide : ['$score2',userTop5.length]},dupParams: 1,s1: 1}},
            	{$unwind    : '$dupParams'},
                {$match     : {dupParams: {$in : userParams}}},
                {$group     : {_id : '$_id',score3 : {$sum : 1},s1 : {$first : '$s1'},s2 : {$first : '$s2'}}},
                {$project   : {s3 : {$divide : ['$score3',userParams.length]},s1: 1, s2: 1}},
                {$project   : {s : {$add : ['$s1','$s2','$s3']}}},
                {$sort      : {s : -1}}, // later add gender
                {$limit     : 2},
			function(err,doc){
				if(!doc.length){
					models.OnlineUsers.findOne({avlblStatus: true,user_id: {$ne : userId}},function(err,doc){
						if(err || !doc) cb(err,null);
						else cb(null,[doc])
						return;
					});
				}
				 //Needs to check
				else {
				 	if(err) cb(err,null);
					else cb(null,doc)
				}
		});
	}));
}

models.OnlineUsers.getSocketAndUserIdByOnlineId = function(onlineId,cb){
	models.OnlineUsers.findOne({_id : onlineId},'socketId user_id',d.db.bind(function(err,doc){
		if(err) cb(err,null);
		else cb(null,doc.socketId,doc.user_id);
	}));
}

models.OnlineUsers.getUserIdBySocketId = function(socketId,cb){
	models.OnlineUsers.findOne({socketId : socketId},'user_id',d.db.bind(function(err,doc){
		console.log(doc);
		if(err || !doc) cb(true,null);
		else cb(null,doc.user_id)
	}));
}

//Helper method , used unnecessarly , but i dont have time

module.exports.ObjectIdFromString = function(string,cb){
	cb(mongoose.Types.ObjectId(string));
	return;
}

models.Archive.moveToArchive = function(type,doc,arrayOfRusers,cb){
	var stringifiedDoc = JSON.stringify(doc);
	models.Archive.create({type : type,document : stringifiedDoc,relatedUsers : arrayOfRusers},cb);
}

models.UserDetail.getCommonInterests = function(userId,twinId,cb){
	async.parallel([
		function(cbt){
			models.UserDetail.findOne({_id : userId},'params top5Params',d.db.bind(function(err,doc){
				if(err) cbt(err,null);
				else cbt(null,_.union(doc.params,doc.top5Params));
			}))
		},
		function(cbt){
			models.UserDetail.findOne({_id : twinId},'params top5Params',d.db.bind(function(err,doc){
				if(err) cbt(err,null);
				else cbt(null,_.union(doc.params,doc.top5Params));
			}))
		}
		],d.db.bind(function(err,results){
			if(err) cb(err,null);
			else cb(null,_.intersection(results[0],results[1]));
			
		}))
}

//Wrap any code after this to allow error handling
})