var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var textSearch = require('mongoose-text-search'); 

var shoutsSchema 		= new Schema({
	shouter_id 		: {type : ObjectId, index : true},
	content 	   	: String,
	created_At 		: {type : Number , default : Date.now, index : true},
	tags 	   		: [String],
	views 	   		: {type : Number , default : 0},
	shouterStatus 	: {type : ObjectId , ref : 'UserStatus'},
    responders 		: [{type : ObjectId,ref : 'UserDetail'}],
    threads 		: [{threadId : ObjectId, responderId : ObjectId,_id : false}], 
    reportLog	   	: [{reporterId : ObjectId,reportedAt : Number,_id : false}],
    reportImpact 	: {type : Number, default : 0}
})

shoutsSchema.plugin(textSearch);
shoutsSchema.index({content : 'text', tags : 'text'});

module.exports = mongoose.model('Shout',shoutsSchema);