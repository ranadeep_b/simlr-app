var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var preRegister 		= new Schema({
	name 		: String,
	gender  	: Boolean,
	normalPic 	: String,
	thumb 		: String,
	params		: [Number],
	created_At	: {type: Number, default : Date.now() },
	fbId 	    : {type : Number,index : true,unique : true},
	fbAccessToken : String,
	fbExpires   : Number
})


module.exports = mongoose.model('PreRegister',preRegister);