var domain = require('domain'),
	util   = require('util'),
	fs 	   = require('fs');

var fbErr = domain.create(),
	dbErr = domain.create(),
	appErr= domain.create();

appErr.run(function(){
	fbErr.on('error',function(err){
		util.log(err);
		fs.appendFile('fbErrors.txt',err.message+"\r"+err.stack,function(err){
			if(err) throw new Error('Error updating fbErrors.txt')
		})

	})
	dbErr.on('error',function(err){
		util.log(err);
		fs.appendFile('dbErrors.txt',err.message+"\r"+err.stack,function(err){
			if(err) throw new Error('Error updating dbErrors.txt');
		})

	})

})
//Fb domain, database domain ,

module.exports = {
	fb 	: fbErr,
	db 	: dbErr,
	app : appErr
}