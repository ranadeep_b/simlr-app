var Params 	  = require('./models/Params'),
	Questions = require('./models/Questions'),
	_ 		  = require('lodash'),
	PSST 	  = require('./models/PSST'),
	Misc 	  = require('./models/Misc');

Misc.create({
	bannedUsers : []
},function(err){
	if(err) console.log(err);
})

var paramsArray = [{title: 'Funny',		type : 0,pid : 1},
				  {title : 'Frank',		type : 0,pid : 2},
				  {title : 'Sweet',		type : 0,pid : 3},
				  {title : 'Crazy',		type : 0,pid : 4},
				  {title : 'Geeky',		type : 0,pid : 5},
				  {title : 'Badass',	type : 0,pid : 6},
				  {title : 'Sporty',	type : 0,pid : 7},
				  {title : 'Smart',	type : 0,pid : 8},
				  {title : 'Shy',		type : 0,pid : 9},

				  {title : 'Art & Design',		type : 1,pid : 21},
				  {title : 'Gaming',			type : 1,pid : 22},
				  {title : 'Startups',				type : 1,pid : 23},
				  {title : 'Fashion',				type : 1,pid : 24},
				  {title : 'Dating & Relationships',type : 1,pid : 25},
				  {title : 'Higher Education',		type : 1,pid : 26},
				  {title : 'Hobbies',				type : 1,pid : 27},
				  {title : 'TV Shows',				type : 1,pid : 28},
				  {title : 'Gadgets & Hi-Tech',		type : 1,pid : 29},
				  {title : 'Sharing Ideas',			type : 1,pid : 30},
				  {title : 'Finance & Money',		type : 1,pid : 31},
				  {title : 'CS & Programming',		type : 1,pid : 32},
				  {title : 'Mechanics & Robotics',	type : 1,pid : 33},
				  {title : 'Reading',				type : 1,pid : 34},
				  {title : 'Science & Technology',	type : 1,pid : 35},
				  {title : 'Sports & Recreation',	type : 1,pid : 36},
				  {title : 'Travelling',			type : 1,pid : 37},
				  {title : 'Writing',				type : 1,pid : 38}];
		//Add Music,Networking,Placements

Params.create(paramsArray,function(err){
	if(err) console.log(err);
});

//=================Questions===============

//create admin interface for adding questions .
var questions = [
	{
		q : 'What is your life changing decision ?',
		pid : 0
	},
	{
		q : 'How do you describe your current life ?',
		pid : 0
	},
	{
		q : 'Did you press reveal ! ?',
		pid : 0
	},
	{
		q : 'Where do you live ? Past, present or the future',
		pid : 0
	},
	{
		q : 'What do you think about the other person from what he/she likes?',
		pid : 0
	},
	{
		q : 'Are you a saver or a spender?',
		pid : 0
	},
	{
		q : 'If you are an artist, where do you get your inspiration from?',
		pid : 21
	},
	{
		q : 'What do you think about the new fad, the flat UI',
		pid : 21
	},
	{
		q : 'What are your tools of trade (Art & Design)',
		pid : 21
	},
	{
		q : 'If you are an artist, exchange your work!',
		pid : 21
	},
	{
		q : 'Share your deviantArt or dribble !',
		pid : 21
	},
	{
		q : 'Good artists copy ? ',
		pid : 21
	},
	{
		q : 'Are you having a career in design?',
		pid : 21
	},
	{
		q : 'Digital art taking over the print ?',
		pid : 21
	},
	{
		q : 'NFS is the best racing series ever?',
		pid : 22
	},
	{
		q : 'Whats important for a game?Graphics or gameplay?',
		pid : 22
	},
	{
		q : 'Whats the most challenging game you have played so far',
		pid : 22
	},
	{
		q : 'What games are you currently hooked onto?',
		pid : 22
	},
	{
		q : 'Are you waiting for half-life-3',
		pid : 22
	},
	{
		q : 'Most favorite GTA game title?',
		pid : 22
	},
	{
		q : 'Do you feel video games improve you in any way?',
		pid : 22
	},
	{
		q : 'Which game would you gift to your worst enemy?',
		pid : 22
	},
	{
		q : 'Most badass gaming character?',
		pid : 22
	},
	{
		q 	: 'Whats your current startup idea ?',
		pid : 23 
	},
	{
		q 	: 'Have you heard about lean startups ?',
		pid : 23 
	},
	{
		q 	: 'Which are top 3 news sources you follow?',
		pid : 23 
	},
	{
		q 	: 'What is the most innovative startup you have seen lately?',
		pid : 23 
	},
	{
		q 	: 'What do you think is the NEXT BIG THING ?',
		pid : 23 
	},
	{
		q 	: 'Angel investors, angels or demons ?',
		pid : 23 
	},
	{
		q 	: 'Best place for startups ?',
		pid : 23 
	},
	{
		q 	: 'Have you been to any hackathon ?',
		pid : 23 
	},
	{
		q 	: 'Whats your favorite nail paint color?',
		pid : 24 
	},
	{
		q 	: 'Did you know there are more than 10 kinds of heels? Which is your favorite',
		pid : 24 
	},
	{
		q 	: 'What goes on well with a white shirt ?',
		pid : 24 
	},
	{
		q 	: 'Light colors or dark colors?',
		pid : 24 
	},
	{
		q 	: 'Favorite fashion brand?',
		pid : 24 
	},
	{
		q 	: 'Do you guys have a wishlist ? Exchange !',
		pid : 24 
	},
	{
		q 	: 'Define fashion',
		pid : 24 
	},
	{
		q : 'Your view on Dating in India?',
		pid : 25
	},
	{
		q : 'Whats the most romaintic thing youve ever heard/done',
		pid : 25
	},
	{
		q : 'Did you ever fall in love?',
		pid : 25
	},
	{
		q : 'Are you single ?',
		pid : 25
	},
	{
		q : 'What turns you on',
		pid : 25
	},
	{
		q : 'Whats a complete No No on a first date ?',
		pid : 25
	},
	{
		q : 'Can you love someone without ever seeing them ?',
		pid : 25
	},
	{
		q : 'Most romantic movie/TV show ?',
		pid : 25
	},
	{
		q : 'Describe how your ideal partner should be like',
		pid : 25
	},
	{
		q : 'What do you think about chivalry',
		pid : 25
	},
	{
		q : 'What is the current framework/library you are working on ?',
		pid : 32
	},
	{
		q : 'What is your most favorite IDE ?',
		pid : 32
	},
	{
		q : 'Is there a best programming language ?',
		pid : 32
	},
	{
		q : 'What is the languge you code the most in?',
		pid : 32
	},
	{
		q : 'Exchange your github profiles !',
		pid : 32
	},
	{
		q : 'Participated in any hackathon? How was the experience ?',
		pid : 32
	},
	{
		q : 'Readability or perforamce ? Which is more important for a programming language',
		pid : 32
	},
	{
		q : 'When did you start coding ?',
		pid : 32
	},
	{
		q : 'Share your code that you are most of proud of',
		pid : 32
	},
	{
		q : 'Say one cool thing about your favorite programming language ?',
		pid : 32
	},
	{
		q 	: 'Favorite song in the past few days ?',
		pid : 27
	},
	{
		q 	: 'Best movie that you think the other person didnt watch and should watch it now !',
		pid : 27
	},
	{
		q 	: 'Which fictional character you think you resemble the most',
		pid : 27
	},
	{
		q 	: 'What is that hobby of yours that your proud about?',
		pid : 27
	},
	{
		q 	: 'Are you a photographer? If yes share your work! No? then post a link to a random image.',
		pid : 27
	},
	{
		q : 'Best TV character ever created?',
		pid : 28
	},
	{
		q : 'Favorite chanacter in Friends ?',
		pid : 28
	},
	{
		q : 'Legen...Wait for it ...dary moment of yours ?',
		pid : 28
	},
	{
		q : 'Who is the second most awesome TV character? Barney stinson Hi5',
		pid : 28
	},
	{
		q : 'That TV show that you think the other person would not have seen ?',
		pid : 28
	},
	{
		q : 'The TV show that you regretted watching ?',
		pid : 28
	},
	{
		q : 'Cartoons are not the same anymore. What do you think?',
		pid : 28
	},
	{
		q : 'Favorite 90s show?',
		pid : 28
	},
	{
		q : 'Favorite 90s cartoon character?',
		pid : 28
	},
	{
		q : 'Calculate and share the no.of hours you spent on TV shows this year and wonder if its worth it ?',
		pid : 28
	},
	{
		q : 'Which TV shows made you cry ?',
		pid : 28
	},
	{
		q : 'The funniest TV character ?',
		pid : 28
	},
	{
		q : 'Android/iOS/WindowsPhone/MozillaOS pick one?',
		pid : 29
	},
	{
		q : 'Which mobile platform would you develop an app for ?',
		pid : 29
	},
	{
		q : 'Do you guys know about phoneblock ?',
		pid : 29
	},
	{
		q : 'Mozilla OS, What do you think about it ?',
		pid : 29
	},
	{
		q : 'What are gadgets that you are proud of owing ?',
		pid : 29
	},
	{
		q : 'Best gadget you bhought in your life ?',
		pid : 29
	},
	{
		q : 'What do you think about Micromax,karbonn and other indian mobile brands ?',
		pid : 29
	},
	{
		q : 'Which is the most fascinating mobile app youve used lately ?',
		pid : 29
	},
	{
		q : 'Do you think all humans must be treated equal ?',
		pid : 30
	},
	{
		q : 'What are you Religous,atheist or agonistic ?',
		pid : 30
	},
	{
		q : 'What are your political views ?',
		pid : 30
	},
	{
		q : 'Do you believe in any kind of philosophy ?',
		pid : 30
	},
	{
		q : 'Are you changing the world in any way ?',
		pid : 30
	},
	{
		q : 'Whats is the purpose of life according to you ?',
		pid : 30
	},
	{
		q : 'Can true democracy ever be acheived ?',
		pid : 30
	},
	{
		q : 'Exchange a mindblowing fact !',
		pid : 30
	},
	{
		q : 'Do you invest in stocks ?',
		pid : 31
	},
	{
		q : 'Share your favorite warren buffet quote',
		pid : 31
	},
	{
		q : 'When do you plan to retire financially',
		pid : 31
	},
	{
		q : 'Whats your investment plan ?',
		pid : 31
	},
	{
		q : 'Share your financial wisdom',
		pid : 31
	},
	{
		q : 'What are some unconventional ways to get rich quick ?',
		pid : 31
	},
	{
		q : 'What are some good money saving techniques you use ?',
		pid : 31
	},
	{
		q : 'What is your first bot about ? (Robotics)',
		pid : 33
	},
	{
		q : 'Are you working on a prototype (Robotics)',
		pid : 33
	},
	{
		q : 'What is your idea of a futuristic robot ',
		pid : 33
	},
	{
		q : 'What projects are you currently working on (Robotics)',
		pid : 33
	},
	{
		q 	: 'Who is your most favorite fictious character & why ?',
		pid : 34
	},
	{
		q 	: 'Do you think twilight is over-rated ?',
		pid : 34
	},
	{
		q 	: 'Do you think harry potter is over-rated ?',
		pid : 34
	},
	{
		q 	: 'Which was your first novel ?',
		pid : 34
	},
	{
		q 	: 'Which novel got you excited the most ?',
		pid : 34
	},
	{
		q 	: 'Which is the novel you read way too quickly ?',
		pid : 34
	},
	{
		q 	: 'Which is that movie where movie is better than the novel ?',
		pid : 34
	},
	{
		q 	: 'When did you read your first novel ?',
		pid : 34
	},
	{
		q 	: 'What is that area of science that interests you most ?',
		pid : 35
	},
	{
		q 	: 'When did you find science so fascinating ?',
		pid : 35
	},
	{
		q 	: 'Explain the other person what you think about black-holes and dark matter ! ?',
		pid : 35
	},
	{
		q 	: ' ?',
		pid : 35
	},
	{
		q 	: 'Do you think cricket is over-rated in India ?',
		pid : 36
	},
	{
		q : 'Where do you get your inspiration for writing ?',
		pid : 38
	},
	{
		q : 'Exchange your blogs ?',
		pid : 38
	},

]

_.each(questions,function(qn){
	Questions.create({
		q  	: qn.q,
		pid : qn.pid,
		randomNo : Math.random() 
	},function(err){
		if(err) console.log(err);
	});
})


//====================PSST===================
var pssts = [
	{
		psst : 'did you know that the original name for butterfly was flutterby',
		pid  : 0
	},
	{
		psst : 'Do you know that ants never sleep?',
		pid  : 0
	},
	{
		psst : 'Did you know C language is actually written in C ?',
		pid  : 32
	},
	{
		psst : 'Do you know that google is rolling out a new service called helpouts ?',
		pid  : 0
	},
	{
		pstt : 'Do you know that female version of dude is dudine ?',
		pid  : 0
	},
	{
		psst : 'Do you know that average human dream lasts only about 2-3 seconds ?',
		pid  : 0
	},
	{
		psst : 'Do you know that the brain is more active during the night than the day !',
		pid  : 0
	}
]

_.each(pssts,function(p){
	PSST.create({
		psst 	 : p.psst,
		pid  	 : p.pid,
		randomNo : Math.random() 
	},function(err){
		if(err) console.log(err);
	})
})