var request   = require('request'),
	crypto 	  = require('crypto'),
	fs 		  = require('fs'),
	_ 		  = require('lodash'),
	d 		  = require('./domains'),
	async 	  = require('async');


var GRAPH_URL  = 'https://graph.facebook.com',
	APP_ID 	   = '543381882362634',
	APP_SECRET = '9ae7a261325ea79062cb947ca30eb7a4';
/*

	PPIC_PATH  = path.resolve('public/img/ppics'),
	THUMB_PATH = path.resolve('public/img/thumbs');

var makeRequest = function(urlPart,formBody,type,cb){
	var method = '';
	if(typeof type === 'function') method = 'GET';
	else method = type;

	if(typeof formBody === 'function') {
		cb       = formBody;
		formBody = {};
	}

	switch(method) {
		case 'GET' : 
			request(GRAPH_URL+urlPart).query()
			break;
		case 'POST': 
			break;
	}
}
*/

d.app.run(function(){

	//Wrap alll the code inside this , for error handling

module.exports.hash = function(bake){
	return crypto.createHash('md5').update(bake).digest('hex');
}

module.exports.extendAccessToken = function(accessToken,cb){
	var url = GRAPH_URL+'/oauth/access_token?grant_type=fb_exchange_token&client_id='+APP_ID+'&client_secret='+APP_SECRET+'&fb_exchange_token='+accessToken;
	request(url,d.fb.bind(function(err,response,body){
		if(err || response.statusCode !== 200){		
			//Logout !important
			cb(err,null)
		}
		else {
			var array = body.trim().split('&'),
				obj   = 	{
								accessToken : array[0].split('=')[1],
								expires     : array[1].split('=')[1]
							}
			cb(false,obj);
		}
	}));
} 

module.exports.getBasicInformation = function(accessToken,id,cb){
	var now = Date.now();
	async.parallel([
		function(cbt){
			request({url : GRAPH_URL+'/me?access_token='+accessToken,json : true},d.fb.bind(function(err,response,body){
				if(err || response.statusCode !== 200) cbt(err,null);
				else cbt(null,{name : body.name, gender : body.gender === 'male' ? 1 : 0});
			}))
		},
		function(cbt){
			request({url : GRAPH_URL+'/me/picture?width=150&height=150&access_token='+accessToken, encoding : null},d.fb.bind(function(err,response,body){
				if(err || response.statusCode !== 200) cbt(err,null);
				else {
					fs.writeFile('./public/img/ppics/'+id+';'+now+'.jpg',body,d.app.bind(function(err){
						if(err) cbt(err,null);
						else cbt(null,'/img/ppics/'+id+';'+now+'.jpg');
					}))
				}
			}))

		},
		function(cbt){
			request({url : GRAPH_URL+'/me/picture?type=square&access_token='+accessToken, encoding : null},d.fb.bind(function(err,response,body){
				if(err || response.statusCode !== 200) cbt(err,null);
				else {
					fs.writeFile('./public/img/thumbs/'+id+';'+now+'.jpg',body,d.app.bind(function(err){
						if(err) cbt(err,null);
						else cbt(null,'/img/thumbs/'+id+';'+now+'.jpg');
					}))
				}
			}));
		}
		],d.fb.bind(function(err,results){
			if(err) {
				console.log(err);
				cb(err,null);
			}
			else cb(null,{
				name      : results[0].name,
				gender    : results[0].gender,
				normalPic : results[1],
				thumb 	  : results[2]
			})
		}))
}

module.exports.setupArchive = function(accessToken,cb){
	async.parallel([
		function(cbt){
			request({url : GRAPH_URL+'/me?access_token='+accessToken, json : true},d.fb.bind(function(err,res,body){
				if(err || res.statusCode !== 200) {
					cbt(err,null);
				}
				cbt(null,body ? body : []);
			}));
		},
		function(cbt){
			request({url : GRAPH_URL+'/me?fields=likes.limit(1000)&access_token='+accessToken, json : true},d.fb.bind(function(err,res,body){
				if(err || res.statusCode !== 200) {
					console.log(err);
					cbt(err,null);
				}
				cbt(null,body ? body.likes.data : [] ); //Array of objects with category,name,created_time,id
			}));
		},
		function(cbt){
			var obj = {
				music 		: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='MUSICIAN/BAND' ORDER BY fan_count ASC"),
				movies  	: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='Movie' ORDER BY fan_count DESC"),
				tv 			: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='TV SHOW' ORDER BY fan_count DESC"),
				books 		: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='BOOK' ORDER BY fan_count DESC"),
				foods   	: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='FOOD/BEVERAGES' ORDER BY fan_count DESC"),
				videoGames  : encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='VIDEO GAME' ORDER BY fan_count ASC"),
				causes 		: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='Cause' ORDER BY fan_count ASC"),
				fos 		: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='FIELD OF STUDY' ORDER BY fan_count ASC"),
				education 	: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='EDUCATION' ORDER BY fan_count DESC"),
				sports 		: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='SPORT' ORDER BY fan_count DESC"),
				authors 	: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='Author' ORDER BY fan_count DESC"),
				pst 		: encodeURIComponent("SELECT page_id,name FROM page WHERE page_id IN (SELECT uid, page_id, type,created_time FROM page_fan WHERE uid=me() ORDER BY created_time DESC) AND type='Professional Sports Team' ORDER BY fan_count DESC")
			};

			var query = JSON.stringify(obj);
			request({url : GRAPH_URL+'/fql?q='+query+'&access_token='+accessToken, json : true},d.fb.bind(function(err,res,body){
				if(err || res.statusCode !== 200) cbt(err,null);
				cbt(null,body ? body.data : []); //Array of Objects with name(string),fql_result_set(Array of objects) sorted by name ASC alpahabetical
			}));

		},
		function(cbt){
			request({url : GRAPH_URL+'/me/friends?&access_token='+accessToken, json : true},d.fb.bind(function(err,res,body){
				if(err || res.statusCode !== 200) cbt(err,null);
				cbt(null,body ? body.data : []) //Array of objects with name,id
			}));	
		}
		],d.fb.bind(function(err,results){
			if(err) cb(err,null);
			else {
				cb(null,{
					publicProfile : results[0] ? results[0] : [],
					likes 	  	  : results[1] ? results[1] : [],
					likesIds 	  : results[1] ? _.pluck(results[1],'id') : [],
					musicLikes 	  : results[2][7].fql_result_set ? results[2][7].fql_result_set : [],
					moviesLikes	  : results[2][6].fql_result_set ? results[2][6].fql_result_set : [],
					tvLikes 	  : results[2][10].fql_result_set ? results[2][10].fql_result_set : [],
					booksLikes 	  : results[2][1].fql_result_set ? results[2][1].fql_result_set : [],
					foodsLikes 	  : results[2][4].fql_result_set ? results[2][4].fql_result_set : [],
					videoGamesLikes : results[2][11].fql_result_set ? results[2][11].fql_result_set : [],
					causesLikes   : results[2][2].fql_result_set ? results[2][2].fql_result_set : [],
					fosLikes 	  : results[2][5].fql_result_set ? results[2][5].fql_result_set : [],
					educationLikes: results[2][3].fql_result_set ? results[2][3].fql_result_set : [],
					sportsLikes   : results[2][9].fql_result_set ? results[2][9].fql_result_set : [],
					authorsLikes  : results[2][0].fql_result_set ? results[2][0].fql_result_set : [],
					pstLikes 	  : results[2][8].fql_result_set ? results[2][8].fql_result_set : [],
					friends 	  : results[3] ? results[3] : [],
					friendsIds    :  results[3] ? _.pluck(results[3],'id') : []
				});
			}

		}));
}

module.exports.verifyAccessToken = function(fbId,accessToken,cb){
	request({url : 'https://graph.facebook.com/'+fbId+'/permissions?access_token='+accessToken,json : true},d.fb.bind(function(err,res,body){
		if(err || res.statusCode !== 200) cb(err,null);
		else {
			var permissions = body.data[0];
			if(permissions.installed === 1 && permissions.basic_info === 1 && permissions.user_likes === 1 && permissions.user_friends === 1) cb(false,accessToken);
			else cb(true,null);
		}
	}))
}	

module.exports.getMutualFriends = function(userId,twinId,accessToken,cb){
	//Get names,ids,thumbs
}


//Get pages liked by both users and sort them by page_fan(create_time DESC) and page(fan_count ASC) all kinds of categories

//closing the d.app.run , error handling
})
