<h3 id='shout-intro' class='hidden-sm'><span>Is anybody there who </span><span id='shuffle' class='hidden-sm'>is preparing for GRE ?</span></h3>
<div id='shout-create'>
	<div id='shout-textbox'>
		<textarea type='text' id='the-great-shoutbox' placeholder='Is anybody there who ___'></textarea>
		<button class='btn btn-default' id='shout-submit-button'>Shout</button>
		<p class='small-notifications'>* All your shouts are anonymous</p>
		<div id='tags-input-box'></div>
	</div>
</div>
<hr>
<div id='centerBottomContainer'>
	<div id='magicBox'>
		<h3>Select the kind of people you like to talk to </h3>
		<a href="#" id='clearChecked'>×</a>
		<div id='preferences'>
			<div class='row'>
				<div class='col col-lg-6' id='characterTraits'>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='a1' class='box-container'><a class='box'></a><span class='hoverText'>Funny</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='a2' class='box-container'><a class='box'></a><span class='hoverText'>Frank</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='a3' class='box-container'><a class='box'></a><span class='hoverText'>Sweet</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='a4' class='box-container'><a class='box'></a><span class='hoverText'>Crazy</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='a5' class='box-container'><a class='box'></a><span class='hoverText'>Geeky</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='a6' class='box-container'><a class='box'></a><span class='hoverText'>Badass</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='a7' class='box-container'><a class='box'></a><span class='hoverText'>Sporty</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='a8' class='box-container'><a class='box'></a><span class='hoverText'>Smart</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='a9' class='box-container'><a class='box'></a><span class='hoverText'>Shy</span></div>
						</div>
					</div>
				</div>
				<div class='col col-lg-6' id='interestParams'>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p21' class='box-container'><a class='box'></a><span class='hoverText'>Design &amp; Art</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p22' class='box-container'><a class='box'></a><span class='hoverText'>Gaming</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p23' class='box-container'><a class='box'></a><span class='hoverText'>Startups</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p24' class='box-container'><a class='box'></a><span class='hoverText'>Fashion</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p25' class='box-container'><a class='box'></a><span class='hoverText'>Dating &amp; Relationships</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p26' class='box-container'><a class='box'></a><span class='hoverText'>Higher Education</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p27' class='box-container'><a class='box'></a><span class='hoverText'>Hobbies</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p28' class='box-container'><a class='box'></a><span class='hoverText'>TV Shows</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p29' class='box-container'><a class='box'></a><span class='hoverText'>Gadgets &amp; Internet</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p30' class='box-container'><a class='box'></a><span class='hoverText'>Sharing Ideas</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p31' class='box-container'><a class='box'></a><span class='hoverText'>Finance &amp; Business</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p32' class='box-container'><a class='box'></a><span class='hoverText'>Coding</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p33' class='box-container'><a class='box'></a><span class='hoverText'>Mechanics &amp; Robotics</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p34' class='box-container'><a class='box'></a><span class='hoverText'>Reading</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p35' class='box-container'><a class='box'></a><span class='hoverText'>Science &amp; Technology</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p36' class='box-container'><a class='box'></a><span class='hoverText'>Sports</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p37' class='box-container'><a class='box'></a><span class='hoverText'>Travelling</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p38' class='box-container'><a class='box'></a><span class='hoverText'>Writing</span></div>
						</div>
					</div>
				</div>
			</div>
			<button class='btn btn-default btn-block'>DISCOVER PEOPLE</button>
		</div>
	</div>
</div>