<div class='settingsContainer'>
<h2>Settings</h2>
<div id="nameChange">
	<div class='row'>
		<div class='col col-lg-4'>
			Name : 
		</div>
		<div class='col col-lg-8'>
			<%= name%>
		</div>
	</div>
</div>
<div id="interestsChange">
	<div class='row'>
		<div class='col col-lg-4'>
			Interests : 
		</div>
		<div class='col col-lg-8'>
			<% _.each(interests,function(i){ %>
				<span class='settingsInterest' id='interest<%=i%>'></span>
			<% }) %>
		</div>
	</div>
	<div style='display : none;' id='interestParams'>
	<h3>Select exactly 5 interests :</h3>
	<div class='row'>
				<div class='col col-lg-12'>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p21' class='box-container'><a class='box'></a><span class='hoverText'>Design &amp; Art</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p22' class='box-container'><a class='box'></a><span class='hoverText'>Gaming</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p23' class='box-container'><a class='box'></a><span class='hoverText'>Startups</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p24' class='box-container'><a class='box'></a><span class='hoverText'>Fashion</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p25' class='box-container'><a class='box'></a><span class='hoverText'>Dating &amp; Relationships</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p26' class='box-container'><a class='box'></a><span class='hoverText'>Higher Education</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p27' class='box-container'><a class='box'></a><span class='hoverText'>Hobbies</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p28' class='box-container'><a class='box'></a><span class='hoverText'>TV Shows</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p29' class='box-container'><a class='box'></a><span class='hoverText'>Gadgets &amp; Internet</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p30' class='box-container'><a class='box'></a><span class='hoverText'>Sharing Ideas</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p31' class='box-container'><a class='box'></a><span class='hoverText'>Finance &amp; Business</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p32' class='box-container'><a class='box'></a><span class='hoverText'>Coding</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p33' class='box-container'><a class='box'></a><span class='hoverText'>Engineering</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p34' class='box-container'><a class='box'></a><span class='hoverText'>Reading</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p35' class='box-container'><a class='box'></a><span class='hoverText'>Science &amp; Technology</span></div>
						</div>
					</div>
					<div class='row'>
						<div class='col col-lg-4'>
							<div id='p36' class='box-container'><a class='box'></a><span class='hoverText'>Sports</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p37' class='box-container'><a class='box'></a><span class='hoverText'>Travelling</span></div>
						</div>
						<div class='col col-lg-4'>
							<div id='p38' class='box-container'><a class='box'></a><span class='hoverText'>Writing</span></div>
						</div>
					</div>
				</div>
	</div>
	<div class='row'>
		<div class='col col-lg-4 col-offset-4'>
			<button class='btn btn-small btn-default disabled' id='interestsUpdateButton'>Update</button>
			<button class='btn btn-small btn-default' id='interestsUpdateCancel'>Cancel</button>
		</div>
	</div>
	</div>
</div>
<div id="fblikesChange">
	<div class='row'>
		<div class='col col-lg-4'>
		Update facebook likes : 
		</div>
		<div class='col col-lg-8'>
			Music Movies TV Books Sports Foods etc..
		</div>
	</div>
</div>
<% if(clearSlate === false){%>
<div class='row' id='clearSlate'>
<div class="col col-lg-6">
    <label style='display : inline-block;'>
      <input type="checkbox" value='1' id='clearSlateCheckbox'> Clear slate (can be used only once)
    </label>
</div>
</div>
<% }%>
</div>