<div id='twinProfileContainer'>
	<div id='twinIdentity'>
		<%if(revealed){%>
		<div id='bio'></div>
		<%}else{%>
		<div id='bio'>
			<img src="/img/ppics/default.png" id='twinImage'><h3 id='twinName' style='text-align : center;'> ? </h3>
		</div>
		<%}%>
	</div>
	<button id='reveal' class='btn btn-primary'>Reveal</button>
	<div id='contactBox' style='display : none;'>
		<button class='btn btn-primary' id='stayInTouch'>Stay in touch</button>
	</div>
	<hr>
	<div>
		<h3>Interests</h3>
		<div id='twinInterests'>
			<% _.each(interests,function(i){%>
			<% var idName = 'interest'+i  %>
			<div class='twinInterest'>
				<span class='interestImage' id='<%= idName%>'></span> 
			</div>
			<%});%>
		</div>
	</div>
</div>