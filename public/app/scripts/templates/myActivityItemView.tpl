<h2>Recent Activity</h2>
<h3>Shouts</h3>
<div id='recentShouts'>
	<% _.each(recentShouts,function(e){ %>
		<div class='recentShout' id='<%= e._id%>'>
			<div class='row'>
				<div class='col col-lg-12'><%=e.content%></div>
			</div>
			<div class='row'>
				<div class='col col-lg-3 timeStamp'>
					<%=e.created_At%>
				</div>
				<div class='col col-offset-6 col-lg-1'>
					<a href="#" class='editShout'>Edit</a>
				</div>
				<div class='col col-lg-2'>
					<a href="#" class='deleteShout'>Delete</a>
				</div>
			</div>
		</div>
	<%})%>
</div>
<h3>Chats</h3>
<div id='recentChats'>
	<% _.each(recentChats,function(e){ %>
		<a href="/chatLog/<%= e._id%>" class='recentChat' id='<%= e._id%>' target='_blank'>
			<div class='row'>
				<div class='col col-lg-9 thin-font'>
						<% if(e.revealed){%>
						<%	var myName = document.getElementById('user-name') %>
						<%  myName = (myName.innerText || myName.textContent)%>
						<%  myName = myName.slice(1) %>
						<%	var twinName = e.names[0] === myName ? e.names[1] : e.names[0] %>
							<%= twinName%> and you.	
						<%}else{%>
							 Stranger and you
						<%}%>
				</div>
				<div class='col col-lg-3 small-fonts-1'>
					<span class='messagesLength thin-font'><%=e.messagesLength%></span><br>
					Messages
				</div>
			</div>
			<div class='row'>
				<div class='col col-lg-12 timeStamp'>
					<%=e.ended%>
				</div>
			</div>
		</a>
	<%})%>
</div>
<hr>
<button class='btn btn-default btn-small'>Back</button>