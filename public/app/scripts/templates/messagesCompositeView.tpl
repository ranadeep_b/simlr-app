<div class='row' id='threadInfo'>
	<div class='col col-offset-2 col-lg-2'>
		<img src="#" id='twinPic'>
	</div>
	<div class='col col-lg-8'>
		<h3 id='twinName'></h3>
		<div class='row'>
			<div class='col col-lg-12'>
				<span id='twinLastSeen'></span>
			</div>
		</div>
	</div>
</div>
<div class='row' id='expireyDetails'>
</div>
<ul id='threadMessagesContainer'>
</ul>
<div class='row'>
	<div class='col col-lg-12'>
		<textarea id='threadMessageNew' placeholder='Hey whatsup ?'></textarea>
		<button class='btn btn-default' id='threadMessageSend'>Send</button>
	</div>
</div>