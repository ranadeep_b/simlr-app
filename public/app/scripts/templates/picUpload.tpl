<h2>Select a picture..</h2>
<div id='current-pic'>
	<span>Current: </span>
	<img src="/getMyPic">
</div>
<div>
<form id='picUploadForm' enctype='multipart/form-data'><input type='file' id='picUploadInput' name='myPic'>
	<div class='row' style='margin-top : 10px;'>
		<div class='col col-lg-4 col-offset-4'>
			<button id='pic-choose-button' class='btn btn-default'>Select pic..</button>
		</div>
		<div class='col col-lg-4'>
			<button class='btn btn-default btn-small' style='margin-top : 7px;'>Back</button>
		</div>
	</div>
</form>
</div>