<div class='chatBox'>
	<div id='chatBoxWrapper'>
			<p id='firstChatMessage' class='feedback'>
				<span id='psstContainer'>Say Hi ! . Exchange Ideas,network,share . Please dont be rude</span>
				<br>
				<span id='isTyping'></span>
			</p>
			<hr>
		<ul id='chatMessagesContainer'>
		</ul>

	</div>
	<div id='chatSubmitArea'>
		<textarea id='chatTextArea'></textarea>
		<button id='chatSubmitButton' class='btn btn-default'>Send</button>
	</div>
	<div class='row'>
		<div class='col col-lg-6'>
			<a href="#" id='byeButton' class='feedback'>Say bye</a>
		</div>
		<div class='col col-lg-6 report'>
			<a href="#" id='reportChat' class='feedback'>Report rude/ill mannered</a>
		</div>
	</div>
</div>