<div class='threadMessage <%=sender%>'>
	<div class='row'>
		<div class='col col-lg-12'>
			<p class='threadMessageContent'><%= message%></p>
		</div>
	</div>
	<div class='row'>
		<div class='col col-offset-8 col-lg-4'>
			<span class='timeStamp' id ='<%= timeSent%>'></span>
		</div>
	</div>
</div>