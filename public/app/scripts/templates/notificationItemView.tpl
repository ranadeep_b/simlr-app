<div>
	<a href="<%= actionUrl%>" class="notification">
		<div class='row'>
			<div class='col-lg-2 col col-offset-1'>
				<span class='notificationImage' id='<%="notification"+type%>'></span>
			</div>
			<div class='col-lg-9' style='text-align : left;'>
				<span class='notificationContent'><%= content%></span>
			</div>
		</div>
	</a>
</div>