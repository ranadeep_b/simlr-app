<div class='row'>
	<div class='col col-lg-12'>
		<div id='convoFeedback' class='row'>
			<h3>How was the conversation ?</h3>
			<div class='col col-lg-4 col-offset-4'>
				<select id='chatRating'>
					<option value='3'>Amazing/Enlightening</option>
					<option value='2'>Interesting</option>
					<option value='1' selected='selected'> Ok</option>
					<option value='-1'>Boring</option>
					<option value='-3'>Lifeless</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class='row'>
	<div class='col col-lg-12'>
		<div id='twinFeedback'>
			<h3>How was the person ?</h3>
			<div id='traitChoices'>
				<div class='row'>
						<div class='col col-lg-12'>
							<div class='row'>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a1' class='box-container'><a class='box'></a><span class='hoverText'>Funny</span></div>
								</div>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a2' class='box-container'><a class='box'></a><span class='hoverText'>Frank</span></div>
								</div>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a3' class='box-container'><a class='box'></a><span class='hoverText'>Sweet</span></div>
								</div>
							</div>
							<div class='row'>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a4' class='box-container'><a class='box'></a><span class='hoverText'>Crazy</span></div>
								</div>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a5' class='box-container'><a class='box'></a><span class='hoverText'>Geeky</span></div>
								</div>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a6' class='box-container'><a class='box'></a><span class='hoverText'>Badass</span></div>
								</div>
							</div>
							<div class='row'>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a7' class='box-container'><a class='box'></a><span class='hoverText'>Sporty</span></div>
								</div>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a8' class='box-container'><a class='box'></a><span class='hoverText'>Smart</span></div>
								</div>
								<div class='col col-lg-3 col-offset-1'>
									<div id='a9' class='box-container'><a class='box'></a><span class='hoverText'>Shy</span></div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id='twinTestimonyContainer' class='row'>
	<h3>Make a remark !</h3>
	<div class='col col-offset-4 col-lg-4'>
		<textarea id='twinTestimony' placeholder='He is soo funny, i think if anyone needs a writer this is the guy.(keep it simple)'></textarea>
	</div>
	<div class='col col-lg-4'>
		<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fsimlr.me&amp;width&amp;height=80&amp;colorscheme=light&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;send=true&amp;appId=543381882362634" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:60px;" allowtransparency="true"></iframe>
	</div>
</div>
<hr>
<div class='row'>
	<div class='col col-lg-3 col-offset-9'>
		<button id='sendFeedback' class='btn btn-default'>Continue</button>
	</div>
</div>
<input type='hidden' value='<%= chatId%>'>


