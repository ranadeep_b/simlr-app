require.config({

	paths : {
		jquery 		  : './vendor/libs/jquery-1.10.1.min',
		underscore 	  : './vendor/libs/underscore-min',
		backbone 	  : './vendor/libs/backbone-min',
		marionette	  : './vendor/libs/backbone.marionette',
		wreqr		  : './vendor/plugins/backbone.wreqr.min',
		text 		  : './vendor/plugins/text',
		tpl 		  : './vendor/plugins/tpl',
		socketio 	  : '../socket.io/socket.io.min',
		spin		  : './vendor/plugins/spin.min',
		shuffle 	  : './vendor/plugins/jquery.shuffleLetters',
		magicSuggest  : './vendor/plugins/magicsuggest-1.3.0-min',
		mCustomScroll : './vendor/plugins/jquery.mCustomScrollbar.concat.min',
		imagesloaded  : './vendor/plugins/imagesloaded',
		qTip		  : './vendor/plugins/jquery.qtip.min',
		visibility	  : './vendor/plugins/visibility',
		tab 		  : './vendor/plugins/tab',
		dropdown 	  : './vendor/plugins/dropdown',
		interestsMap  : './interestsMap',
		moment 		  : './vendor/plugins/moment.min',
		favicon 	  : './vendor/plugins/tinycon'

	},

	waitSeconds 	  : 180,

	shim : {

		socketio : {
			exports : 'io'
		},

		underscore : {
			exports : '_'
		},

		backbone : {
			deps : ['underscore','jquery'],
			exports : 'Backbone'
		},

		marionette : {
			deps : ['backbone'],
			exports : 'Backbone.Marionette'
		},

		wreqr 	   : {
			deps : ['backbone'],
			exports : 'Backbone.Wreqr'
		},

		shuffle : {
			deps : ['jquery'],
			exports : 'jQuery.fn.shuffleLetters'
		},

		magicSuggest : {
			deps : ['jquery'],
			exports : 'jQuery.fn.magicSuggest'
		},

		mCustomScroll : {
			deps : ['jquery'],
			exports : 'jQuery.fn.mCustomScrollbar'
		},

		visibility : {
			exports : 'Visibility'
		},

		tab 	   : {
			deps : ['jquery'],
			exports : 'jQuery.fn.tab'
		},

		dropdown   : {
			deps : ['jquery'],
			exports : 'jQuery.fn.dropdown'
		},

		moment 	   : {
			exports : 'moment'
		},

		favicon    : {
			exports : 'Tinycon'
		}

	},

	tpl 	: {
		extension : '.tpl'
	}
});

//Initialise 
require([
		'backbone',
		'routers/index',
		'app',
		],function	(Backbone ,Router ,app){
			//var presence 	= io.connect(w.protocol+'//'+w.host+'/presence');

			app.start();

			Backbone.history.start();


});



