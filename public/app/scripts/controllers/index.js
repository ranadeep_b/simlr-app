define(['../vent','jquery','../app','backbone','marionette','../io','../views/spinnerView','../views/spinnerView2','../routers/index',
		'../models/miniProfile','../views/miniProfileItemView',
		'../models/homeCenter','../views/homeCentreItemView',
		'../collections/shoutsCollection','../models/shout','../models/shoutSearch',
		'../views/shoutBoxLayout','../views/shoutSearchItemView','../views/shoutsWrapperCompositeView',
		'../models/twinProfile','../models/chatMessage','../models/chatClosed',
		'../collections/chatsCollection',
		'../views/twinProfileItemView','../views/chatItemView','../views/chatClosedItemView','../views/chatCompositeView',
		'../views/homeLayoutView',
		'../views/chatLayoutView',
		'../views/chatClosedLayoutView',
		'../collections/newMessages',
		'../views/newMessagesCompositeView',
		'../views/sendMessageCompositeView',
		'../models/myActivity',
		'../views/myActivityItemView',
		'../models/contact','../collections/contacts',
		'../views/allContactsCompositeView',
		'../models/settings',
		'../views/settingsItemView',
		'../collections/notifications',
		'../views/notificationsCompositeView',
		'../collections/messages',
		'../views/messagesCompositeView',
		'../models/question','../collections/questions',
		'../views/questionsCompositeView',
		'../views/complimentsItemView',
		'../models/compliments'
	],function(vent,$,App,Backbone,Marionette,socket,spinner,spinner2,RouterInstance,
		miniProfile,miniProfileItemView,
		homeCenter,homeCentreItemView,
		shoutsCollection,shout,shoutSearch,
		shoutBoxLayout,shoutSearchItemView,shoutsWrapperCompositeView,
		twinProfile,chatMessage,chatClosed,
		chatsCollection,
		twinprofileItemView,chatItemView,chatClosedItemView,chatCompositeView,
		homeLayoutView,
		chatLayoutView,
		chatClosedLayoutView,
		newMessages,
		newMessagesCompositeView,
		sendMessageCompositeView,
		myActivity,
		myActivityItemView,
		Contact,Contacts,
		allContactsCompositeView,
		settings,
		settingsItemView,
		notifications,
		notificationsCompositeView,
		messages,
		messagesCompositeView,
		question,questions,
		questionsCompositeView,
		complimentsItemView,
		complimentsModel
		){

	var currentHomeLayout = App.homeLayout;

	var Controller = Marionette.Controller.extend({
		home 				: function(){
			var homeLayout = new homeLayoutView;
			App.main.show(homeLayout);
			App.homeLayout = currentHomeLayout = homeLayout;
			//show loading box , on done close it 
			//miniProfile
			var miniProfileModel = new miniProfile;
			vent.trigger('loading:miniprofile');
			miniProfileModel.fetch().done(function(){
				spinner2.stop();
				var view = new miniProfileItemView({model : miniProfileModel});
				homeLayout.miniprofile.show(view);		
			})
			//home center
			var homeCenterModel = new homeCenter;
			homeLayout.center.show(new homeCentreItemView({model : homeCenterModel}));
			//initialise shoutbox
			var shoutBoxView = new shoutBoxLayout;
			homeLayout.shoutbox.show(shoutBoxView);

			var shoutsCollectionInstance = new shoutsCollection;
			vent.trigger('loading:shoutbox');
			shoutsCollectionInstance.fetch().done(function(data){
				spinner.stop();
				shoutBoxView.shoutSearchFilter.show(new shoutSearchItemView({model : new shoutSearch}));
				shoutBoxView.shoutsWrapper.show(new shoutsWrapperCompositeView({collection : shoutsCollectionInstance }));
			});
			vent.trigger('refreshHashbang');
		},

		initChat 			: function(){
			//obj has chatId and twinId
			vent.once('initChatControllerInfo',function(obj){

					$('.overlay > div').remove();
					$('.overlay').hide()
					$('#main').fadeIn();

					var chatLayout = new chatLayoutView
					App.main.show(chatLayout);
					vent.trigger('sendChatInfoToView',obj)
					//twinprofileView
					socket.on('readyToReveal',function(identityObj){
						var twinProfileModel = new twinProfile({urlRoot : '/getTwinProfile',url : '/getTwinProfile/'+obj.chatId});
						twinProfileModel.fetch({url : '/getTwinProfile/'+obj.chatId}).done(function(){
							var view = new twinprofileItemView({model : twinProfileModel});
							chatLayout.profilebox.show(view);
							vent.trigger('revealed',identityObj)
							vent.trigger('sendChatInfoToView',obj);
						})
						//alert !!!! the #bio is invisible , so re render model 
					});

					socket.on('contactRequest',function(){
						vent.trigger('contactRequest');
					});

					socket.on('contactResponse',function(response){
						vent.trigger('contactResponse',response);
					});

					var twinProfileModel = new twinProfile({urlRoot : '/getTwinProfile',url : '/getTwinProfile/'+obj.chatId});
					twinProfileModel.fetch({url : '/getTwinProfile/'+obj.chatId}).done(function(){
						var view = new twinprofileItemView({model : twinProfileModel});
						chatLayout.profilebox.show(view);
						vent.trigger('sendChatInfoToView',obj);
					})
					//chatView
					var chatsCollectionInstance = new chatsCollection;
					chatLayout.chatbox.show(new chatCompositeView({collection : chatsCollectionInstance}));

					//questionBox
					var questionsCollection = new questions;
					chatLayout.questionbox.show(new questionsCompositeView({collection : questionsCollection}));
					vent.trigger('sendChatInfoToView',obj);
			});
		},

		closeChat 			: function(){
			socket.removeAllListeners('readyToReveal');
			socket.removeAllListeners('contactRequest');
			socket.removeAllListeners('contactResponse');

			vent.once('sendChatIdToController',function(chatId){
				//initChatClosedLayout
				var chatClosedLayout = new chatClosedLayoutView;
				App.main.show(chatClosedLayout);

				var chatClosedModel = new chatClosed({chatId : chatId});
				chatClosedLayout.feedback.show(new chatClosedItemView({model : chatClosedModel}));
				//on continue app.main.show(currentHomeLayout) or redirect to home
			});

		},

		getLatestThreads 	: function(){
			var collection = new newMessages;
			vent.trigger('loading:center');
			collection.fetch().done(function(d,ts,xhr){
				spinner.stop();
				var messagesView = new newMessagesCompositeView({collection : collection });
				currentHomeLayout.center.show(messagesView);
			})
		},

		popupNotifications  : function(){
			var collection = new notifications;
			vent.trigger('loading:center');
			collection.fetch().done(function(data){
				spinner.stop();
				var notificationsView = new notificationsCompositeView({collection : collection});
				currentHomeLayout.center.show(notificationsView);
			})
		},

		sendNewMessage 		: function(){
			var collection = new Contacts;
			vent.trigger('loading:center');
			collection.fetch().done(function(d,ts,xhr){
				spinner.stop();
				var sendMessageView = new sendMessageCompositeView({collection : collection})
				currentHomeLayout.center.show(sendMessageView);
			})
		},

		getThread 			: function(threadId){
			vent.trigger('loading:center');
			$.ajax('/thread/'+threadId).done(function(d,ts){
				spinner.stop();
				var collection = new messages(d.data);
				var messagesCompositeViewInstance = new messagesCompositeView({collection : collection});
				currentHomeLayout.center.show(messagesCompositeViewInstance);
				vent.trigger('sendThreadInfoToView',d.threadInfo);
			})
		},

		getAllThreads 		: function(){
			
		},

		myActivity 			: function(){
			var myActivityModel = new myActivity;
				vent.trigger('loading:center');
				myActivityModel.fetch().done(function(){
					spinner.stop();
					currentHomeLayout.center.show(new myActivityItemView({model : myActivityModel}));
				})
		},

		getContacts 		: function(){
			var collection = new Contacts;
			vent.trigger('loading:center');
			collection.fetch().done(function(d,ts,xhr){
				spinner.stop();
				currentHomeLayout.center.show(new allContactsCompositeView({collection : collection}));
			})
		},

		getSettings 		: function(){
			var settingsModel = new settings;
			vent.trigger('loading:center');
			settingsModel.fetch().done(function(){
				spinner.stop();
				currentHomeLayout.center.show(new settingsItemView({model : settingsModel}));
			})
		},

		searchShouts 	    : function(query){
			var context = this;
			$('#temporary').remove();
			var target = document.getElementById('shoutSearchSubmit');
				spinner.spin(target);
			var divHtml = '<div id="temporary" style="margin : 10px;" class="row"><div class="col col-lg-2"><button class="btn btn-small btn-default" id="searchOk">Back</button></div></div>';
				$(divHtml).appendTo('#search-filter-wrapper');
			$.ajax({
				url : '/search',
				data : {
					query : query
				}
			})
			.done(function(d,ts){
				spinner.stop();
				if(d.length > 0 &&'content' in d[0]){
					var successHtml = '<div class="col col-lg-10"><h5>'+d.length+' results found.</h5></div>';
						$(successHtml).appendTo('#temporary').hide(0).fadeIn();
						currentHomeLayout.shoutbox.currentView.shoutsWrapper.currentView.collection.reset(d);
				}
				else {
					var html = '<div class="col col-lg-10"><h5>No search results found</h5></div>'
						$(html).appendTo('#temporary').hide(0).fadeIn();
						currentHomeLayout.shoutbox.currentView.shoutsWrapper.currentView.collection.reset(0);
				}
			})
			.fail(function(){
				spinner.stop();
			});

			$('#searchOk').on('click',function(e){
				$(this).off('click');
				$('#temporary').remove();
				vent.trigger('takeMeHome');
			})
			//show spin, load , add to collection , on back context.home()
		},

		getCompliments 	: function(){
			var complimentsInstance = new complimentsModel;
			vent.trigger('loading:center');
			complimentsInstance.fetch().done(function(){
				spinner.stop();
				currentHomeLayout.center.show(new complimentsItemView({model : complimentsInstance}))
			});
		}

	})
	return new Controller; // Since controller is only used once 
});