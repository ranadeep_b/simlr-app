define(['../vent','underscore','../templates','marionette','jquery','shuffle','magicSuggest','../io','mCustomScroll','visibility'],
	function(vent,_,templates,Marionette,$,shuffle,magicSuggest,socket,mCustomScrollbar,Visibility){

	var homeCenterItemView = Marionette.ItemView.extend({

		template 	: templates.homeCenterTemplate,

		events		: {
			'click .box-container' 			: 'checkit',
			'focus #the-great-shoutbox' 	: 'initMagicSuggest',
			'keypress #the-great-shoutbox'  : 'charLimitCheck',
			'click #clear-input' 			: 'clearInput',
			'click #shout-submit-button'	: 'submitShout',
			'click #preferences > button'   : 'discoverPeople',
			'click #clearChecked' 			: 'clearChecked'
		},

		triggers 	: {},

		ui 		 	: {},

		flasherTimeOutId : 0,

		typeTraitArray : [],

		typeInterestArray : [],

		paramArray		: [],

		alreadyBusy 	: false,

		visibilityListenerId : null,

		onShow      : function(){
			var context = this;
			//Init Shuffler 
			var list = ["likes to program in python ?",
						"is preparing for GATE ?",
						"likes to talk about music ?",
						"is working on a startup ?"
						];
			var max  = list.length-1;
			var count = 0;
			function flasher(){
				$('#shuffle').text('')
				$('#shuffle').shuffleLetters({text : list[count]})
				if(count == max) count = 0;
				else count += 1;
			}
			context.flasherTimeOutId = setInterval(flasher,5000);

			//socket events subscribe

			vent.on('AreYouReady',function(obj){
				if(context.alreadyBusy) {
					socket.emit('twinAreYouReadyResponse',{response : false});
					return;
				}
				if(obj.type === 'discoverPeople'){
					window.confirmModal('Knock Knock','We found a person similar to you, Are you interested to connect ?');
					vent.on('confirmClicked',function(response){
						if(response){
							$.ajax({
								url 	: '/initiateChat',
								type 	: 'POST',
								data 	: {
									mySocketId 	 : socket.socket.sessionid,
									twinSocketId : obj.initiatorSocketId
								},
								dataType : 'json'
							})
							.done(function(d){
								socket.emit('twinAreYouReadyResponse',{response : response,twinSocketId : socket.socket.sessionid,chatId : d.chatId});
								vent.trigger('initChat',{twinSocketId : obj.initiatorSocketId,chatId : d.chatId});
							})
							.fail(function(){
								socket.emit('twinAreYouReadyResponse',{response : false})
							})
						}
						else {
							socket.emit('twinAreYouReadyResponse',{response : response})
							$.ajax({
								url  :'/report',
								data : {
									type  : 'chatCancel'
								}
							});
						}

						vent.off('confirmClicked');
					});
				}
				else{
					window.confirmModal('Yay !','Someone responded to your shout and is online !.Click ok to connect now');
					vent.on('confirmClicked',function(response){
						if(response){
							$.ajax({
								url 	: '/shoutRespondInitiateChat',
								type 	: 'POST',
								data 	: {
									shoutId 		: obj.shoutId,
									mySocketId  	: socket.socket.sessionid,
									twinSocketId	: obj.initiatorSocketId,
									twinId 			: obj.initiatorId
								},
								dataType : 'json'
							})
							.done(function(d){
								socket.emit('twinAreYouReadyResponse',{response : response,twinSocketId : socket.socket.sessionid,chatId : d.chatId});
								vent.trigger('initChat',{twinSocketId : obj.initiatorSocketId,chatId : d.chatId});
							})
							.fail(function(){
								socket.emit('twinAreYouReadyResponse',{response : false})
							})
						}
						else {
							socket.emit('twinAreYouReadyResponse',{response : response})
							$.ajax({
								url  :'/report',
								data : {
									type  : 'chatCancel'
								}
							});
						}
						vent.off('confirmClicked');
					});
				}

				})

			//Scrollbar implementation
			$('#interestParams').mCustomScrollbar({
				set_height 	   	  : 240,
				scrollButtons 	  : {
					enable : true
				},
				autoHideScrollbar : true,
				theme 			  : 'dark-thin', 
				advanced   		  : {updateBrowserResize : true}
			})

			context.typeTraitArray = [];
			context.typeInterestArray = [];
			context.paramArray 		= [];
			$('#clearChecked').hide(0);

			//Change status when visible
   			context.visibilityListenerId = Visibility.change(function(e,state){
   				if(state === 'hidden'){
   					context.requestStatusChange(0);
   				}
   				else context.requestStatusChange(1);
   			});
		},

		requestStatusChange : function(status){
			var context = this;
			$.ajax({
				url : '/changeStatus',
				type: 'POST',
				data: {
					status : status,
					mySocketId : socket.socket.sessionid
				}
			})
			.fail(function(xhr,ts,err){
				setTimeout(context.requestStatusChange(status),2000);
			})

		},

		waitForResponse : function(){			
			var context = this;
			if(context.alreadyBusy) return;
 			context.alreadyBusy = true;
			socket.removeAllListeners('AreYouReadyResponse');
			context.requestStatusChange(0)
			context.alertMessage('We found you an interesting person, waiting for him/her to accept.',6000)

			socket.once('AreYouReadyResponse',function(responseObj){
				if(responseObj.response === true){
					vent.trigger('initChat',{twinSocketId : responseObj.twinSocketId,chatId : responseObj.chatId});
					context.alreadyBusy = false;
				}
				else {
					$('#shouts-container .btn,.btn-block').removeClass('disabled');
					$('.alert').remove();
					context.alertMessage('The other person cancelled the request , please try again',3000);
					context.alreadyBusy = false;
					$('#entertainBox').fadeOut(function(){
						$(this).remove();
					})
					$('#magicBox').fadeIn();
					$.each(context.paramArray,
						function(i,e){
							$(e).click();
						}
					);
					context.requestStatusChange(1);
				}
			})  				

		},

		entertain : function(){
			//fadeOut #magicBox
			var html = '<div id="entertainBox" class="row"> \
							<div class="col col-lg-12"> \
								<h3>We are connecting you to someone as awesome as you </h3> \
								<div class="tips"> \
									<p class="tip"> * Maintain dignity</p> \
									<p class="tip"> * Exchange ideas, get help, give advices</p> \
									<p class="tip"> * Increase your network</p> \
									<p class="tip"> * Keep cool and chat happy : )</p> \
								</div> \
							</div> \
						</div>';
			$('#magicBox').fadeOut();
			$('#centerBottomContainer').append(html).hide(0).fadeIn();
		},

		checkit		: function(e){
			var context = this;
			$(e.currentTarget).toggleClass('checked');  // Dont use 'this' , since its the iteview not the element . currentTarget - obj listening to event and target is obj that captured event
			var title = $(e.currentTarget).children('span').text(),
				type  = $(e.currentTarget).attr('id')[0] === 'a' ? 'trait' : 'interest';
			this.updateCheckUI(type,title);
			context.paramArray = $('.checked').map(function(i,e){return $(e).attr('id')});
		},

		onClose     : function(){

		},

		alertMessage : function(msg,time,type){
			if(!type) type = 'alert-error'
			$('#magicBox h3').hide(0);
			var html = '<div class="alert '+type+'"><button type="button" class="close" data-dismiss="alert">×</button>'+msg+'</div>'
			$(html).prependTo('#centerBottomContainer').delay(time).fadeOut(1000,function(){
				$('#magicBox h3').fadeIn(100);
			});
			$('.close').click(function(e){
				$(e.currentTarget).parent().remove();
				$('#magicBox h3').fadeIn(100);
			})

		},

		initMagicSuggest : function(){
			if($('#clear-input').length) return;
			//Magic Suggest and clear
			var msinput = $('<input/>', {type : 'text', autocomplete : 'off' ,placeholder : 'Enter tags here..'});
			msinput.appendTo('#tags-input-box').fadeIn();
			$('#tags-input-box input').magicSuggest({
				maxSelection 	: 4,
				hideTrigger  	: true,
				maxDropHeight 	: 0
			});
			var clear   = $('<a/>',{ text : 'Clear', href :'#', id : 'clear-input'});
			clear.appendTo('#shout-create').hide().fadeIn()
		},

		clearInput : function(e){
			if(typeof e !== 'undefined') e.preventDefault();
			$('#tags-input-box').html('');
			$('#the-great-shoutbox').val('');
			$('#clear-input').remove();
		},

		submitShout : function(){
			var context = this;
			if(!$('#the-great-shoutbox').val()) this.alertMessage('Shout something ..just type in box above',5000,'alert-info');
			else {
				if( $('#the-great-shoutbox').val().length > 200) this.alertMessage('Message too long, max 200 characters allowed',5000)
				else {
					$('#shout-submit-button').addClass('disabled');
					var shout = $('#the-great-shoutbox').val();
					var tags  = eval($('#ms-input-0 ~ input').val()) ? eval($('#ms-input-0 ~ input').val()).join(':') : [];
					var data  = {shout : shout , tags : tags};
					$.ajax({
						url 	: '/shoutSubmit',
						type	: 'POST',
						data 	: data,
						dataType: 'json'
					})
					.done(function(data,ts){
						if(!data.error) context.alertMessage(data.msg,4000,'alert-success');
						context.clearInput();
					})
					.always(function(){
						$('#shout-submit-button').removeClass('disabled');
					})
					.fail(function(xhr,ts,err){
						this.alertMessage('There is a problem , please check your connection',3000)
					});
				}
			}
			//valiadate
			//throttle submit 
			//append ; at last and append colon seperated Tags
		},

		discoverPeople : function(){
			var context = this;
				context.initiatedByMe = true;
			//fadeout discover people area , with loading , finding some interesting 
			//show faces 
			//this.showOnlineUsersPics();
			//getUserParams
				var userParams = [];
				$('.checked').each(function(i,e){
					userParams.push($(e).attr('id').slice(1));				
				})
				var POSTBody = {
					params 		: userParams.join(':'),
					socketId 	: socket.socket.sessionid 
				}

				$('#shouts-container .btn,.btn-block').addClass('disabled');
			//submit form
			$.ajax({
				url : '/abracadabra',
				type: 'POST',
				data: POSTBody,
				dataType : 'json'
			})
			.always(function(p1,p2,p3){
				context.entertain();
			})
			.done(function(data,ts,xhr){
				if(data.error) {
					context.alertMessage(data.msg,3000);
					context.$('#entertainBox').fadeOut(0,function(){
						$(this).remove();
					});
					$('#shouts-container .btn,.btn-block').removeClass('disabled');
					$('#magicBox').fadeIn(0);
				}
				else {
					context.waitForResponse();
				}
			})
			.fail(function(xhr,ts,err){
				$('#shouts-container .btn,.btn-block').removeClass('disabled');
				context.alertMessage('Please check your internet connection or try again later',3000)
			});
		},

		updateCheckUI : function(type,title){
			var context = this;

			if(type === 'trait') {
				if(context.typeTraitArray.indexOf(title) !== -1){
					context.typeTraitArray = _.without(context.typeTraitArray,title);
				}
				else {
					context.typeTraitArray.push(title);
				}
			}
			else {
				if(context.typeInterestArray.indexOf(title) !== -1){
					//remove from title
					context.typeInterestArray = _.without(context.typeInterestArray,title);
				}
				else {
					context.typeInterestArray.push(title);
				}
			}

			var findMe 		 = 'Find me',
				people 		 = 'people',
				interestedIn = 'interested in',
				traits       = context.typeTraitArray.length > 0 ? '<i style="color : #ed0f69;">'+context.typeTraitArray.join(',')+' '+'</i>' : '',
				interests    = context.typeInterestArray.length > 0 ? interestedIn+' '+'<i style="color : #1abc9c">'+context.typeInterestArray.join(',')+'</i>' : '';
			$('#magicBox h3').html(findMe+' '+traits+people+' '+interests);

			if(context.typeTraitArray.length + context.typeInterestArray.length === 0) {
				$('#magicBox h3').text('Select the kind of people you like to talk to');
				$('#clearChecked').hide(0);
			}

			if(context.typeTraitArray.length + context.typeInterestArray.length === 1) $('#clearChecked').fadeIn(1000);
		
			var length = $('#magicBox h3').text().length;
			 $('#magicBox h3').css({'font-size' : 30-length/20});
		},

		clearChecked : function(e){
			e.preventDefault();
			if($('.checked').length){
				$('.checked').each(function(i,e){
					$(e).click();
				})
			}
		},

		onBeforeClose : function(){
			var context = this;
			vent.off('AreYouReady');
			vent.off('confirmClicked');
			Visibility.unbind(context.visibilityListenerId);
			clearTimeout(context.flasherTimeOutId);	
		}

	});
	return homeCenterItemView;
})