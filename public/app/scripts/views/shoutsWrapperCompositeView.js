define(['underscore','../templates','../models/shout','./shoutItemView','marionette','mCustomScroll','jquery','moment','./spinnerView'],
	function(_,templates,shout,shoutItemView,Marionette,mCustomScrollbar,$,moment,spinner){
	var shoutsWrapperCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.shoutsWrapperTemplate,

		itemView 		  : shoutItemView,

		itemViewContainer : '#shouts-container',

		events			  : {},

		triggers 		  : {},

		ui 		 		  : {},

		offset 			  : 0,

		pageNo 			  : 1,

		refreshTimeoutId  : null,

		onShow 			  : function(){
			var context = this;
			//Initiating Spinner
			context.refreshTimeoutId = setInterval(function(){
				$('#shoutbox-rg .timeStamp').each(function(i,e){
					$(e).text(moment(parseInt($(e).attr('id'))).fromNow());
				})
			},60000)

			var getLatestShouts = function(lastTimeSent){
				var target = document.getElementById('shouts-wrapper');
				spinner.spin(target);
				$.ajax({
					url  : '/getLatestShouts',
					data : {
						lastTimeSent : lastTimeSent
					} 
				})
				.done(function(d,ts){
					spinner.stop();
					if('content' in d[0]){
						context.offset += d.length
						_.each(d,function(shoutData){
							var shoutModel = new shout(shoutData),
							    shoutView  = new shoutItemView({model : shoutModel});
							    var html   = shoutView.render().$el;
							    $(html).prependTo('#shouts-container').hide(0).fadeIn(3000);
						});
					}
					else {
						var html = "<h5>No recent shouts to update.</h5><hr>"
						$(html).prependTo('#shouts-container').hide(0).fadeIn().delay(1000).fadeOut(1000,function(){
							$(this).remove();
						});
					}
				})
			}
			//Scrollbar 
			$('#shouts-container').parent().mCustomScrollbar({
				set_width 		  : 400,
				set_height 	   	  : 440,
				scrollButtons 	  : {
					enable : true
				},
				autoHideScrollbar : true, 
				advanced   		  : {updateBrowserResize : true},
				theme 			  : 'dark',
				callbacks 		  : {
					onTotalScroll : function(){
						var target = document.getElementById('forSpinner');
							spinner.spin(target);
						$.ajax({
							url  : '/getShouts',
							data : {
								pageNo : context.pageNo,
								offset : context.offset
							} 
						})
						.done(function(d,ts){
							spinner.stop();
							if('content' in d[0]){
								++context.pageNo;
								context.collection.add(d);
							}
							else {
								var html = "<h5>No more shouts to display</h5>"
								$(html).appendTo('#forSpinner').hide(0).fadeIn().delay(2000).fadeOut(function(){
									$(this).remove();
								});
							}
						})
					},
					onTotalScrollBack : function(){
						var lastTimeSent = parseInt($('.shout .shout-time-elapse').eq(0).attr('id'));
						$('.shout .shout-time-elapse').each(function(i,e){
							lastTimeSent = lastTimeSent < parseInt($(e).attr('id')) ? parseInt($(e).attr('id')) : lastTimeSent; 
						});
						getLatestShouts(lastTimeSent);
					}
				}

			});
		}

	});
	return shoutsWrapperCompositeView;
})