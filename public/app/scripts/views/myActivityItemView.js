define(['../vent','../templates','marionette','moment'],function(vent,templates,Marionette,moment){
	var myActivityItemView = Marionette.ItemView.extend({

		template 	: templates.myActivityItemView,

		events		: {
			'click .editShout' 	 : 'editShout',
			'click .deleteShout' : 'deleteShout',
			'click .btn-small' 	 : 'goBack'
		},

		triggers 	: {},

		ui 		 	: {},

		onShow 		: function(){
			$('#center-rg .timeStamp').each(function(i,e){
				var time = parseInt($(e).text());
				$(e).text(moment(time).fromNow());
			})
		},

		editShout   : function(e){
			e.preventDefault();
			if($('#editContainer').length > 0) return;
			var shoutElement 	 = $(e.currentTarget).parent().parent().parent(),
				prevShoutContent = shoutElement.find('.col-lg-12').text(), 
				shoutId      	 = shoutElement.attr('id');
			var html = '<div class="row" id="editContainer"><div class="col col-lg-8"><textarea id="shoutContent"></textarea></div>'
						+'<div class="col col-lg-4"><button style ="margin : 12px" class="btn btn-small btn-default" id="updateShoutContent">Update</button>'
						+'<button class="btn btn-default btn-small" id="cancel">Cancel</button</div></div>'
			$(html).appendTo('#'+shoutId).hide(0).fadeIn();
			$('#editContainer textarea').val(prevShoutContent);

			$('#updateShoutContent').click(function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				var newShoutContent = $('#editContainer textarea').val();
				$.ajax({
					url 	: '/modifyShout/'+shoutId,
					type 	: 'POST',
					data  	: {
						type : 'edit',
						shoutContent : newShoutContent
					}
				})
				.done(function(){
					shoutElement.find('.col-lg-12').text(newShoutContent);
					$('#editContainer').html('<p style="color : #ed0f69;text-align : center;"> Updated successfully</p>').fadeOut(function(){
						$(this).remove();
					})
				})
				.fail(function(){
					$('#editContainer').html('<p style="color : #ed0f69;text-align : center;>"Cannot be updated . Please check & try again</p>').fadeOut(function(){
						$(this).remove();
					})
				})
			});

			$('#cancel').click(function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				$('#editContainer').fadeOut(function(){
						$(this).remove();
				})
			})
			
		},

		deleteShout : function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			var shoutId = $(e.currentTarget).parent().parent().parent().attr('id');
			$.ajax({
				url 	: '/modifyShout/'+shoutId,
				type 	: 'POST',
				data 	: {
					type : 'delete',
				} 
			})
			.done(function(){
				$(e.currentTarget).parent().html('deleted.').fadeOut(800,function(){
					$(this).parent().parent().remove();
				})
			})
			.fail(function(){
				//
			})
		},

		goBack 		: function(){
			vent.trigger('goBackHomeCenter');
		}

	});
	return myActivityItemView;
})