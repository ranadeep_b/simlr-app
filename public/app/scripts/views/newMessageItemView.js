define(['../templates','marionette'],function(templates,Marionette){
	var newMessageItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		className 	: 'newMessage',

		template 	: templates.newMessageTemplate,

		events		: {},

		triggers 	: {},

		ui 		 	: {},

	});
	return newMessageItemView;
})