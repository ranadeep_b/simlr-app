define(['../templates','marionette','../vent','../io','interestsMap','qTip'],function(templates,Marionette,vent,socket,map,qtip){
	var twinProfileItemView = Marionette.ItemView.extend({

		template 	: templates.twinProfileTemplate,

		events		: {
			'click #reveal'  	 : 'sendRevealRequest', 
		},

		triggers 	: {},

		ui 		 	: {

		},

		chatInfo    : null,

		toolTip 	: null,

		onShow 			  : function(){
			var context = this;

			vent.once('sendChatInfoToView',function(obj){
				context.chatInfo = obj;
			});

			vent.once('revealed',function(identityObj){
				var html  = '<img src='+identityObj.picUrl+' id="twinImage"></img><h3 id="twinName">'+identityObj.name+'</h3>';
				$('#bio').children().remove();
				$('#bio').append(html).hide(0).fadeIn();
				$('#reveal').fadeOut(0,function(){
					$(this).remove();
				})
				$('#contactBox').fadeIn();
				$('#stayInTouch').on('click',{viewContext : context},context.sendContactRequest);
			});

			vent.once('contactRequest',function(){
				$('#stayInTouch').fadeOut();
				var html = "<p class='small-fonts-1'>"+$('#twinName').text()+" wants to be in touch with you<br/><a id='acceptRequest' href='#' class='contactRespond'>Accept </a><a id='rejectRequest href='#' class='contactRespond'> Reject</a></p>";
				$(html).appendTo('#contactBox').hide(0).fadeIn();
				$('.contactRespond').click(function(e){
					e.preventDefault();
					var response = $(e.currentTarget).attr('id') === 'acceptRequest' ? 'true' : 'false';
					$.ajax({
						url : '/contactResponse',
						type: 'POST',
						data: {
							response 	 : response,
							chatId 	 	 : context.chatInfo.chatId,
							twinSocketId : context.chatInfo.twinSocketId,
							mySocketId 	 : socket.socket.sessionid 
						}
					})
					.done(function(d,ts,xhr){
						if(response === 'true'){
							$('#contactBox').fadeOut().html('').fadeIn();
							var html = "<p class='small-fonts-1'> You are now in contact with "+$('#twinName').text()+"</p>";
							$(html).appendTo('#contactBox').hide(0).fadeIn();
						}
						else $('#stayInTouch').fadeIn();
					})
					.fail(function(){
						//give fail message
					})
				})
			});
			
			vent.once('contactResponse',function(response){
				var bool = response === 'true' ? true : false;
				$('#contactBox').fadeOut().html('').fadeIn();
				if(bool) {
					var html = "<p class='small-fonts-1'> You are now in contact with "+$('#twinName').text()+"</p>";
					$(html).appendTo('#contactBox').hide(0).fadeIn();
				}
				else{
					var html = "<p class='small-fonts-1'>"+$('#twinName').text()+" is not ready yet.</p>";
					$(html).appendTo('#contactBox').hide(0).fadeIn().delay(3000).fadeOut()
					$('#stayInTouch').fadeIn();
				}
			});

			//Map Interests
			$('.interestImage').each(function(i,e){
				$(e).qtip({
					content : map.interests[$(e).attr('id').substr(-2)],
					
				})
			})

			//Tooltip
			context.toolTip = $('#reveal').qtip({
					id 		: 'revealToolTip',
					content : {
						text : 'Reveal your identity.The other person will not know that you pressed it untill he/she presses :)'
					},
					position: {
						my : 'bottom left',
						at : 'right'
					},
					show 	: {
						event : 'mouseenter'
					},
					hide 	: {
						fixed : true,
						delay : 300
					}
			});
		},

		onBeforeClose 	  : function(){
			vent.off('revealed');
			vent.off('contactRequest');
			vent.off('contactResponse');
			if($('.contactRespond').length){
				$('.contactRespond').off('click');
			}

		},

		sendRevealRequest : function(e){
			var context = this;
			$(e.currentTarget).addClass('disabled').text('Wait..')
			$.ajax({
				url : '/sendRevealRequest',
				type: 'POST',
				data: {
					chatId 		 : context.chatInfo.chatId,
					twinSocketId : context.chatInfo.twinSocketId,
					mySocketId 	 : socket.socket.sessionid
				}
			})
			.done(function(d,ts,xhr){

			})
			.fail(function(xhr,ts,er){
				if(parseInt(ts) === 400) return;
				else setTimeout(context.sendRevealRequest,3000,e);
			})
		},

		sendContactRequest : function(e){
			var context = e.data.viewContext;
			if($('#reveal').length){
				var html = " <p class='small-notifications'>You have to reveal before being in contact </p>";
				$(html).appendTo('#contactBox').hide(0).fadeIn().delay(3000).fadeOut()
				return;
			}
			//start sending request
			$(e.currentTarget).addClass('disabled').text('Sending request..');
			$.ajax({
				url : '/sendContactRequest',
				type: 'POST',
				data: {
					chatId 		 : context.chatInfo.chatId,
					twinSocketId : context.chatInfo.twinSocketId,
					mySocketId 	 : socket.socket.sessionid
				}
			})
			.done(function(d,ts,xhr){
				$(e.currentTarget).fadeOut(function(){
					$(this).remove();
				});
				var html = "<p class='small-notifications'>"+d.msg+"</p>"
				$('#contactBox').append(html).hide(0).fadeIn();
			})
			.fail(function(xhr,ts,err){
				setTimeout(context.sendContactRequest,3000);
			})
		}

	});
	return twinProfileItemView;
})