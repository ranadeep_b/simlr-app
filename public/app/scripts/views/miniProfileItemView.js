define(['../templates','marionette','../vent','jquery','qTip'],function(templates,Marionette,vent,$,qTip){
	var miniProfileItemView = Marionette.ItemView.extend({

		template 	: templates.miniProfileTemplate,

		events		: {
			'click #miniprofile-basic a' 		: 'initPicUpload',
			'click #profile-main-nav-list li' 	: 'changeActiveClass'
		},

		triggers 	: {},

		ui 		 	: {

		},

		toolTip 	: null,

		updateSocialStatus : function(className,title){
			var html = '<a href="#" class="label label-'+className+'">'+title+'</a>';
			$(html).appendTo($('#socialStatus').text('')).hide(0).fadeIn();
		},

		changeActiveClass : function(e){
			$('#profile-main-nav-list .active').removeClass('active');
			$(e.currentTarget).addClass('active');
		},	

		onShow 	: function(){
			var context = this;
			var rank = parseInt($('#socialStatus').text());
			switch(rank) {
				case 1: 
					context.updateSocialStatus('danger','Mischeivous');
					break;
				case 2:
					context.updateSocialStatus('warning','Off the road');
					break;
				case 3:
					context.updateSocialStatus('default','Just off the boat');
					break;
				case 4:
					context.updateSocialStatus('primary','Classy');
					break;
				case 5:
					context.updateSocialStatus('success','Legendary');
					break;
				default :
					context.updateSocialStatus('info','Something is wrong');
					break;
			}

			context.toolTip = $('#socialStatus a').qtip({
				id 		: 'socialStatusBadge',
				content : {
					title : 'User decency badge',
					text  : '<div> \
							<div class="row"><div class="col col-lg-5"><span class="label label-danger">Mischeivous</span></div><div class="col col-lg-7">Rude/Abused the usage of service.</div></div> \
							<hr><div class="row"><div class="col col-lg-5"><span class="label label-warning">Off the road</span></div><div class="col col-lg-7">Frequently creating bad experiences.</div></div> \
							<hr><div class="row"><div class="col col-lg-5"><span class="label label-default">Just off the boat</span></div><div class="col col-lg-7">Just started discovering.Normal user</div></div> \
							<hr><div class="row"><div class="col col-lg-5"><span class="label label-primary">Classy</span></div><div class="col col-lg-7">Very friendly and a genuine person.</div></div> \
							<hr><div class="row"><div class="col col-lg-5"><span class="label label-success">Legendary</span></div><div class="col col-lg-7">Explaination unnecessary !.</div></div> \
							<hr><div style="text-align : center;"><a href="#badgeLegend">Know more</a></div>\
							</div>'
				},
				position: {
					my : 'left center',
					at : 'right'
				},
				show 	: {
					event : 'click mouseenter'
				},
				hide 	: {
					fixed : true,
					delay : 300
				}
			});
		},

	    initPicUpload : function(e){
	    	vent.trigger('picUpload:started');
	    	return false; // e.preventDefault and stopBubbling at once
	    },

	    onBeforeClose : function(){
	    	this.toolTip.qtip('api').destroy();
	    }

	});
	return miniProfileItemView;
})