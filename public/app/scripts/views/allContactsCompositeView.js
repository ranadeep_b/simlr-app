define(['../vent','../templates','./buddyItemView','marionette'],function(vent,templates,buddyItemView,Marionette){
	var allContactsCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.allContactsCompositeView,

		id 		 		  : 'AllContacts',

		itemView 		  : buddyItemView,

		itemViewContainer : '#contacts',

		events			  : {
			'click .btn-small' : 'goBack'
		},

		triggers 		  : {},

		ui 		 		  : {},

		goBack  		  : function(){
			vent.trigger('goBackHomeCenter');
		}

	});

	return allContactsCompositeView;
})