define(['../templates','marionette','../vent','jquery'],function(templates,Marionette,vent,$){
	var picUploadItemView = Marionette.ItemView.extend({

		template 	: templates.picUploadTemplate,

		events		: {
			'click .btn-small' : 'returnBack'
		},

		triggers 	: {},

		ui 		 	: {

		},

		onShow     : function(){
			var context = this
			,fileOpt  = {}
			,file;
			input = $('#picUploadInput');
			input.hide();
			$('#pic-choose-button').click(function(e){
				input.click();
				return false;
			});
			input.change(function(){
				file     = this.files[0];
				fileOpt.name = file.name.split('\\').pop();
       		    fileOpt.size = (file.size/1024)/1024;
       		    fileOpt.type = file.type;
       		    if(validateresult()==true){
       		    	uploadFile();
       		    }
			});

			function uploadFile(){
				var error = true,
				    htmlprogress = '<div class="UploadDetails"><div class="filename"></div><div class="progress progress-striped"><div class="progress-bar progress-bar-info" style="width: 0%"></div></div></div>';
				context.$el.append(htmlprogress);
				var formData = new FormData();
				formData.append('myPic',file);
				$.ajax({
					url : '/setMyPic',
					type: 'POST',
					data: formData,
					dataType:'json',

					complete : function(data){
						var percent = 100;
						$('.progress-bar').width(percent+'%').html('<center>'+percent+"%</center>");
						console.log(data);
						if(data.responseJSON.responseCode == 1){
							$('#miniprofile-basic img').attr('src',data.responseJSON.picUrl);
							$('#current-pic img').attr('src',data.responseJSON.picUrl);
							$('.UploadDetails').fadeOut();

						}
						else {
							$('#current-pic').prepend('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>Oops..Err..please choose another file .</div>');
							$('.UploadDetails').remove();
							closeNotification();
						}
					},

					xhr: function() {  // custom xhr
                  		myXhr = $.ajaxSettings.xhr();
                  		if(myXhr.upload){ // check if upload property exists
                    	myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // for handling the progress of the upload
                		}
                		return myXhr;
                	},

                	cache: false,
                	contentType : false,
                	processData : false

				});
				return error;
			};

			function progressHandlingFunction(e){
				if(e.lengthComputable){
					var total = e.total;
					var loaded= e.loaded;
					$('.filename').text(fileOpt.name);
					var percent = Number(((e.loaded * 100)/e.total).toFixed(2));
					$('.progress-bar').width(percent+'%').html('<center>'+percent+"%</center>");
				}
			};

			function validateresult(){
				var canUpload = true;
				var validationresult = validateExtension();
				if(!validationresult){
					canUpload = false;
					$('#current-pic').prepend('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>Only .jpg .png formats allowed</div>');
					closeNotification();	
				}
				else {
					canUpload = true;
				}
				var validationresult = validateSize();
				if(!validationresult){
					canUpload = false;
					$('#current-pic').prepend('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>File size must be less than 4 MB plese.</div>');
					closeNotification();
				}
				return canUpload;
			};

			function validateExtension(){
				var ext = fileOpt.name.split('.').pop().toLowerCase();
				var allowed = ['png','jpg','jpeg'];
				if($.inArray(ext, allowed) == -1) {
          			return false;
      			}
      			else{
        			return true;
      			}
			};

			function validateSize(){
      			if (Number(fileOpt.size) > 4){
        			return false;
      			}
      			else{
        			return true;
      			}
    		};

    		function closeNotification(){
    			$('.alert-error').click(function(e){
    				$(this).fadeOut();
    			});
    		};

		},

	    returnBack : function(e){
	    	vent.trigger('goBackHomeCenter');
	    	return false; // e.preventDefault and stopBubbling at once
	    },

	    onBeforeClose : function(){
	    	$('p.alert-info .label').off();
	    }

	});
	return picUploadItemView;
})