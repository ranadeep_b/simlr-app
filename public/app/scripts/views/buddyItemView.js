define(['../templates','qTip','marionette'],function(templates,qtip,Marionette){
	var buddyItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		className 	: 'contact',

		template 	: templates.buddyTemplate,

		events		: {
			'click .contactWrapper' : 'moveToThread'
		},

		triggers 	: {},

		ui 		 	: {},

		toolTip 	: null,

		onShow 		: function(){
			var context = this;
			//Click event for contactContainer
			//Initalise tooltip
				context.toolTip = $('.unfollow-close').qtip({
				content : {
					text : "Do you want to <a href='#' class='confirmUnfollow'>Unfollow</a>?"
				},
				position: {
					my : 'bottom center',
					at : 'left'
				},
				show 	: {
					event : 'click'
				},
				hide 	: {
					event : false,
					inactive : 1800
				},
				events 	: {
					show : function(ev,api){
						ev.stopImmediatePropagation();
						var threadId = $(api.elements.target).parent().attr('id').split('/')[1];
						$('.confirmUnfollow').click(function(e){
							e.preventDefault();
							$.ajax({
								url : '/unfollow',
								data : {
									threadId : threadId
								}
							})
							.done(function(){
								$(api.elements.content).html('<b>Unfollowd.</b>')
							})
							.fail(function(){
								$(api.elements.content).html('<b>Failed. Try again later.</b>')
							})
						})
					},
					hide : function(ev,api){
						$('.confirmUnfollow').off();
					}
				}
			});
		},

		moveToThread  : function(e){
			if($(e.target).hasClass('unfollow-close')) return;  //Prevent this from firing when that tooltip is clicked , friggin thing took my hour
			var urlFragment = $(e.currentTarget).attr('id');
			window.location = '/home'+urlFragment;
		},

		onBeforeClose : function(){
		}

	});
	return buddyItemView;
})