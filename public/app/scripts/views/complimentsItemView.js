define(['../templates','marionette','../interestsMap','underscore','../vent'],function(templates,Marionette,map,_,vent){
	var complimentsItemView = Marionette.ItemView.extend({

		className 	: 'complimentsWrapper',

		template 	: templates.complimentsItemView,

		events		: {
			'click .btn-small' : 'goBack'
		},

		triggers 	: {},

		ui 		 	: {},

		onShow 		: function(){
			var compliments = _.map($('.complimentsInfo').attr('id').split(';'),function(i){ return '<i style="color : #ed0f69">'+map.traits[parseInt(i)]+'</i>'}).join(',')
			var params 	   = _.map($('.paramsInfo').attr('id').split(';'),function(i){ return '<i style="color : #1abc9c">'+map.interests[parseInt(i)]+'</i>'}).join(',')
			var string 	   = 'You are seen as a '+compliments+' person interested in '+params;
			$('#youAreSeenAs').html(string);
		},



		onBeforeClose : function(){

		},

		goBack 		  : function(){
			vent.trigger('goBackHomeCenter');
		}

	});
	return complimentsItemView;
})