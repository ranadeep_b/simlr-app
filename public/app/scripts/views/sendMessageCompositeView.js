define(['../vent','../templates','./buddyItemView','marionette'],function(vent,templates,buddyItemView,Marionette){
	var sendMessageCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.sendMessageTemplate,

		id 		 		  : 'sendNewMessageContainer',

		itemView 		  : buddyItemView,

		itemViewContainer : '#buddies',

		events			  : {
			'click .btn-small' : 'goBack'
		},

		triggers 		  : {},

		ui 		 		  : {},
		
		goBack  		  : function(){
			vent.trigger('goBackHomeCenter');
		}

	});

	return sendMessageCompositeView;
})