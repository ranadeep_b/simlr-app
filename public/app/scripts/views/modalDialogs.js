define(['qTip','../vent'],function(qtip,vent){

function dialogue(content, title) {
    $('<div />').qtip({
        content: {
            text: content,
            title: title
        },
        position: {
            my: 'center', at: 'center',
            target: $(window)
        },
        show: {
            ready: true,
            modal: {
                on: true,
                blur: false
            }
        },
        hide: false,
        style: 'dialogue',
        events: {
            render: function(event, api) {
                $('button,a', api.elements.content).click(function(e) {
                  vent.trigger('confirmClicked',$(e.currentTarget).attr('id') === 'confirmOk' ? true : false);
                  api.hide(e);
                });

                setTimeout(function(){
                  api.hide();
                },10 * 1000);
            },
            hide: function(event, api) { api.destroy(); }
        }
    });
}


window.alertModal = function(title,content) {
  var message = $('<p />', { text: content }),
      ok = $('<button />', { text: 'Ok', 'class': 'full' });

    dialogue( message.add(ok), title );
}

window.promptModal = function(title,content,val) {
	if(!val) val = 'Enter here';
  var message = $('<p />', { text: content }),
      input = $('<input />', { val: val, id : 'modalDialogPromptValue'}),
      ok = $('<button />', {
        text: 'Ok'
      }),
      cancel = $('<button />', {
        text: 'Cancel'
      });

    dialogue( message.add(input).add(ok).add(cancel), title);
}

window.confirmModal = function(title,content) {
      var html = "<div class='confirmModalContent'><p style='font-size : 14px;line-height : 20px;'>"+content+"</p><hr>"+
      "<div class='confirmButtons'>"+
      "<a class='confirmButton' id='confirmCancel'>Cancel</a>"+
      "<button class='confirmButton btn btn-default' id='confirmOk' style='margin-left : 180px;'>Ok</button>"+
      "</div></div>"

    dialogue( $(html), title );
}

	return {
		alert 	: window.alertModal,
		prompt  : window.promptModal,
		confirm : window.confirmModal
	};
})