define(['../vent','../templates','./notificationItemView','marionette','underscore','../interestsMap','./spinnerView'],
	function(vent,templates,notificationItemView,Marionette,_,interestsMap,spinner){
	var notificationsCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.notificationCompositeView,

		id 				  : 'notificationsWrapper',

		itemView 		  : notificationItemView,

		itemViewContainer : '#notifications',

		events			  : {
			'click .btn-small' : 'goBack'
		},

		triggers 		  : {},

		ui 		 		  : {},

		onShow 			  : function(){
			if(!$('#notifications').children().length){
				var html = '<h4>You dont have any new notifications ...</h4>'
				$(html).appendTo('#notifications').hide(0).fadeIn();
			}
			$('#notificationsBtn span').text('0');
			$.ajax('/clearNotifications');

			spinner.spin(document.getElementById('recentNotifications'));

			$.ajax({
				url : '/getRecentNotifications',
				dataType : 'json'
			})
			.always(function(){
				spinner.stop();
			})
			.done(function(json){
				_.each(json,function(notification){
					var html = "<li class='notificationWrapper'><div class='row'><div class='col col-lg-12'><a href="+notification.actionUrl+" class='notification'>"
					+ "<div class='row'><div class='col col-lg-2 col-offset-1'><span class='notificationImage' id="+'notification'+notification.type+">"
					+"</span></div><div class='col col-lg-9' style='text-align : left;'><span class='notificationContent'>"+notification.content+"</span></div></div></div></li>";
					$(html).appendTo('#recentNotifications').fadeIn();
				})
				//Map interests for compliments notifications
				$('a.notification[href="#compliments"] .notificationContent').each(function(i,e){
					var array = $(e).text().split('%');
						for(var i = 1;i<array.length;++i){
							array[i] = "<i style='color:#ed0f69;'>"+interestsMap.traits[array[i]]+"</i> ,";
						}
					var completeLine = array.join('')
					$(e).html(completeLine);	
				})
			})
			.fail(function(){
				$('#recentNotifications h3').text('Error loading notifications')
			})

		},

		goBack  		  : function(){
			vent.trigger('goBackHomeCenter');
		},

		onBeforeClose 	  : function(){
		}

	});

	return notificationsCompositeView;
})