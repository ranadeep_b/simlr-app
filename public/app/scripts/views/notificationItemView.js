define(['../templates','marionette'],function(templates,Marionette){
	var notificationItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		className 	: 'notificationWrapper',

		template 	: templates.notificationTemplate,

		events		: {},

		triggers 	: {},

		ui 		 	: {},

		onShow 		: function(){
			
		}

	});
	return notificationItemView;
})