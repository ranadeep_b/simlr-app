define(['underscore','../vent','../templates','./newMessageItemView','marionette','./spinnerView']
	,function(_,vent,templates,newMessageItemView,Marionette,spinner){
	var newMessagesCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.newMessagesTemplate,

		itemView 		  : newMessageItemView,

		itemViewContainer : '#newMessages',

		events			  : {
			'click .msgFooter button' : 'goBack'
		},

		triggers 		  : {},

		ui 		 		  : {},

		onShow 			  : function(){

			if(!$('#newMessages').children().length){
				var html = '<h4>You dont have any new messages ...</h4>'
				$(html).appendTo('#newMessages').hide(0).fadeIn();
			}

			spinner.spin(document.getElementById('recentThreads'));
			$.ajax({
				url :'/getRecentThreads',
				dataType : 'json'
			})
			.done(function(d,ts){
				spinner.stop();
				_.each(d,function(e){
					var html = '<a style="display : block;" href=#messages/'+e.threadId+' id='+e.lastUpdated+'><div class="row"><div class="col col-offset-2 col-lg-3"><img class="threadSenderPic" src='+e.senderPic+'></div>'+
								'<div class="col col-lg-7"><h3 class="threadSenderName">'+e.senderName+'<p class="content">'+e.content+'</p></h3></div>'+
								'</div></a>';
					$(html).appendTo('#recentThreads').hide(0).fadeIn();
				})
			})
			.fail(function(){
				spinner.stop();

			})
		},

		goBack  		  : function(){
			vent.trigger('goBackHomeCenter');
		}

	});

	return newMessagesCompositeView;
})