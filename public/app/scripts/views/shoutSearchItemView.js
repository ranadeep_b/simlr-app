define(['../templates',
		'marionette',
		'jquery',
		'../vent'
	   ],function(templates,Marionette,$,vent){
	var shoutSearchItemView = Marionette.ItemView.extend({

		template 	: templates.shoutSearchTemplate,

		events		: {
			'click #shoutSearchSubmit' : 'shoutSearch',
			'keydown #search' 		   : 'searchOnEnter'
		},

		triggers 	: {},

		ui 		 	: {},

		searchOnEnter : function(e){
			if(e.which === 13){
				this.shoutSearch(e);
			}
		},

		shoutSearch : function(e){
			e.preventDefault();
			var message = $('#search').val();
			if(message.length < 1 || message.length > 40) return;
			vent.trigger('search',message);
		}

	});
	return shoutSearchItemView;
})