define(['../templates','marionette'],function(templates,Marionette){
	var chatClosedItemView = Marionette.ItemView.extend({

		className 	: 'chatFeedback',

		template 	: templates.chatClosedItemView,

		events		: {
			'click #sendFeedback' : 'sendFeedback',
			'click .box-container': 'checkit'
		},

		triggers 	: {},

		ui 		 	: {},

		checkit 	: function(e){
			$(e.currentTarget).toggleClass('checked');
		},

		sendFeedback : function(e){
			var choices = [];
			$('.checked').each(function(i,e){
				choices.push($(e).attr('id').slice(1));
			});

			var chatRating = $('#chatRating').val();

			var twinTestimony = $('#twinTestimony').val() || ''

			$.ajax({
				url : '/sendFeedback/'+$('input[type="hidden"]').val(),
				type: 'POST',
				data: {
					choices    : choices.join(':'),
					chatRating : chatRating,
					testimony  :  twinTestimony
				}
			})
			.done(function(d,ts,xhr){
				window.location.href = '/home';
			})
			.fail(function(){
				window.location.href ='/home';
			})
		}

	});
	return chatClosedItemView;
})