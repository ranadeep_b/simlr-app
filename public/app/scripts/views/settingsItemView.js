define(['../templates','marionette','../vent','underscore','qTip'],function(templates,Marionette,vent,_,qtip){
	var settingsItemView = Marionette.ItemView.extend({

		id 		 	: 'settings',

		template 	: templates.settingsItemViewTemplate,

		events		: {

		},

		triggers 	: {},

		ui 		 	: {},

		oldName 	: '',

		toolTip 	: null,

		onShow 		: function(){
			var context = this;
			context.oldName = $('#nameChange .col-lg-8').text();
			$('#nameChange').on('click',{viewContext : context},context.changeName);
			$('#interestsChange').on('click',{viewContext : context},context.changeInterests);
			$('#fblikesChange').on('click',{viewContext : context},context.changefbLikes);
			if(!!$('#clearSlate').length) {
				 $('#clearSlate input[type=checkbox]').on('click',{viewContext : context},context.clearSlate)
				context.toolTip = $('#clearSlate').qtip({
					id 		: 'clearSlateTooltip',
					content : {
						text : 'Restore your badge to normal (Just off the boat). Can be used only once !'
					},
					position: {
						my : 'bottom center',
						at : 'center'
					},
					show 	: {
						event : 'mouseenter'
					},
					hide 	: {
						fixed : true,
						delay : 300
					}
				});
		  	}	
		},

		changeName 	: function(e){
			e.preventDefault();
			var context = e.data.viewContext;
			$('#nameChange').off('click');
			var updateHTML = function(name){
				 	$('#nameChange .row').remove();
					var html = '<div class="row"><div class="col col-lg-4">Name : </div><div class="col col-lg-8">'+name+'</div></div>';
					$(html).appendTo('#nameChange');
					$('#nameChange').on('click',{viewContext : context},context.changeName);
				},
			html = '<div class="row"><div class="col col-lg-4">Update name: </div><div class="col col-lg-4"><input type="text" id="newNameInput"></div><div class="col col-lg-4"><button class="btn btn-small btn-default" id="nameChangeSubmit">Update</button><button id="nameChangeCancel" class="btn btn-small btn-default">Cancel</button></div></div>';
			$('#nameChange').html(html).hide(0).fadeIn(200);
			$('#nameChangeSubmit').click(function(e){
				e.stopImmediatePropagation();
				var name = $('#newNameInput').val();
					//validate name;
					$(e.currentTarget).addClass('disabled').text('updating..')
				$.ajax({
					url : '/settings/update',
					data: {
						type : 'name',
						name : name
					}
				})
				.done(function(){
					updateHTML(name);
					$('#user-name').text(name);
					context.oldName = name;
				})
				.fail(function(){
					var html = "<span class='alert alert-warning'>Please remove special characters and use your genuine name</span>";
						$(html).appendTo('#nameChange').hide(0).fadeIn(1000);
					updateHTML(oldName);
				})
			})

			$('#nameChangeCancel').click(function(e){
				e.stopImmediatePropagation();
				updateHTML(context.oldName);
			})


		},

		changeInterests : function(e){
			var context 	  = e.data.viewContext;
			$('#interestParams').fadeIn();
			$('#interestsChange').off('click');
			var interestsArray = $('.settingsInterest').map(function(){ return 'p'+$(this).attr('id').substr(-2)});
				$.each(interestsArray,function(i,e){
					$(e).addClass('checked');
				})

			$('.box-container').click(function(e){
				$(e.currentTarget).toggleClass('checked');
				if($('.checked').length === 5) $('#interestsUpdateButton').removeClass('disabled');
			})

			$('#interestsUpdateButton').click(function(e){
				e.stopImmediatePropagation();
				if($('.checked').length != 5) {
					$('#interestParams h3').hide(500).fadeIn();
					return;
				}
				$(this).addClass('disabled').text('Updating..')
				var params = $('.checked').map(function(){ return $(this).attr('id').substr(-2); }).toArray().join(';')
				$.ajax({
					url 	: '/settings/update',
					data 	: {
						type : 'interests',
						params : params
					} 
				})
				.done(function(){
					var el = $('#interestParams').clone();
					$('#interestParams').fadeOut(function(){
						$(this).remove();
					})
					el.appendTo('#interestsChange').hide(0);
					$('#interestsChange').on('click',{viewContext : context},context.changeInterests);
					$('#interestsChange .col-lg-8').children().remove();
					$.each(params.split(';'),function(i,e){
						var html = '<span class="settingsInterest" id="interest'+e+'"></span>';
						$(html).appendTo('#interestsChange .col-lg-8');
					})
				})	
				.fail(function(){
					var el = $('#interestParams').clone();
					$('#interestParams').fadeOut(function(){
						$(this).remove();
					})
					el.appendTo('#interestsChange').hide(0);
					$('#interestsChange').on('click',{viewContext : context},context.changeInterests);
				})
			})

			$('#interestsUpdateCancel').click(function(e){
				e.stopImmediatePropagation();
				var el = $('#interestParams').clone();
					$('#interestParams').fadeOut(function(){
						$(this).remove();
					})
					el.appendTo('#interestsChange').hide(0);
					$('#interestsChange').on('click',{viewContext : context},context.changeInterests);
			})

			//remove all events on .done,.fail,and on cancel

		},

		changefbLikes  : function(e){
			var context = e.data.viewContext;
			$('#fblikesChange').off('click');
			var html = '<div class="col col-lg-8">Update facebook likes <button class="btn btn-small btn-default" id="fblikesChangeButton">Update</button><button class="btn btn-small btn-default" id="fblikesChangeCancel">Cancel</button></div>';
			var clone = $('#fblikesChange .col-lg-8');
			$('#fblikesChange .col-lg-8').remove();
			$(html).appendTo('#fblikesChange .row').hide(0).fadeIn();

			$('#fblikesChangeButton').click(function(e){
				e.stopImmediatePropagation();
				$(this).addClass('disabled').text('Updating');
				$.ajax({
					url : '/settings/update',
					data : {
						type : 'fbLikes'
					}
				})
				.done(function(){
					$('#fblikesChangeButton').text('Updated');
					$('#fblikesChange .col-lg-8').text('Likes update successfully').delay(900).fadeOut(function(){
						$(this).remove();
						$(clone).appendTo('#fblikesChange .row').hide(0).fadeIn();
					})
					$('#fblikesChange').on('click',{viewContext : context},context.changefbLikes);
				})
				.fail(function(){
					$('#fblikesChange .col-lg-8').remove();
					$(clone).appendTo('#fblikesChange .row').hide(0).fadeIn();
					$('#fblikesChange').on('click',{viewContext : context},context.changefbLikes);
				})
			})

			$('#fblikesChangeCancel').click(function(e){
				e.stopImmediatePropagation();
				$('#fblikesChange .col-lg-8').remove();
				$(clone).appendTo('#fblikesChange .row').hide(0).fadeIn();
				$('#fblikesChange').on('click',{viewContext : context},context.changefbLikes);
			})
		},

		clearSlate : function(e){
			var context = e.data.viewContext;
			var html = '<div class="col col-lg-6">Restore badge ?<button class="btn btn-small btn-default" id="clearSlateButton">Update</button><button class="btn btn-small btn-default" id="clearSlateCancel">Cancel</button></div>';
			$(html).appendTo('#clearSlate').hide(0).fadeIn();

			$('#clearSlate input[type=checkbox]').off('click');
			$('#clearSlateButton').click(function(e){
				$(this).addClass('disabled').text('Updating');
				$.ajax({
					url : '/settings/update',
					data : {
						type : 'clearSlate'
					}
				})
				.done(function(){
					$('#clearSlate').html('Badge Restored').delay(1000).fadeOut(function(){
						$(this).remove()
					})
					$('#clearSlate input[type=checkbox]').on('click',{viewContext : context},context.clearSlate)
				})
				.fail(function(){
					$('#clearSlate .col-lg-6')[1].text('Please try again').delay(1000).fadeOut(function(){
						$(this).remove();
					})
					$('#clearSlateButton').removeClass('disabled').text('Update');
					$('#clearSlate input').attr('checked',false);
					$('#clearSlate input[type=checkbox]').on('click',{viewContext : context},context.clearSlate)
				})
			})

			$('#clearSlateCancel').click(function(e){
				$($('#clearSlate .col-lg-6')[1]).fadeOut(function(){
					$(this).remove();
				})
				$('#clearSlate input').attr('checked',false);
				$('#clearSlate input[type=checkbox]').on('click',{viewContext : context},context.clearSlate)
			})
		},

		onBeforeClose : function(){
			$('.settingsContainer').remove();
			$('p.alert-info .label').off();
		}

	});
	return settingsItemView;
})