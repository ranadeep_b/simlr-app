define(['../templates','marionette','jquery','shuffle','../io','../vent','mCustomScroll'],function(templates,Marionette,$,shuffle,socket,vent,mCustomScrollbar){
	var questionItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		className 	: 'question',

		template 	: templates.questionTemplate,

		events		: {
			'click .activeQuestion .answerSubmitButton' 		: 'sendAnswer', 
			'click .activeQuestion .skipQuestionButton' 		: 'skipQuestion'
		},

		triggers 	: {},

		ui 		 	: {},

		onShow 	    : function(){
			vent.once('sendQuestionInfoToView',function(obj){
				setTimeout(function(){
					$('#'+obj.qid+' .q').text('');
					$('#'+obj.qid+' .q').shuffleLetters({text : obj.q});
				},0);
			})

			setTimeout(function(){
				if($('#questionsContainer').children().length > 1) $('#questionsContainerWrapper > div').mCustomScrollbar('scrollTo','bottom');
			},500);
		},

		sendAnswer  : function(){
			var context = this;
			var chatId  = $('.chatBox').attr('id'),
				qid     = $('.activeQuestion').attr('id'); 
			    message = $('.activeQuestion .answerInputBox').val();
				if(message.length < 1) return;
				$.ajax({
					url  : '/answer',
					type : 'POST',
					data : {
						chatId : chatId,
						qid    : qid,
						answer : message
					}
				})
				.done(function(d,ts){
					$('.activeQuestion .answerInput').fadeOut(function(){
						$(this).remove();
					})
					$('.activeQuestion .userAnswer .answer').text(message).hide(0).fadeIn();
					var html = "<p class='feedback'>Your response is sent..</h4>"
					$(html).appendTo('.activeQuestion').hide(0).fadeIn().delay(1500).fadeOut(500,function(){
						$(this).remove();
					})
				})
				.fail(function(){
					//!important
				})
		},

		skipQuestion: function(e){
			e.preventDefault();
			var chatId  = $('.chatBox').attr('id');
			$.ajax('/skipQuestion/'+chatId)
			.done(function(d,ts){
				$('.activeQuestion .answerInput').fadeOut(function(){
					$(this).remove();
				})
				var html = '<p class="feedback">Noted. Improve our questions by giving <a href=/feedback target="_blank">feedback</a></p>';
				$(html).appendTo('.activeQuestion').hide(0).fadeIn();
			}).
			fail(function(){
				//!important
			})
		}


	});
	return questionItemView;
})