define(['jquery','../vent','../templates','./chatItemView','marionette','../io','mCustomScroll','underscore','shuffle','favicon','visibility'],
	function($,vent,templates,chatItemView,Marionette,socket,mCustomScrollbar,_,shuffle,Tinycon,Visibility){
	var chatCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.chatsWrapperTemplate,

		itemView 		  : chatItemView,

		itemViewContainer : '#chatMessagesContainer',

		events			  : {
			'click #chatSubmitButton' : 'sendMesage',
			'click #byeButton' 		  : 'sayBye',
			'click #reportChat'       : 'reportChat'
		},

		triggers 		  : {},

		ui 		 		  : {},

		chatInfo 		  : null,

		revealed 		  : false,

		backspaceCount 	  : 0,

		faviconCount 	  : 0,

		visibilityListenerId : null,

		twinName 		  : 'Stranger',

		onShow 			  : function(){
			var context = this;
			$('#header').fadeOut();

			visibilityListenerId =Visibility.change(function(e,state){
				if(state === 'visible') {
					Tinycon.setBubble(0);
					context.faviconCount = 0;
				}
			})

			vent.once('sendChatInfoToView',function(obj){
				context.chatInfo = obj;
				$('.chatBox').attr('id',obj.chatId);
			})

			//subscribe
			socket.on('readyToReveal',function(twin){
				context.revealed = true,
				context.twinName = twin.name;
			})

			socket.on('newChatMessage',function(msg){
				var newMessage = msg;
				newMessage.byMe = false;
				newMessage.revealed = context.revealed;
				newMessage.twinName = context.twinName;
				context.collection.add(newMessage);
				var ul = $('#chatMessagesContainer')[0];
				ul.scrollTop = ul.scrollHeight;
				//add to collection ; this.collection.add({})
				//check if onChange is fired and view is rendered
				if(Visibility.hidden()){
					Tinycon.setBubble(++context.faviconCount);
				}
			});

			//isTyping, Psst and on Enter press send implementation 

			var isTypingSemaphore 	= false,
				callingPSSTSemaphore = false;

			var sendWritingOff = _.debounce(function(e){
				isTypingSemaphore = false;
				socket.emit('writingOff',{chatId : context.chatInfo.chatId});
			},2000)

			var incrementBackSpaceCount = _.debounce(function(e){
				++context.backspaceCount;
				if(context.backspaceCount > 9 && !callingPSSTSemaphore){
					callingPSSTSemaphore = true;
					$.ajax({
						url : '/getPSST/'+context.chatInfo.chatId,
						dataType : 'json'
					})
					.done(function(d){
						context.callPSST(d.psst);
						context.backspaceCount = 0;
						callingPSSTSemaphore = false;
					})
				}
			},1000);

			$('#chatTextArea').on('keydown',function(e){
				if(!isTypingSemaphore){
					socket.emit('writingOn',{chatId : context.chatInfo.chatId});
					isTypingSemaphore = true;
				}
				else {
					sendWritingOff();
				}
				if(e.which === 13 && !e.shiftKey) {
					socket.emit('writingOff',{chatId : context.chatInfo.chatId});
					isTypingSemaphore = false;
					e.preventDefault();
					context.sendMesage(e);
				}
				else if(e.which === 8) {
					incrementBackSpaceCount();
				}
			});

			socket.on('writingOn',function(){
				$('#isTyping').text('The other person is typing a message ....').hide(0).fadeIn();
			})

			socket.on('writingOff',function(){
				$('#isTyping').text('');
			})

			socket.on('newPSST',function(obj){
				context.callPSST(obj.psst);
			})
		},

		onBeforeClose 	  : function(){
			socket.removeAllListeners('writingOn');
			socket.removeAllListeners('writingOff');
			socket.removeAllListeners('newChatMessage');
			socket.removeAllListeners('readyToReveal');
			socket.removeAllListeners('newPSST');
			$('#chatTextArea').off('keydown');
			$('#header').fadeIn();
		},

		callPSST 		 : function(psst){
			$('#psstContainer').shuffleLetters({text : 'psst......'+psst});
		},	

		sendMesage 		  : function(e){
			e.preventDefault();
			var context = this;
			var content = $('#chatTextArea').val();
				if(content.length < 1) return; // To check !important
			var	 message = {content : content, time : Date.now(), byMe : true,revealed : context.revealed};
			$('#chatTextArea').val('');
			$.ajax({
				url 	: '/sendChatMessage',
				type    : 'POST',
				data 	: {
					content 	 : escape(message.content),
					twinSocketId : context.chatInfo.twinSocketId,
					chatId 		 : context.chatInfo.chatId
				}

			})
			.done(function(){
				context.collection.add(message);
				var ul = $('#chatMessagesContainer')[0];
				ul.scrollTop = ul.scrollHeight;
			})
			
		},

		sayBye 			  : function(e){
			e.preventDefault();
			//setup for confirmation,
			var context = this;
			var result = window.confirm('Are you sure you want to exit ?')
			if(result){
				$.ajax('/closeChat/'+context.chatInfo.chatId)
				.done(function(d,ts,xhr){
					vent.trigger('initChatClose',context.chatInfo.chatId)
				})
				.fail(function(xhr,ts,err){
					window.alert('Please try again.')
				})
				//init chat closed vent event in router
				//Fire controller
			}
			
		},

		reportChat 		 : function(e){
			e.preventDefault();
			var context = this;
			$.ajax({
				url  : '/report',
				data : {
					type   : 'reportChat',
					chatId : context.chatInfo.chatId
				}
			}).done(function(d,ts){
				$('#reportChat').fadeOut(function(){
					$(this).remove();
				})
				var html = '<p>Thanks for keeping the serivce safe . Action will be taken after verification.</p>';
				$(html).appendTo($('.chatBox report')).hide(0).fadeIn();
			})
		}

	});
	return chatCompositeView;
})