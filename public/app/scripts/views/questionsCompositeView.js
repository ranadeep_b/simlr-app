define(['underscore','../vent','../templates','./questionItemView','marionette','../io','jquery','mCustomScroll','tab','dropdown']
	,function(_,vent,templates,questionItemView,Marionette,socket,$,mCustomScrollbar,tab,dropdown){
	var questionsCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.questionsTemplate,

		id 		 		  : 'questionsWrapper',

		itemView 		  : questionItemView,

		itemViewContainer : '#questionsContainer',

		events			  : {
			'click .twinLikes' : 'getTwinLikes'
		},

		triggers 		  : {},

		ui 		 		  : {},

		onShow 			  : function(){
			var context = this;
			vent.once('sendChatInfoToView',function(obj){
				//sendFirstQuestion Request
				$.ajax('/getQuestion/'+obj.chatId)
				.done(function(d,ts){
					context.collection.add(d);
					vent.trigger('sendQuestionInfoToView',d);						
				})
				.fail(function(){
					//!important
				});
				//subscribe for other new questions
				socket.on('newQuestion',function(){
					if($('.activeQuestion').length > 0) {
						if($('.activeQuestion .answerInput').length > 0){
							$('.activeQuestion .answerInput').fadeOut(function(){
								$(this).remove();
							})
						}
						$('.activeQuestion').removeClass('activeQuestion');

					}

					$.ajax('/getQuestion/'+obj.chatId)
					.done(function(d,ts){
						context.collection.add(d);
						vent.trigger('sendQuestionInfoToView',d);							
					})
					.fail(function(){
						//!important
					})
				});

				socket.on('newAnswer',function(obj){
					$('#'+obj.qid+' .twinAnswer .answer').text(obj.answer).hide(0).fadeIn();
				})

				//Load Music Likes
				$.ajax({
					url : '/getTwinLikes/'+obj.chatId,
					data: {
						type : 'musicLikes'
					},
					dataType : 'json'
				})
				.done(function(d,ts){
					$('#likesContainer').html('');
					if(d.length < 1) {
						$('<h4>No Music liked :(</h4>').appendTo('#likesContainer').hide(0).fadeIn();
					}
					else {
						_.each(d,function(e){
							var url = 'https://graph.facebook.com/'+e.page_id+'/picture?type=square';
							var html = '<div class="like"><img src='+url+' alt='+e.name+'><span class="feedback likeName">'+e.name+'</div>';
							$('#likesContainer').append($(html));
						})
					}
				})
				.fail(function(){
					$('#likesContainer').html('');
					$('<h4>Error . Please try again later :(</h4>').appendTo('#likesContainer').hide(0).fadeIn();
				})
			})

			$('#questionsContainerWrapper > div').mCustomScrollbar({
				set_height 	   	  : 320,
				scrollButtons 	  : {
					enable : true,
					scrollType : 'pixels',
					scrollAmount : 250
				},
				mouseWheelPixels: 200,
				autoHideScrollbar : true, 
				advanced   		  : {
					updateOnContentResize : true,
					autoScrollOnFocus: false
				},
				callbacks 		  : {
					 onTotalScrollOffset: 16
				},
				theme 			  : 'dark',
			})

		},

		getTwinLikes 	: function(e){
			var context = this;
			e.preventDefault();
			var type  	= $(e.currentTarget).attr('id'),
			    chatId 	= $('.chatBox').attr('id'),
			    title 	= $(e.currentTarget).text();
			$.ajax({
				url  	: '/getTwinLikes/'+chatId,
				data 	: {
					type : type
				},
				dataType: 'json'  
			})
			.done(function(d,ts){
				$('#likesContainer').html('')
				if(d.length < 1) {
					$('<h4>No '+title+' liked :(</h4>').appendTo('#likesContainer').hide(0).fadeIn();
				}
				else {
					$('#likesContainer').append('<p>'+title+' liked by him/her</p>')
					_.each(d,function(e){
						var url = 'https://graph.facebook.com/'+e.page_id+'/picture?type=square';
						var html = '<div class="like"><img src='+url+' alt='+e.name+'><span class="likeName small-fonts-2">'+e.name+'</div>';
						$('#likesContainer').append($(html));
					})
				}
			})
			.fail(function(){
				$('<h4>Error . Please try again later :(</h4>').appendTo('#likesContainer').hide(0).fadeIn();
			})
		},

		onBeforeClose 	: function(){
			socket.removeAllListeners('newQuestion');
			socket.removeAllListeners('newAnswer');
		}

	});

	return questionsCompositeView;
})