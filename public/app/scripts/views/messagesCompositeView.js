define(['../vent',
		'../templates',
		'marionette',
		'./messageItemView',
		'../models/message',
		'moment',
		'../io',
		'underscore'
		],
	function(vent,templates,Marionette,messageItemView,message,moment,socket,_){
	var messagesCompositeView = Marionette.CompositeView.extend({

		template 		  : templates.messagesTemplate,

		itemView 		  : messageItemView,

		itemViewContainer : '#threadMessagesContainer',

		events			  : {
			'click #threadMessageSend' : 'sendMessage'
		},

		triggers 		  : {},

		ui 		 		  : {},

		threadId 		  : null,

		timeOutId 		  : null,

		onShow 		  	  : function(){
			var context = this;
			vent.once('sendThreadInfoToView',function(obj){
				context.threadId = obj.threadId;
				$('#twinPic').attr('src',obj.twinPicUrl)
				$('#twinName').text(obj.twinName + ' and you');
				if(!obj.online) {
					var dateStamp = moment(parseInt(obj.twinLastSeen)).calendar();
					$('#twinLastSeen').parent().prepend($('<span class="text-hide shouter-status-icon offline" style="margin-right : 5px;">.</span>'));
					$('#twinLastSeen').text('Last seen '+dateStamp);
				}
				else {
					$('#twinLastSeen').parent().prepend($('<span class="text-hide shouter-status-icon online" style="margin-right : 5px;">.</span>'));
					$('#twinLastSeen').text('online');
				}
				if(obj.isExpires){
					var contactRequestHtml = "";
					if(obj.contactRequests.fresh){
						contactRequestHtml = "<span id='contactRequestMessage'>To further continue,<a id='threadStayInTouch' href='#'>Stay in touch</a><span>";
					}
					else {
						if(obj.contactRequests.sent) contactRequestHtml = "Your contact request is pending.";
						else contactRequestHtml = " "+obj.twinName + " wants to be in touch ,<a id='threadStayInTouch' href='#'>Accept</a><span>";
					}
					var expiresOn = moment(obj.expires).fromNow();
					var html = "<h5 id='expireyMessage'> This conversation expires "+ expiresOn+". "+contactRequestHtml+".</h5>";
					$(html).appendTo($('#expireyDetails'));
					//Stay in touch
					if($('#threadStayInTouch').length){
						$('#threadStayInTouch').click(function(e){
							e.preventDefault();
							$(e.currentTarget).hide(0)
							$.ajax({
								url : '/sendThreadContactRequest/'+obj.threadId
							})
							.done(function(){
								if($('#contactRequestMessage').length) $('#contactRequestMessage').text(' Your contact request is pending.');
								else $('#expireyMessage').text('You are now in contact. Happy chatting.')
							})
							.fail(function(){
								$(e.currentTarget).fadeIn();
							})
						})
					}
					//Reporting
					if(obj.canReport){
						var html2 = '<button type="button" class="close" aria-hidden="true" id="reportShoutRespose" title="Report as spam">&times; Report</button>';
						$('#expireyDetails').after($(html2));
						$('#reportShoutRespose').click(function(e){
							$.ajax({
								url : '/report',
								data: {
									type : 'reportShoutResponse',
									threadId : obj.threadId
								}
							})
							.done(function(d,ts){
								$(e.currentTarget).parent().text('Thanks for your feedback').delay(1000).fadeOut(function(){
									$(this).remove();
									vent.trigger('takeMeHome');
								});
							})
						});


					}
				}
			})

			//Keydown , istyping a message and enter events
			var isTypingSemaphore = false;
			var sendWritingOff = _.debounce(function(e){
				isTypingSemaphore = false;
				socket.emit('writingOff',{threadId : context.threadId});
			},2000)
			//Bind the enter key press
			$('#threadMessageNew').keydown(function(e){
				if(!isTypingSemaphore){
					socket.emit('writingOn',{threadId : context.threadId});
					isTypingSemaphore = true;
				}
				else {
					sendWritingOff();
				}
				if(e.which === 13){
					socket.emit('writingOff',{threadId : context.threadId});
					isTypingSemaphore = false;
					e.stopImmediatePropagation();
					context.sendMessage(e);
				}
				
			})
			var ul = $('#threadMessagesContainer')[0];
			ul.scrollTop = ul.scrollHeight;	

			//Is typing a message 
			socket.on('writingOn',function(){
				$('#threadInfo #twinLastSeen').text(' is typing a message ...');
			})

			socket.on('writingOff',function(){
				$('#threadInfo #twinLastSeen').text('online');
			})

			if($('#threadMessagesContainer .message').length === 75){
				var html = '<a href="#" style="text-align : center;" id="loadAllMessages">Load all messages.</a>';
				$(html).prependTo('#threadMessagesContainer');
				$('#loadAllMessages').click(function(e){
					e.preventDefault();
					$.ajax({
						url 	: '/thread/'+context.threadId,
						data 	: {
							loadAll : 1
						} 
					})
					.done(function(d){
						$('#threadMessagesContainer').empty();
						_.each(d.data,function(el){
							context.addMessage(el);
						})
					})
				})
			}
		},

		onRender 		  : function(){
			var context = this;
			this.timeOutId = setInterval(context.checkAndUpdateStatus,30000,context);
			vent.once('sendThreadInfoToView',function(obj){
				if($('#messages .red-alert').text() > 0 ) {
					$.ajax('/clearNotifications/'+obj.threadId)
					.done(function(d,ts){
						$('#messages span').text(parseInt($('#messages span').text())-1);
					});
				}
			})
		},

		alertInfo 		  : function(msg){
			var html = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>'+msg+'</div>'
			$(html).appendTo('#threadMessagesContainer').hide(0).fadeIn().delay(6000).fadeOut(function(){
				$(this).remove();
			})
		},

		updateContactStatus  : function(status){
			if(status) $('.twin').addClass('online');
			else $('.twin').removeClass('online');
		},

		addMessage 			 : function(messageModelData){
			var messageModel = new message(messageModelData);
				this.collection.add(messageModel);
				var ul = $('#threadMessagesContainer')[0];
				ul.scrollTop = ul.scrollHeight;
		},

		sendMessage 		 : function(e){
			var context = this;
			e.preventDefault();
			if($('#threadMessageNew').val().length > 0){
				var message  = $('#threadMessageNew').val();
				$.ajax({
					url : '/sendThreadMessage',
					type: 'POST',
					data: {
						threadId : context.threadId,
						message  : message
					}
				})
				.done(function(d,ts){
					$('#threadMessageNew').val('');
					context.addMessage({
						sender   : 'me',
						timeSent : Date.now(),
						message  : message,
					});

					var ul = $('#threadMessagesContainer')[0];
						ul.scrollTop = ul.scrollHeight;
				})
				.fail(function(){
					context.alertInfo('Sending message failed..')
				})
			}
			else {
				context.alertInfo('Please Enter some text');
				$('#threadMessageNew').focus();
				}
		},

		checkAndUpdateStatus : function(context){
			var lastTimeSent = $($('.twin .timeStamp').toArray().reverse()[0]).attr('id') || Date.now()-60000
			$.ajax({
				url  : '/checkThreadStatus/'+context.threadId,
				data : {
					lastTimeSent : lastTimeSent
				},
				dataType : 'json'
			})
			.done(function(d,ts){
				if (d.contactStatus === true) {
					clearInterval(context.timeOutId);
					context.timeOutId = setInterval(context.checkAndUpdateStatus,9000,context);
					if(!$('#threadInfo .online').length > 0){
						$('#threadInfo .shouter-status-icon').removeClass('offline').addClass('online');
						$('#twinLastSeen').text('online');
					}
				}
				else {
					clearInterval(context.timeOutId);
					context.timeOutId = setInterval(context.checkAndUpdateStatus,50000,context);
					if(!$('#threadInfo .offline').length > 0) {
						$('#threadInfo .shouter-status-icon').removeClass('online').addClass('offline');
						$('#twinLastSeen').text('Last seen '+ moment(parseInt(d.contactStatus)).calendar());
					}
				}
				if(d.data.length){
					_.each(d.data,function(msg){
						context.addMessage(msg);
					})
				}
				$('#center-rg .timeStamp').each(function(i,e){
					$(e).text(moment(parseInt($(e).attr('id'))).fromNow());
				})
			})
			.fail(function(xhr){
				context.alertInfo('Please check your internet connection');
			})
		},

		onBeforeClose : function(){
			var context = this;
			clearInterval(context.timeOutId);
			$('#reportShoutRespose').off('click');
			socket.removeAllListeners('writingOn');
			socket.removeAllListeners('writingOff');
			if($('#threadStayInTouch').length) $('#threadStayInTouch').off();
		}

	});

	return messagesCompositeView;
})