define(['marionette','templates','../vent','../io'],function(Marionette,templates,vent,socket){
	var homeLayoutView = Marionette.Layout.extend({

		template : templates.homeLayout,

		regions : {
			miniprofile : '#miniprofile-rg',
			center		: '#center-rg',
			shoutbox	: '#shoutbox-rg'
		},

		onShow 	: function(){
			socket.on('AreYouReady',function(obj){
				vent.trigger('goBackHomeCenter');
				vent.trigger('AreYouReady',obj);
			});
		},
		onBeforeClose : function(){
			socket.removeAllListeners('AreYouReady');
		}
	});
	return homeLayoutView;
});