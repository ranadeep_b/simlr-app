define(['../templates','marionette','moment'],function(templates,Marionette,moment){
	var messageItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		className 	: 'message',

		template 	: templates.messageTemplate,

		events		: {},

		triggers 	: {},

		ui 		 	: {},

		onShow 		: function(){
			
			this.$el.find('.timeStamp').text(moment(parseInt(this.$el.find('.timeStamp').attr('id'))).fromNow());
		}

	});
	return messageItemView;
})