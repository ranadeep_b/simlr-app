define(['marionette','templates'],function(Marionette,templates){
	var chatClosedLayoutView = Marionette.Layout.extend({

		template : templates.chatClosedLayoutView,

		regions : {
			feedback : '#feedback'
		}
	});
	return chatClosedLayoutView;
});