define(['../templates','marionette'],function(templates,Marionette){
	var chatItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		template 	: templates.chatMessageTemplate,

		events		: {},

		triggers 	: {},

		ui 		 	: {}

	});
	return chatItemView;
})