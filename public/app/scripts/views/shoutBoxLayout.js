define(['marionette','../templates'],function(Marionette,templates){
	var shoutBoxLayout  = Marionette.Layout.extend({

		template : templates.shoutBoxLayout,

		regions	 : {
			shoutSearchFilter	: '#search-filter-wrapper',
			shoutsWrapper		: '#shouts-wrapper'
		},

		onShow	 : function(){
			
		}
	});

	return shoutBoxLayout;
});