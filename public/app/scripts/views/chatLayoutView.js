define(['jquery','marionette','templates','../io','../vent'],function($,Marionette,templates,socket,vent){
	var chatLayout = Marionette.Layout.extend({

		template : templates.chatLayout,

		regions : {
			profilebox  : '#profilebox-rg',
			chatbox 	: '#chatbox-rg',
			questionbox	: '#questionbox-rg'
		},

		chatInfo: null,

		onShow : function(){

			var context = this;

			$(window).on('beforeunload',function(){
				return 'Are you sure you want to leave this session ?'
			});

			vent.once('sendChatInfoToView',function(obj){
				context.chatInfo = obj;
			})

			$('#main').css('margin-top','4%');

			socket.on('closeChat',function(){
				//sent vent to chatClose Layout init
				vent.trigger('initChatClose',context.chatInfo.chatId);
			})
		},

		onBeforeClose : function(){
			$(window).off('beforeunload');
			socket.removeAllListeners('closeChat');
			$('#main').css('margin-top','1%');
		}
	});
	return chatLayout;
});