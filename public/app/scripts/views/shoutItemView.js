define(['../templates','marionette','../io','../vent'],function(templates,Marionette,socket,vent){
	var shoutItemView = Marionette.ItemView.extend({

		tagName		: 'li',

		template 	: templates.shoutTemplate,

		events		: {
			'click .respondShout'     : 'respondToShout',
			'click .reportShout' 	  : 'reportShout' 
		},

		triggers 	: {},

		ui 		 	: {},

		respondCount : 0,

		onShow 		 : function(){
			$('#shouts-container .timeStamp').each(function(i,e){
				$(e).text(moment(parseInt($(e).attr('id'))).fromNow());
			})
		},

		alertInfo   : function(msg,prependToEl,time,type){
			if($('.alert').length) $('.alert').remove(); 
			if(!type) type = 'alert-error';
			if(!time) time = 3000;
			if(this.respondCount > 0) return;
			var html = '<div class="alert '+type+'"><button type="button" class="close" data-dismiss="alert">×</button>'+msg+'</div>'
			$(html).prependTo(prependToEl).hide(0).fadeIn().delay(time).fadeOut(1000);
		},

		respondToShout : function(e){
			var context = this;

			e.preventDefault();
			var shout 	= $(e.currentTarget).closest('.shout'),
				shoutId = shout.attr('id'); 

			$.ajax({
				url  : '/shoutRespond',
				type : 'POST',
				data : {
					mySocketId : socket.socket.sessionid,
					shoutId    : shoutId
				},
				dataType : 'json'
			})
			.done(function(d,ts,xhr){
				if(d.error){
					context.alertInfo(d.msg,shout);
					if(context.respondCount > 0) {
						$('#shoutRespondButton').closest('.row').remove();
						context.respondCount = 0;
					}
					var html = "<div class='row' id='shortMessageShoutRespond'><hr><div class='col col-lg-8'><input type='text' placeholder='Send a short message' autocomplete='off' id='shoutRespondInput'></div><div class='col col-lg-4'><button type='button' id='shoutRespondButton' class='btn btn-small btn-primary'>Respond</button></div></div>";
					$(html).appendTo(shout).fadeIn();
					context.respondCount = 1;
					$('#shoutRespondInput').focus();
					$('#shoutRespondButton').click(function(e){
						var shoutRespondMessage = $('.shout input').val();
						shoutId  = $(e.currentTarget).closest('.shout').attr('id');
						if(0 < shoutRespondMessage.length < 140) {	
							$.ajax({
								url 	: '/shoutRespondSubmit',
								type 	: 'POST',
								data 	: {
									shoutId : shoutId,
									message : shoutRespondMessage
								},
								dataType : 'json' 
							})
							.done(function(d,ts,xhr){
								if(d.error){
									$(e.currentTarget).closest('.shout').html('<h4>'+d.error+'</h4>').delay(2000).fadeOut(3000,function(){
										$(this).remove();
									});
									return;
								}
								$(e.currentTarget).closest('.shout').html('<h5>Responded. You both can chat <a href="#messages/'+d.threadId+'">here</a>(for a limited time)</h5>').delay(5000).fadeOut(2000,function(){
									$(this).remove();
								});

							})
						}
						else {
							context.alertInfo('Please enter a message less than 140 characters',shout);
						}
					});

					$('#shoutRespondInput').blur(function(e){
						$('#shortMessageShoutRespond').fadeOut(function(){
							$(this).remove();
						});
					});


				}
				else {
					//vent.trigger('initShoutRespondChat',{})
					$('#shouts-container .btn,.btn-block').addClass('disabled');
					var twinSocketId = d.twinSocketId;
					context.alertInfo('Shouter is online ! waiting for him/her to accept .',shout,10000,'alert-success');
					socket.once('AreYouReadyResponse',function(obj){
						if(obj.response === true){
							vent.trigger('initChat',{twinSocketId : obj.twinSocketId,chatId : obj.chatId});
						}
						else {
							context.alertInfo('The other person cancelled the request',shout);
							$('#shouts-container .btn,.btn-block').removeClass('disabled');
						}
					});
					
				}
			})
			.fail(function(xhr,ts,err){
				context.alertInfo('There is a connection or server problem .',shout)
			})
		},

		reportShout : function(e){
			var shoutId = $(e.currentTarget).closest('.shout').attr('id');
			$.ajax({
				url  : '/report',
				data : {
					type    : 'reportShout',
					shoutId : shoutId 
				} 
			}).
			done(function(){
				$(e.currentTarget).parent().parent().parent().text('Thanks for the feedback').delay(1500).fadeOut(function(){
					$(this).remove();
				});
			})
		},

		onBeforeClose : function(){
			socket.removeAllListeners('AreYouReadyResponse');
		}	

	});
	return shoutItemView;
})