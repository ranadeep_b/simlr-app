define(['marionette','../vent','../controllers/index','../views/homeLayoutView','../views/spinnerView','../views/spinnerView2']
	,function(Marionette,vent,Controller,homeLayout,spinner,spinner2){

	var Router = Marionette.AppRouter.extend({

		appRoutes : {
			'home'					: 'home',
			'chat'					: 'initChat',
			'chatFeedback'			: 'closeChat',
			'messages' 				: 'getLatestThreads',  // eqv #messages
			'notifications' 		: 'popupNotifications',
 			'messages/new'  		: 'sendNewMessage',
 			'messages/:threadId'    : 'getThread',
 			'activity'				: 'myActivity',
 			'contacts' 				: 'getContacts',
 			'settings'				: 'getSettings',
 			'search/:query' 	    : 'searchShouts',
 			'compliments'			: 'getCompliments'
 		},

 		initialize : function(){
 			this.bind('route',function(){
 				url = Backbone.history.getHash();
 				url = url ? url : 'defaultHome';
 				ga('send','pageview',{page : '/#'+url});
 			})
 		},

		routes 	  : {

		}

		//methods for the ones in routes		
	});

	var RouterInstance =  new Router({controller : Controller});
	//All vents 
	vent.on('goBackHomeCenter',function(){				
		vent.trigger('showDefaultCenter');
		RouterInstance.navigate('')
	});

	vent.on('initChat',function(obj){
		RouterInstance.navigate('chat',{trigger : true});
		vent.trigger('initChatControllerInfo',obj);
	});

	vent.on('initChatClose',function(chatId){
		RouterInstance.navigate('chatFeedback',{trigger : true});
		vent.trigger('sendChatIdToController',chatId);
	})

	vent.on('refreshHashbang',function(){
		RouterInstance.navigate('');
	})

	vent.on('takeMeHome',function(){
		RouterInstance.navigate('home',{trigger : true});
	})

	vent.on('search',function(message){
 		RouterInstance.navigate('search/'+message,{trigger : true});
	})

	vent.on('loading:miniprofile',function(){
		$('#miniprofile-rg div').css('visibility','hidden');
		spinner2.spin(document.getElementById('miniprofile-rg'))

	})

	vent.on('loading:center',function(){
		$('#center-rg div').css('visibility','hidden');
		spinner.spin(document.getElementById('center-rg'));
	})

	vent.on('loading:shoutbox',function(){
		$('#shouts-container').css('visibility','hidden');
		spinner.spin(document.getElementById('shouts-wrapper'));
	})

	return RouterInstance;
});