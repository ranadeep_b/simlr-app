define(['jquery','vent','underscore','marionette','module',
		'views/modalDialogs',
		'io',
		'views/homeLayoutView',
		'models/miniProfile','views/miniProfileItemView','views/picUploadItemView',
		'models/homeCenter','views/homeCentreItemView',
		'collections/shoutsCollection','models/shout','models/shoutSearch',
		'views/shoutBoxLayout','views/shoutSearchItemView','views/shoutsWrapperCompositeView'],
		function($,vent,_,Marionette,Module,
				 modalDialogs,
				 socket,
				 homeLayoutView,
				 miniProfile,miniProfileItemView,picUploadView,
				 homeCenter,homeCentreItemView,
				 Shouts,Shout,ShoutSearch,
				 shoutBoxLayout,shoutSearchView,shoutsWrapperCompositeView){

			var app = new Marionette.Application();

			app.addRegions({
				main : '#main'
			});

			var homeLayout = app.homeLayout = new homeLayoutView;
		  	// Backup for view instance in controller 

			app.addInitializer(function(){
				//miniProfile
				var miniProfileData  = Module.config().miniProfileData;
				var MiniProfileModel = new miniProfile(miniProfileData);

				var HomeCentreModel  = new homeCenter({});
				//Add views , show through layout view ,

				var ShoutsCollection = new Shouts();
				
				app.main.show(homeLayout);
				//onShow of app.main region
				var MiniProfileView = new miniProfileItemView({model : MiniProfileModel});
				homeLayout.miniprofile.show(MiniProfileView);

				var HomeCentreView 	= new homeCentreItemView({model : HomeCentreModel})  
				homeLayout.center.show(HomeCentreView);
				
				var ShoutBoxView  	=  new shoutBoxLayout();
				homeLayout.shoutbox.show(ShoutBoxView);

				ShoutBoxView.shoutSearchFilter.show(new shoutSearchView({model : new ShoutSearch({})}));
				ShoutsCollection.fetch().done(function(){
					ShoutBoxView.shoutsWrapper.show(new shoutsWrapperCompositeView({ collection : ShoutsCollection}));
				})
				//onShow of ShoutBoxView
				//vent callbacks
				vent.on('picUpload:started',function(){
					app.homeLayout.center.show(new picUploadView());
				});

				vent.on('showDefaultCenter',function(){
					app.homeLayout.center.show(new homeCentreItemView({model : HomeCentreModel}));
				})

			});

		return app;

});