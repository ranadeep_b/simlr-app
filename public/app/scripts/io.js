define(['socketio'],function(io){
		var w = window.location;
		var mainSocket 		= io.connect('https://www.simlr.me:8081',{reconnect : false});

		var displayError = function(){
			var html = "<h3>Seems there is a problem.<a href='/'>Refresh</a> the browser and if problem persists update your browser and email to contact@simlr.me </a></h3>"
			$(html).appendTo('.overlay');
			$('#main').fadeOut();
			$('.overlay').fadeIn();
		}

		mainSocket.on('error',displayError);

		mainSocket.on('connect_failed',displayError);

		return mainSocket;
})