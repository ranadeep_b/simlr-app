define(function(require){

	return {

		homeLayout 			  		: require('tpl!templates/homeLayout'),

		chatLayout 					: require('tpl!templates/chatLayout'),

		picUploadTemplate	  		: require('tpl!templates/picUpload'),

		miniProfileTemplate   		: require('tpl!templates/miniProfileItemView'),

		twinProfileTemplate 		: require('tpl!templates/twinProfile'),

		newMessageTemplate 	  		: require('tpl!templates/newMessageItemView'),

		newMessagesTemplate 		: require('tpl!templates/newMessagesCompositeView'),

		settingsItemViewTemplate 	: require('tpl!templates/settingsItemViewTemplate'),

		messageTemplate 			: require('tpl!templates/messageItemView'),

		messagesTemplate 			: require('tpl!templates/messagesCompositeView'),

		myActivityItemView 			: require('tpl!templates/myActivityItemView'),

		buddyTemplate 				: require('tpl!templates/buddyTemplate'),

		allContactsCompositeView    : require('tpl!templates/allContactsCompositeView'),

		sendMessageTemplate 		: require('tpl!templates/sendMessageCompositeView'),

		notificationTemplate 		: require('tpl!templates/notificationItemView'),

		notificationCompositeView 	: require('tpl!templates/notificationsCompositeView'),

		homeCenterTemplate 	  		: require('tpl!templates/homeCenterItemView'),

		shoutBoxLayout		  		: require('tpl!templates/shoutBoxLayout'),

		shoutSearchTemplate   		: require('tpl!templates/shoutSearchItemView'),

		shoutTemplate 		  		: require('tpl!templates/shoutItemView'),

		shoutsWrapperTemplate 		: require('tpl!templates/shoutsWrapperCompositeView'),

		chatMessageTemplate 		: require('tpl!templates/chatItemView'),

		chatsWrapperTemplate 		: require('tpl!templates/chatCompositeView'),

		chatClosedLayoutView 		: require('tpl!templates/chatClosedLayoutView'),

		chatClosedItemView 			: require('tpl!templates/chatClosedItemView'),

		questionTemplate 			: require('tpl!templates/questionItemView'),

		questionsTemplate 			: require('tpl!templates/questionsCompositeView'),

		complimentsItemView 		: require('tpl!templates/complimentsItemView')
		
 	}
})