define(['backbone'],function(Backbone){

	var myActivity = Backbone.Model.extend({
		urlRoot : '/getMyActivity'
	})

	return myActivity;
});