define(['backbone'],function(Backbone){
		var compliments = Backbone.Model.extend({
			urlRoot : '/getCompliments'
		});
		return compliments;
})