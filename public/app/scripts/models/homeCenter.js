define(['backbone'],function(Backbone){

	var homeCenter = Backbone.Model.extend({
		urlRoot : '/getHomeCenter'
	})

	return homeCenter;
});