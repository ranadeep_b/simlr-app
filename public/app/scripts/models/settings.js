define(['backbone'],function(Backbone){
		var settings = Backbone.Model.extend({
			urlRoot : '/settings'
		});
		return settings;
})