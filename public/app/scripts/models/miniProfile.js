define(['backbone'],function(Backbone){

	var miniProfile = Backbone.Model.extend({
		urlRoot : '/getMiniProfile'
	})

	return miniProfile;
});