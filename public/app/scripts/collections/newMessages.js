define(['backbone','../models/newMessage'],function(Backbone,newMessage){
		var newMessages = Backbone.Collection.extend({
			model : newMessage,
			url   : '/getLatestThreads'
		});
	return newMessages;
})