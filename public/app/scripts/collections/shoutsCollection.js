define(['../models/shout','backbone'],function(Shout,Backbone){
	var Shouts = Backbone.Collection.extend({

		model : Shout,

		url   : '/getShouts'
	});

	return Shouts;
})