define(['backbone','../models/contact'],function(Backbone,contact){
		var buddies = Backbone.Collection.extend({
			model : contact,
			url   : '/getContacts'
		});
	return buddies;
})