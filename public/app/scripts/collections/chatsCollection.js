define(['../models/chatMessage','backbone'],function(chatMessage,Backbone){
	var chatMessages = Backbone.Collection.extend({

		model : chatMessage
	});

	return chatMessages;
})