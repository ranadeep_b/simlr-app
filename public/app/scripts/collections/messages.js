define(['backbone','../models/message'],function(Backbone,Message){
		var messages = Backbone.Collection.extend({
			model : Message
		});
	return messages;
})