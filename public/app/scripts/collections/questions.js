define(['backbone','../models/question'],function(Backbone,Question){
		var questions = Backbone.Collection.extend({
			model : Question
		});
	return questions;
})