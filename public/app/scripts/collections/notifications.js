define(['../models/notification','backbone'],function(notification,Backbone){
	var notifications = Backbone.Collection.extend({

		model : notification,
		url   : '/getLatestNotifications'
	});

	return notifications;
})