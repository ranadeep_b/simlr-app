define(['backbone','../models/buddy'],function(Backbone,follow){
		var buddies = Backbone.Collection.extend({
			model : follow,
			url   : '/getFollowers'
		});
	return buddies;
})