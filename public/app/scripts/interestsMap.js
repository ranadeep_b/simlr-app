define([],function(){
	var map = {
		traits : {
			0 : 'newbie in town',
			1 : 'Funny',
			2 : 'Frank',
			3 : 'Sweet',
			4 : 'Crazy',
			5 : 'Geeky',
			6 : 'Badass',
			7 : 'Sporty',
			8 : 'Smart',
			9 : 'Shy'
		},
		interests : {
			21 : 'Design &amp; Art',
			22 : 'Gaming',
			23 : 'Startups',
			24 : 'Fashion',
			25 : 'Dating &amp; Relationships',
			26 : 'Higher Education',
			27 : 'Hobbies',
			28 : 'TV Shows',
			29 : 'Gadgets &amp; Internet',
			30 : 'Sharing Ideas',
			31 : 'Finance &amp; Business',
			32 : 'Coding',
			33 : 'Mechanics &amp; Robotics',
			34 : 'Reading',
			35 : 'Science &amp; Technology',
			36 : 'Sports',
			37 : 'Travelling',
			38 : 'Writing'

		}
	};

	return map;
})