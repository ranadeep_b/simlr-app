<h2>Compliments</h2>
<hr>
<div class='row'>
	<div class='col col-lg-12'>
		<h3 id='youAreSeenAs' style='color : #333;'>
		</h3>
	</div>
</div>
<h3 style='margin-top : 10px;'>Remarks</h3>
<div class='row testimoniesContainer'>
	<div class='col col-lg-12'>
		<% _.each(testimonies,function(testimony){ %>
		<% if(testimony.testimony.length > 0) {%>
			<div class='testimonyWrapper'>
				<div class='row'>
					<div class='col col-lg-12'>
						<p class='testimony'><%= testimony.testimony%></p>
					</div>
				</div>
				<div class='row'>
					<div class='col col-offset-8 col-lg-4'>
						<span class='testimonyGiver' style='color :#000; font-style:italic;'><%= testimony.givenBy%></span>
					</div>
				</div>
			</div>
		<% }%>
		<% }) %>
	</div>
</div>
<div class='row'>
	<div class='col col-lg-4 col-offset-4'>
		<button class='btn btn-small btn-default' style='margin-top : 10px;'>Back</button>
	</div>
</div>
<div style='display : none;' class='complimentsInfo' id='<%= compliments%>'></div>
<div style='display : none;' class='paramsInfo' id='<%= params%>'></div>
