<div class='shout' id='<%= _id%>'>
	<div class='row shout-content'>
		<div class='col col-lg-3 shouter-info'>
				<div class='shouter-status'>
				<% if(shouterStatus.status) { %>
					<span class='text-hide shouter-status-icon online'>.</span>
				<% } else { %>
					<span class='text-hide shouter-status-icon offline'>.</span>
				<!--	<span class='timeStamp' id='<%=shouterStatus.lastOnline%>'></span> -->
				<% } %>
				</div>
				<span class='shout-time-elapse timeStamp' id='<%=created_At%>'></span>
		</div>
		<div class='col col-lg-6' style='padding : 1px;'>
				<p class='shout-text'><%= content%></p>
		</div>
		<div class='col col-lg-3'>
				<button type="button" class="close respond reportShout" aria-hidden="true" title='Report as negative'>&times;</button>
				<button href="#" class='respond respondShout btn btn-small btn-default'>Respond!</button>
		</div>
	</div>
	<div class='row shout-tags'>
			<div class='col col-lg-3'><a href="#" class='text-hide'>Tags</a></div>
			<div class='col col-lg-9 tags-wrapper'>
				<% _.each(tags,function(tag) { %>
					<a href="//google.com?q=<%=tag%>" target='_blank' class='shout-tag'><%= tag%></a>
				<% }) %>
			</div>
	</div>
	<% if(typeof respondedByYou !== 'undefined' && respondedByYou){%>
		<div class='row'>
			<div class='col col-lg-12'>
				<h5>* You <a href="#messages">responded</a> to this shout</h5>
			</div>
		</div>
	<% }%>
</div>
