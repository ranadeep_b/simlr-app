<div class='row' id='questionsContainerWrapper'>
	<div class='col col-lg-12'>
		<ul id='questionsContainer'></ul>
	</div>
</div>
<div class='row' id='likesContainerWrapper'>
	<ul class='nav nav-tabs'>
		<li class='active'><a href="#" id='musicLikes' class='twinLikes' data-toggle="tab">Music</a></li>
		<li><a href="#" id='booksLikes' class='twinLikes' data-toggle="tab">Books</a></li>
		<li><a href="#" id='tvLikes' class='twinLikes' data-toggle="tab">TV Shows</a></li>
		<li><a href="#" id='moviesLikes' class='twinLikes' data-toggle="tab">Movies</a></li>
		<li class="dropdown">
    		<a class="dropdown-toggle" data-toggle="dropdown" href="#">More..<span class="caret"></span></a>
    		<ul class="dropdown-menu" role="menu">
    			<li><a href="#" id='authorsLikes' class='twinLikes'>Authors</a></li>
    			<li><a href="#" id='foodsLikes' class='twinLikes'>Foods</a></li>
    			<li><a href="#" id='videoGamesLikes' class='twinLikes'>Video Games</a></li>
    			<li><a href="#" id='causesLikes' class='twinLikes'>Causes</a></li>
    			<li><a href="#" id='educationLikes' class='twinLikes'>Education</a></li>
    			<li><a href="#" id='fosLikes' class='twinLikes'>Field of Study</a></li>
    			<li><a href="#" id='sportsLikes' class='twinLikes'>Sports</a></li>
    			<li><a href="#" id='pstLikes' class='twinLikes'>Sports Teams</a></li>
    		</ul>
  		</li>
	</ul>
	<div id='likesContainer'></div>
</div>
