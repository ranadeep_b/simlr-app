<div id='<%= qid%>' class='activeQuestion'>
	<div class='row questionContainer'>
		<div class='col col-lg-12'>
			<div class='row'>
				<h4 class='q'><%= q%></h4>
			</div>
			<hr>
			<div class='row twinAnswer'>
				<div class='col col-lg-12'>
					<div class='answer'>
					</div>
				</div>
			</div>
			<div class='row userAnswer'>
				<div class='col col-lg-12'>
					<div class='answer'>
					</div>
				</div>
			</div>
			<div class='row answerInput'>
				<div class='row'>
					<div class='col col-lg-12'>
						<textarea class='answerInputBox' placeholder ='Write your response..'></textarea>
						<button class='answerSubmitButton btn btn-default'>Send</button>
					</div>
				</div>
				<div class='row skipQuestion'>
					<div class='col col-lg-6'>
						<p class='feedback'>Dont like the question ?</p>
					</div>
					<div class='col col-lg-6'>
						<a href="#" class='skipQuestionButton feedback'>Skip to next</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
