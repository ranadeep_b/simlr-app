<h3>Send a message</h3>
<div id='sendNewMessage'>
	<input type='text' placeholder='Buddy name..' autocomplete='off'>
	<hr>
	<textarea></textarea>
	<button class='btn btn-default'>Send</button> 
</div>
<p class='alert-info'> Enter the name of people in your contacts</p>
<hr>
<div class='buddies-container'>
	<ul id='buddies'>

	</ul>
</div>
<button class='btn btn-default btn-small'>Back</button>
