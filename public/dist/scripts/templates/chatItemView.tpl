	<% if(byMe) {%>
		<div class='chatMessage me'>
		<span class='senderName'>Me: </span>
	<%} else {%>
		<% if(revealed){ %>
		<div class='chatMessage twin'>
		<span class='senderName'><%=twinName %></span>
		<%} else {%>
		<div class='chatMessage twin'>
		<span class='senderName'>Stranger: </span>
		<%}}%>
		<p class='chatMessageContent'><%= content%></p>
</div>