<a href="#messages/<%= threadId%>">
		<div class='row'>
			<div class='col col-offset-2 col-lg-3'>
				<img class='threadSenderPic' src="<%= senderPic%>">
			</div>
			<div class='col col-lg-7'>
				<h3 class='threadSenderName'><%= sender%><p class='content'><%= content%></p></h3>
			</div>
		</div>	
</a>