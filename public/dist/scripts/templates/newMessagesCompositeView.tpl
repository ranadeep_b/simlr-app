<h2>Messages</h2>
<div class='row'>
	<div class='col col-offset-8 col-lg-4'>
		<a href="#contacts"> Send a message</a>
	</div>
</div>
<div class='msgHeader'>
	<h3>Unread</h3>
</div>
<div class='msgBody'>
	<ul id='newMessages'></ul>
</div>
<div id='recentThreads'>
	<h3>Recent threads</h3>
</div>
<hr>
<div class='msgFooter'>
	<div class='row'>
		<div class='col col-lg-4 col-offset-8'>
			<button class='btn btn-default btn-small'>Back</button>
		</div>
	</div>
</div>