<div id='miniprofile'>
	<div id='miniprofile-basic'>
		<a href="#"><img src="/getMyPic"></a>
		<h3 id='user-name'><span id='user-status-icon' class='text-hide'>.</span><%= name%></h3>
	</div>
	<div id='socialStatus' class='<%= rank%>'>
		<%= rank%>
	</div>
	<hr>
	<div id='miniprofile-alerts'>
		<a href="#messages"  id='messages' title='Messages'><span class='red-alert'><%= newThreads.length%></span></a>
		<a href="#notifications" id='notificationsBtn' title='Notifications'><span class='red-alert'><%= newNotifications.length%></span></a>
	</div>
	<div id='miniprofile-nav'>
		<div id='profile-main-nav-wrapper'>
			<ul id='profile-main-nav-list'class="nav nav-pills nav-stacked">
				<li class='active'>
					<a href="#home">
							Home
					</a>
				</li>
				<li>
					<a href="#activity">
						My Activity
					</a>
				</li>
				<li>
					<a href="#contacts">
						<span class='badge pull-right'><%= contacts_c %></span>
						Contacts
					</a>
				</li>
				<li>
					<a href="#compliments">
						Compliments
					</a>
				</li>
				<li>
					<a href="#settings">
						Settings
					</a>
				</li>
				<li>
					<a href="/logout">
						Logout
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>