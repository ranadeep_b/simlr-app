/*
 * Copyright 2011 Andrey “A.I.” Sitnik <andrey@sitnik.ru>,
 * sponsored by Evil Martians.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function(){if(document.visibilityState||document.webkitVisibilityState||document.mozVisibilityState)return;document.hidden=!1,document.visibilityState="visible";var e=null,t=0,n=function(){document.createEvent?(e||(e=document.createEvent("HTMLEvents"),e.initEvent("visibilitychange",!0,!0)),document.dispatchEvent(e)):typeof Visibility=="object"&&Visibility._onChange.call(Visibility,{})},r=function(){document.hidden=!1,document.visibilityState="visible",n()},i=function(){document.hidden=!0,document.visibilityState="hidden",n()};document.addEventListener?(window.addEventListener("focus",r,!0),window.addEventListener("blur",i,!0)):(document.attachEvent("onfocusin",r),document.attachEvent("onfocusout",i))})(),function(e){var t=function(t){return t!=e},n=window.Visibility={onVisible:function(e){if(!n.isSupported()||!n.hidden())return e(),n.isSupported();var t=n.change(function(r,i){n.hidden()||(n.unbind(t),e())});return t},change:function(e){if(!n.isSupported())return!1;n._lastCallback+=1;var t=n._lastCallback;return n._callbacks[t]=e,n._setListener(),t},unbind:function(e){delete n._callbacks[e]},afterPrerendering:function(e){if(!n.isSupported()||"prerender"!=n.state())return e(),n.isSupported();var t=n.change(function(r,i){"prerender"!=i&&(n.unbind(t),e())});return t},hidden:function(){return n._prop("hidden",!1)},state:function(){return n._prop("visibilityState","visible")},isSupported:function(){return t(n._prefix())},_doc:window.document,_chechedPrefix:null,_listening:!1,_lastCallback:-1,_callbacks:{},_hiddenBefore:!1,_init:function(){n._hiddenBefore=n.hidden()},_prefix:function(){if(null!==n._chechedPrefix)return n._chechedPrefix;if(t(n._doc.visibilityState))return n._chechedPrefix="";if(t(n._doc.webkitVisibilityState))return n._chechedPrefix="webkit"},_name:function(e){var t=n._prefix();return""==t?e:t+e.substr(0,1).toUpperCase()+e.substr(1)},_prop:function(e,t){return n.isSupported()?n._doc[n._name(e)]:t},_onChange:function(e){var t=n.state();for(var r in n._callbacks)n._callbacks[r].call(n._doc,e,t);n._hiddenBefore=n.hidden()},_setListener:function(){if(n._listening)return;var e=n._prefix()+"visibilitychange",t=function(){n._onChange.apply(Visibility,arguments)};n._doc.addEventListener?n._doc.addEventListener(e,t,!1):n._doc.attachEvent(e,t),n._listening=!0,n._hiddenBefore=n.hidden()}};n._init()}();